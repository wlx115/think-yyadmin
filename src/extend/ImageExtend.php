<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\extend;
use think\Image;
use think\exception\HttpResponseException;
/**
 * 图片处理扩展
 * Class ImageExtend
 * @package think\admin\extend
 */
class ImageExtend
{

    /**
     * 格式化图片地址
     * @param string $path 图片地址
     * @return string
     */
    private static function ImgPath(string $path){
		$parse_url = parse_url($path);
		return $parse_url['path'];
    }
    /**
     * 获取图像信息
     * @param string $path 图片地址
     * @return array
     */
    public static function getInfo(string $path)
    {
		$path = '.'.static::ImgPath($path);
		$image = Image::open($path);
		// 返回图片的宽度
		$width = $image->width(); 
		// 返回图片的高度
		$height = $image->height(); 
		// 返回图片的类型
		$type = $image->type(); 
		// 返回图片的mime类型
		$mime = $image->mime(); 
		// 返回图片的尺寸数组 0 图片宽度 1 图片高度
		$size = $image->size(); 
		return [$width,$height,$type,$mime,$size];
    }

    /**
     * 裁剪图片
     * @param string $path 图片地址
     * @param string $savepath 裁剪后保存地址，为空则覆盖原地址
     * @param int $w 裁剪宽度
     * @param int $h 裁剪高度
     * @param int $x 裁剪x轴开始位置
     * @param int $y 裁剪y轴开始位置
     * @return mixd
     */
    public static function cropImg(string $path,string $savepath='',int $w=300,int $h=300,int $x=0,int $y=0)
    {
        try {
            $path = '.'.static::ImgPath($path);
            $savepath = empty($savepath) ? $path : $savepath;
            $image = Image::open($path);
            $image->crop($w, $h,$x,$y)->save($savepath);
        } catch  (HttpResponseException $exception) {
            throw $exception;
        }  
    }

    /**
     * 生成缩率图
     * @param string $path 图片地址
     * @param string $savepath 裁剪后保存地址，为空则覆盖原地址
     * @param int $w 裁剪宽度
     * @param int $h 裁剪高度
     * @return mixd
     */
    public static function setThumb(string $path,string $savepath='',int $w=760,int $h=900)
    {
        try {
            $path = '.'.static::ImgPath($path);
            $savepath = empty($savepath) ? $path : $savepath;
            $image = Image::open($path);
            $image->thumb($w, $h)->save($savepath);
        } catch  (HttpResponseException $exception) {
            throw $exception;
        } 
    }
     /**
     * 图片水印添加
     * @param string $path 图片地址
     * @param string $water 水印图片地址
     * @param int $postion 水印位置1-9
     * @param int $alpha 水印透明度0-100
     * @return mixd
     */
    public static function imgWater(string $path,string $water='',int $postion=9,int $alpha=50)
    {
        try {
            $path = '.'.static::ImgPath($path);
            $image = Image::open($path);
            $image->water($water,$postion,$alpha)->save($path);
        } catch  (HttpResponseException $exception) {
            throw $exception;
        } 
    }
    /**
     * 文字水印添加
     * @param string $path 图片地址
     * @param string $text 水印文字
     * @param string $fontPath 水印文字字体路径
     * @param int $size 水印文字大小
     * @param string $color 水印文字颜色
     * @param int $locate 文字写入位置
     * @param int $offset 文字相对当前位置的偏移量
     * @param int $angle 文字倾斜角度
     * @return mixd
     */
    public static function fontWater($path,$text='',$fontPath='HYQingKongTiJ.ttf',$size=20,$color='#ffffff',$locate=9, $offset = 0, $angle = 0)
    {
        try {
            $path = '.'.static::ImgPath($path);
            $image = Image::open($path);
            $image->text($text,$fontPath,$size,$color,$locate,$offset,$angle)->save($path);
        } catch  (HttpResponseException $exception) {
            throw $exception;
        } 
    }
    /**
     * 图片压缩并展示，缩略图预览
     * @param string $path 图片地址
     * @param int $maxW 最大宽度
     * @param int $maxH 最大高度
     * @return mixd
     */
    public static function showImage($path,$maxW=200,$maxH=200)
    {
        $path = '.'.static::ImgPath($path);
        $image = Image::open($path);
        list($type,$width,$height,$mime) = [$image->type(),$image->width(),$image->height(),$image->mime()];
        $fun = "imagecreatefrom".$type;
        $newImage = $fun($path);
        $percent =  $maxW / $width;

        $new_width = intval($width * $percent);
        $new_height = intval($height * $percent);
        $image_thump = imagecreatetruecolor($new_width,$new_height);
        if(strtolower($type) == 'png' || strtolower($type) == 'gif'){
            imagesavealpha($newImage,true);//这里很重要;
            imagealphablending($image_thump,false);//这里很重要,意思是不合并颜色,直接用$img图像颜色替换,包括透明色;
            imagesavealpha($image_thump,true);//这里很重要,意思是不要丢了$thumb图像的透明色;
        }
        //将原图复制带图片载体上面，并且按照一定比例压缩,极大的保持了清晰度
        imagecopyresampled($image_thump,$newImage,0,0,0,0,$new_width,$new_height,$width,$height);
        imagedestroy($newImage);
        ob_clean();
        header('Content-Type: '.$mime);
        $funcs = "image".$type;
        $funcs($image_thump);
        echo ob_get_clean();
        exit;
    }
    
}