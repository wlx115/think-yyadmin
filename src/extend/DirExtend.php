<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\extend;

/**
 * 文件夹管理扩展
 * Class DirExtend
 * @package think\admin\extend
 */
class DirExtend
{
     /**
     * 创建目录
     * @param $path  目录地址 
     * @return bool
     */
    public static function dirCreate($path, $mode = 0777):bool
    {
        if (is_dir($path)) return true;
        $path = self::dirPath($path);
        $temp = explode('/', $path);
        $cur_dir = '';
        $max = count($temp) - 1;
        for ($i = 0; $i < $max; $i++) {
            $cur_dir .= $temp[$i] . '/';
            if (@is_dir($cur_dir)) continue;
            @mkdir($cur_dir, 0777, true);
            @chmod($cur_dir, 0777);
        }
        return is_dir($path);
    }

    /**
     * 格式化目录地址
     * @param $path 目录地址
     * @return string
     */
    public static function dirPath($path):string
    {
        $path = str_replace('\\', '/', $path);
        if (substr($path, -1) != '/') $path = $path . '/';
        return $path;
    }

    /**
     * 复制目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     * @return bool
     */
    public static function dirCopy($dir1, $dir2)
    {
        if (!file_exists($dir2)) return mkdir($dir2);
        //遍历原目录
        $arr = scandir($dir1);
        foreach ($arr as $val) {
            if ($val != '.' && $val != '..') {
                //原目录拼接
                $sfile = $dir1 . '/' . $val;
                //目的目录拼接
                $dfile = $dir2 . '/' . $val;
                if (is_dir($sfile)) {
                    self::dirCopy($sfile, $dfile);
                } else {
                    copy($sfile, $dfile);
                }
            }
        }
        return true;
    }
    
    /**
     * 移动目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     * @return string
     */
    public static function dirMove($dir1, $dir2)
    {
        return self::dirCopy($dir1, $dir2) && self::dirDelete($dir1) ? true : false;
    }

    /**
     * 删除目录（递归删除）
     * @param $path 路径
     * @return bool
     */
    public static function dirDelete($path)
    {
        if (!is_dir($path)) {
             return @unlink($path);
        }else{
            //遍历目录
            $arr = scandir($dir);
            foreach ($arr as $val) {
                if ($val != '.' && $val != '..') {
                    //路径链接
                    $file = $dir . '/' . $val;
                    if (is_dir($file)) {
                        self::dirDelete($file);
                    } else {
                        @unlink($file);
                    }
                }
            }
            return rmdir($dir);
        }
    }

    /**
     * 检查读写权限
     * @param $d 目录或文件地址
     * @return string
     */
    public static function isWrite($d)
    {
        if (is_file($d)) {
            return is_writeable($d) ? true : false;
        } else {
            $tfile = "_test.txt";
            $fp = @fopen($d . "/" . $tfile, "w");
            if (!$fp) {
                return false;
            }
            fclose($fp);
            $rs = @unlink($d . "/" . $tfile);
            if ($rs) {
                return true;
            }
            return false;
        }

    }
    /**
     * 创建文件
     * @param string $path 存储地址
     * @param string $content  储存内容
     * @return void
     */
    public static function createFile($path,$content='')
    {
        if(static::dirCreate(dirname($path)) && $file = fopen($path, "w")){
            fwrite($file, $content);
            fclose($file);
            return true;
        }
        return false;
    }
}