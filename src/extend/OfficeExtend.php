<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\extend;

/**
 * Office文件处理扩展
 * word转html，XLS转html，PDF转图片
 * Class OfficeExtend
 * @package think\admin\extend
 */
class OfficeExtend
{
    /**
     * 上传的图片资源
     */
    static $imgs=[];
    /**
     * 图片资源保存地址
     */
    static $savepath='upload';
    /**
     * word转html
     * @param $source 需要转换的word源文件
     * @param $savepath 图片保存路径
     * @return array [状态，html,图片集]
     */
    public static function doc2html($source,$savepath = 'upload')
    {
        static::$savepath = $savepath;
        $ext = pathinfo($source, PATHINFO_EXTENSION);
        if($ext != 'docx') return [false,'只支持docx后缀的文件',[]];
        if(!file_exists($source)) return [false,'请输入要转换的文件',[]];
        $phpWord = \PhpOffice\PhpWord\IOFactory::load($source);
        $html = '';
        foreach ($phpWord->getSections() as $section) {
            foreach ($section->getElements() as $ele1) {
                $html .= '';
                if ($ele1 instanceof \PhpOffice\PhpWord\Element\TextRun) {
                    $paragraphStyle = $ele1->getParagraphStyle();
                    if ($paragraphStyle) {
                        $html .= '<p style="text-align:'. $paragraphStyle->getAlignment() .';text-indent:2em;">';
                    } else {
                        $html .= '<p style="text-indent:2em;">';
                    }
                    foreach ($ele1->getElements() as $ele2) {
                        $html .= self::elementHandler($ele2, $ele1);
                    }
                    $html .= '</p>';
                } elseif ($ele1 instanceof \PhpOffice\PhpWord\Element\PreserveText) { //判断是否保留元素(如自动生成链接的网址元素)
                    $data = $ele1->getText();
                    $find = array('{', 'HYPERLINK', '}', ' ', '"', 'f', 'g');
                    $replace = '';
                    $resText = str_replace($find, $replace, $data);
                    if (isset($resText)) {
                        $html .= $resText[0];
                    }
                } elseif ($ele1 instanceof \PhpOffice\PhpWord\Element\Table) {
                    $all_table_elements = $ele1->getRows();
                    $countColumns = $ele1->countColumns();
                    $html .= '<table style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;" width="100%">';
                    foreach ($all_table_elements as $tky => $tvl) {
                        $html .= '<tr style="padding:0">';
                        $all_table_cells = $tvl->getCells();
                        foreach ($all_table_cells as $cky => $cvl) {
                            $cell_elements = $cvl->getElements();
                            //获取表格宽度(返回单位为：缇)
                            $td_width = $cvl->getWidth();
                            $td_width_px = round($td_width / 15, 0);
                            $html .= '<td style="border: 1px solid #777777;padding:2px 5px;width:' . $td_width_px . '">';
                            foreach ($cell_elements as $cl) {
                                //判断当存在elements属性时执行
                                if (property_exists($cl, 'elements')) {
                                    $content_elements = $cl->getElements();
                                    foreach ($content_elements as $eky2 => $evl2) {
                                        $html .=  self::elementHandler($evl2, $cl);
                                    }
                                }
                            }
                            $html .= '</td>';
                        }
                        if(count($all_table_cells) < $countColumns){
                            for ($i=0; $i < $countColumns-count($all_table_cells); $i++) { 
                                $html .= '<td style="border: 1px solid #777777;padding:2px 5px;"></td>';
                            }
                        }
                        $html .= '</tr>';
                    }
                    $html .= '</table>';
                }
            }
        }

        return [true,mb_convert_encoding($html, 'UTF-8', 'GBK'),self::$imgs];
    }
    /**
     * 元素内容数据处理
     * @param [type] $end_element 最末级元素，是$parent_element的子元素
     * @param [type] $parent_element 为当前元素
     * @return void
     */
    private static function elementHandler($end_element, $parent_element)
    {
        $html = '';
        if ($end_element instanceof \PhpOffice\PhpWord\Element\Text) { //判断是否普通文本
            $style = $end_element->getFontStyle();
            $fontFamily = mb_convert_encoding($style->getName(), 'GBK', 'UTF-8');
            $fontSize = $style->getSize() ? $style->getSize() : '';
            $isBold = $style->isBold();
            $fontcolor = $style->getColor();
            $styleString = '';
            $fontFamily && $styleString .= "font-family:{$fontFamily};";
            $fontSize && $styleString .= "font-size:{$fontSize}px;";
            $isBold && $styleString .= "font-weight:bold;";
            $fontcolor && $styleString .= "color:{$fontcolor};";
            $html .= sprintf('<span style="%s">%s</span>',$styleString, mb_convert_encoding($end_element->getText(), 'GBK', 'UTF-8'));
        } elseif ($end_element instanceof \PhpOffice\PhpWord\Element\Link) {  //判断是否链接
            $style = $end_element->getFontStyle();
            $fontFamily = mb_convert_encoding($style->getName(), 'GBK', 'UTF-8');
            $fontSize = $style->getSize() ? ($style->getSize() / 72) * 96 : '';
            $isBold = $style->isBold();
            $fontcolor = $style->getColor();
            $styleString = '';
            $fontFamily && $styleString .= "font-family:{$fontFamily};";
            $fontSize && $styleString .= "font-size:{$fontSize}px;";
            $isBold && $styleString .= "font-weight:bold;";
            $fontcolor && $styleString .= "color:{$fontcolor};";
            $html .= sprintf('<a href="%s" style="%s">%s</a>', $end_element->getSource(), $styleString, mb_convert_encoding($end_element->getText(), 'GBK', 'UTF-8'));
        } elseif ($end_element instanceof \PhpOffice\PhpWord\Element\Image) { //判断是否图片
            $imageSrc = self::imgpath($end_element->getSource(),$end_element->getImageExtension());
            $imageData = $end_element->getImageStringData(true);
            // $imageData = 'data:' . $ele2->getImageType() . ';base64,' . $imageData;
            file_put_contents('./'.$imageSrc, base64_decode($imageData));
            self::compressedImage($imageSrc,$imageSrc);
            $html .= '<img src="'.'/'.$imageSrc .'" style="margin:10px auto;display:block">';
        }
        return $html;
    }
    /**
     * XLS转html
     * @param $source 需要转换的xls源文件
     * @param $savepath 图片保存路径
     * @return array [状态，html]
     */
    public static function xls2html($source){
        if(!file_exists($source)) return [false,'请输入要转换的文件'];
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($source);
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Html($spreadsheet);
        $writer->setSheetIndex(0);
        $html = $writer->generateStyles().$writer->generateSheetData();//generateSheetData
        return [true,$html];
    }

    /**
     * PDF转图片
     * @param $source 需要转换的PDF源文件
     * @param $savepath 图片保存路径
     * @return array [状态，html,图片集]
     */
    public static function pdf2png($PDF,$type='arr',$savepath = 'upload')
    {
        static::$savepath = $savepath;
        if(!extension_loaded('imagick')){
            return [false,'缺少imagick扩展',[]];
        }
        if(!file_exists($PDF)){
            return [false,'请输入要转换的文件',[]];
        }
     
        $IM = new imagick();    
        $IM->setResolution(120,120);   
        $IM->setCompressionQuality(100);
        $IM->readImage($PDF);
        $html = '';
        foreach ($IM as $Key => $Var){
            $Var->setImageFormat('png');
            $Filename = self::imgpath($key,'png');
            if($Var->writeImage('./'.$Filename) == true){
                $Return[] = $Filename;
                $html .= '<img src="/'.$Filename.'" style="margin:0px auto;display:block">';
            }
        }
        return [true,$type == 'arr' ? $Return : $html,self::$imgs];
    }

    /**
     * 保存图片路径生成
     */
    private static function imgpath($name='',$ext='jpg')
    {
        $dir = static::$savepath."/wordimg/";
        $path = $dir.md5($name).'.'.$ext;
        static::$imgs[] = $path;
        if(!is_dir($dir)) dir_create($dir);
        return $path;
    }

    /*
    **函数:function compressedImage($imgsrc, $imgdst)
    **功能：
    **  base64格式编码转换为图片并保存对应文件夹 
    **参数：
    ** imgsrc:需要修改的图片路径
    ** imgdst ：压缩后保存路径
    */
    private static function compressedImage($imgsrc, $imgdst) {
        list($width, $height, $type) = getimagesize($imgsrc);
        
        $new_width = $width;//压缩后的图片宽
        $new_height = $height;//压缩后的图片高
            
        if($width >= 800){
            $per = 800 / $width;//计算比例
            $new_width = intval($width * $per);
            $new_height = intval($height * $per);
        }
        
        switch ($type) {
            case 1:
                $giftype = check_gifcartoon($imgsrc);
                if ($giftype) {
                header('Content-Type:image/gif');
                $image_wp = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromgif($imgsrc);
                imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                //90代表的是质量、压缩图片容量大小
                imagejpeg($image_wp, $imgdst, 90);
                imagedestroy($image_wp);
                imagedestroy($image);
                }
                break;
            case 2:
                header('Content-Type:image/jpeg');
                $image_wp = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefromjpeg($imgsrc);
                imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                //90代表的是质量、压缩图片容量大小
                imagejpeg($image_wp, $imgdst, 90);
                imagedestroy($image_wp);
                imagedestroy($image);
                break;
            case 3:
                header('Content-Type:image/png');
                $image_wp = imagecreatetruecolor($new_width, $new_height);
                $image = imagecreatefrompng($imgsrc);
                imagecopyresampled($image_wp, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                //90代表的是质量、压缩图片容量大小
                imagejpeg($image_wp, $imgdst, 90);
                imagedestroy($image_wp);
                imagedestroy($image);
                break;
        }
    }
}