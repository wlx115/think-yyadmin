<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\extend;

/**
 * 数据处理扩展
 * Class DataExtend
 * @package think\admin\extend
 */
class DataExtend
{

    /**
     * 一维数组生成数据树
     * @param array $list 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $sub 子数组名称
     * @return array
     */
    public static function arr2tree(array $list, string $cid = 'id', string $pid = 'pid', string $sub = 'sub'): array
    {
        [$tree, $temp] = [[], array_combine(array_column($list, $cid), array_values($list))];
        foreach ($list as $vo) if (isset($vo[$pid]) && isset($temp[$vo[$pid]])) {
            $temp[$vo[$pid]][$sub][] = &$temp[$vo[$cid]];
        } else {
            $tree[] = &$temp[$vo[$cid]];
        }
        return $tree;
    }

    /**
     * 一维数组生成数据树
     * @param array $list 待处理数据
     * @param string $cid 自己的主键
     * @param string $pid 上级的主键
     * @param string $cpath 当前 PATH
     * @param string $ppath 上级 PATH
     * @return array
     */
    public static function arr2table(array $list, string $cid = 'id', string $pid = 'pid', string $cpath = 'path', string $ppath = ''): array
    {
        $tree = [];
        foreach (static::arr2tree($list, $cid, $pid) as $attr) {
            $attr[$cpath] = "{$ppath}-{$attr[$cid]}";
            $attr['sub'] = $attr['sub'] ?? [];
            $attr['spc'] = count($attr['sub']);
            $attr['spt'] = substr_count($ppath, '-');
            $attr['spl'] = str_repeat("　├　", $attr['spt']);
            $sub = $attr['sub'];
            unset($attr['sub']);
            $tree[] = $attr;
            if (!empty($sub)) {
                $tree = array_merge($tree, static::arr2table($sub, $cid, $pid, $cpath, $attr[$cpath]));
            }
        }
        return $tree;
    }

    /**
     * 获取数据树子ID集合
     * @param array $list 数据列表
     * @param mixed $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSubIds(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $ids = [intval($value)];
        foreach ($list as $vo) if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === intval($value)) {
            $ids = array_merge($ids, static::getArrSubIds($list, intval($vo[$ckey]), $ckey, $pkey));
        }
        return $ids;
    }

    /**
     * 获取数据的直接子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSub(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        // pr($list);
        $subs=array();
        foreach($list as $vo) if (intval($vo[$pkey]) === intval($value)) {
            $subs = array_merge($subs,[$vo]);
        }
        return $subs;
    }

    /**
     * 获取数据的所有子集
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrSubs(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $subs=array();
        foreach($list as $vo) if (intval($vo[$pkey]) === intval($value)) {
            $subs = array_merge($subs,[$vo],static::getArrSubs($list, intval($vo[$ckey]), $ckey, $pkey));
        }
        return $subs;
    }

    /**
     * 获取数据的父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrParent(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $arrs=array();
        foreach($list as $vo) if (intval($vo[$ckey]) > 0 && intval($vo[$ckey]) === intval($value)) {
            return $vo;break;
        }
        return $arrs;
    }

    /**
     * 获取数据的所有父级
     * @param array $list 数据列表
     * @param array $value 起始有效ID值
     * @param string $ckey 当前主键ID名称
     * @param string $pkey 上级主键ID名称
     * @return array
     */
    public static function getArrParents(array $list, $value = 0, string $ckey = 'id', string $pkey = 'pid'): array
    {
        $arrs=array();
        foreach($list as $vo) if (intval($vo[$ckey]) > 0 && intval($vo[$ckey]) === intval($value)) {
            $arrs = array_merge($arrs,[$vo],static::getArrParents($list, intval($vo[$pkey]), $ckey, $pkey));
        }
        return $arrs;
    }

    /**
     * 多维数组中查找数据
     * @param array $array 要查找的数据
     * @param string $key 查找的键值
     * @param string|array $value 查找的键值对应的值
     * @return array
     */
    public static function getArrSearch(array $array,string $key='id',$value=''): array
    {
        $newArray = [];
        $valArr = is_array($value) ? : explode(',',(string) $value);
        foreach($array as $keyp=>$valuep){
            if(in_array($valuep[$key],$valArr)){
                array_push($newArray,$array[$keyp]);
            }
        }
        return $newArray;
    }

    /**
     * 清除html换行等
     * @param string $str 需要清理的字符串
     * @return string
     */
    public static function clearHtml(string $str = ''): string
    {
        if(empty($str)) return $str;
        $str = htmlspecialchars_decode($str);
        $str = trim(strip_tags($str));
        $str = rtrim($str," ,.。，-——（【、；‘“??《<@");
        $str = str_replace(array("&nbsp;","&amp;nbsp;","\t","\r\n","\r","\n","　"," "),array("","","","","","",""," "),$str);
        return $str;
    }
}