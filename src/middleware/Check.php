<?php
// +----------------------------------------------------------------------
// | LHSystem
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.yyinfos.com
// +----------------------------------------------------------------------
// | 作者：独角戏 <qhweb@foxmail.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace think\admin\middleware;

use think\facade\Route;
use think\facade\Session;
use think\admin\service\SiteService;
/**
 * 系统完整性检查
 * Class Check
 */
class Check
{
    /**
     * 系统安装检查中间件
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        // 注册安装程序路由
        Route::group('install', function () {
            Route::rule('/', '\\think\\admin\\install\\Index@index');
            Route::rule('/test', '\\think\\admin\\install\\Index@test');
            Route::rule('/build', '\\think\\admin\\install\\Index@build');
            Route::rule('/step/[:step]', '\\think\\admin\\install\\Index@step');
            Route::rule('/delInstall', '\\think\\admin\\install\\Index@delInstall');
        })->pattern(['step' => '\w+']);

        //检查是否安装系统
        $root = $request->pathinfo();
        if (!file_exists(root_path('data') . 'install.lock') && strpos($root,'nstall') <= 0) {
            return redirect('/install', 301);
        }
        //检查系统授权信息
        if(strpos($root,'nstall') <= 0){
            $topdomain= $request->rootDomain();
            $authorized = str2arr(decode(sysconf('authorized')));
            if(!$authorized){
                $client_check='http://domainauth.qhxckj.com/api?key=client_check&domain='.request()->host().'&topdomain='.$topdomain;
                $check_info=file_get_contents($client_check);
                $authorized = json_decode($check_info,true);
                sysconf('authorized', encode(arr2str($authorized)));
                $authorized = [$authorized['code'],$authorized['error']];
            }
            if($authorized[0]){
                if($request->isAjax()){
                    return json(['code'=>0,'info'=>lang('think_lib_not_authorized')]);
                }else{
                    return response(lang('think_lib_not_authorized_html'));
                }
            }
        }
        
        //设置站点信息
        $request->siteid = SiteService::instance()->getSiteByDomain('','id');    
        return $next($request);
    }
}