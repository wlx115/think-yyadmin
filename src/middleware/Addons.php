<?php
// +----------------------------------------------------------------------
// | LHSystem
// +----------------------------------------------------------------------
// | 版权所有 2014~2020 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.yyinfos.com
// +----------------------------------------------------------------------
// | 作者：独角戏 <qhweb@foxmail.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace think\admin\middleware;

/**
 * 插件中间件
 * Class Check
 */
use think\App;
use think\exception\HttpException;

class Addons
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app  = $app;
    }

    /**
     * 插件中间件
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        // 注册控制器路由
        $this->app->route->rule("addons/:addon/[:controller]/[:action]", $this->route());
        $this->autoload();
        hook('addon_middleware', $request);
        return $next($request);
    }
    
    /**
     * 自动载入插件
     * @return bool
     */
    private function autoload()
    {
        // 插件类库自动载入
        spl_autoload_register(function ($class) {

            $class = ltrim($class, '\\');

            $dir = $this->app->getRootPath();
            $namespace = 'addons';

            if (strpos($class, $namespace) === 0) {
                $class = substr($class, strlen($namespace));
                $path = '';
                if (($pos = strripos($class, '\\')) !== false) {
                    $path = str_replace('\\', '/', substr($class, 0, $pos)) . '/';
                    $class = substr($class, $pos + 1);
                }
                $path .= str_replace('_', '/', $class) . '.php';
                $dir .= $namespace . $path;

                if (file_exists($dir)) {
                    include $dir;
                    return true;
                }

                return false;
            }

            return false;

        });
    }
    /**
     * 插件路由请求
     * @param null $addon
     * @param null $controller
     * @param null $action
     * @return mixed
     */
    private function route($addon = null, $controller = null, $action = 'index')
    {
        $request = $this->app->request;

        $this->app->event->trigger('addons_begin', $request);

        if (empty($addon) || empty($controller) || empty($action)) {
            throw new HttpException(500, lang('addon can not be empty'));
        }
        // 设置当前请求参数addon
        $request->addon = $this->name = $addon;
        // 设置当前请求的控制器、操作
        $request->setController($controller)->setAction($action);
        // 获取插件基础信息
        $info = $this->getAddonsInfo();
        if (!$info) {
            throw new HttpException(404, lang('addon %s not found', [$addon]));
        }
        if (!$info['status']) {
            throw new HttpException(500, lang('addon %s is disabled', [$addon]));
        }

        // 监听addon_module_init
        $this->app->event->trigger('addon_module_init', $request);

        //初始化控制器类
        if (!$class = $this->getAddonsClass($addon, 'controller', $controller)) {
            throw new HttpException(404, lang('addon controller %s not found', [Str::studly($controller)]));
        }

        // 重写视图基础路径
        $config = $this->app->config->get('view');
        $config['view_path'] = $this->getAddonsPath() . $addon . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR;
        $this->app->view->config($config);
        $this->app->config->set($config, 'view');

        // 生成控制器对象
        $instance = new $class($this->app);
        $vars = [];
        if (is_callable([$instance, $action])) {
            // 执行操作方法
            $call = [$instance, $action];
        } elseif (is_callable([$instance, '_empty'])) {
            // 空操作
            $call = [$instance, '_empty'];
            $vars = [$action];
        } else {
            // 操作不存在
            throw new HttpException(404, lang('addon action %s not found', [get_class($instance).'->'.$action.'()']));
        }
        $this->app->event->trigger('addons_action_begin', $call);

        return call_user_func_array($call, $vars);
    }
}