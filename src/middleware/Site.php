<?php
/**
 * 路由中间件
 * 站点切换功能
 */
namespace think\admin\middleware;
use think\admin\service\SiteService;
use think\admin\service\AdminService;

class Site
{
    public function handle($request, \Closure $next)
    {
        $host = $request->host();
        $SiteID = SiteService::instance()->getSiteId();
        $site = SiteService::instance()->getSiteByDomain($host);
        $appName = app('http')->getName();
        $param = $request->param();
        
        if($site){
            if(AdminService::instance()->isSuper() && ($appName != 'index' || isset($param['addon']))){
                $request->siteid = $SiteID ?? $site['id'];
                $request->siteId = $SiteID ?? $site['id'];
            }else{
                $request->siteid = $site['id'];
                $request->siteId = $site['id'];
            }
            $request->siteinfo = $site;
        }else{
            $request->siteid = $SiteID;
            $request->siteId = $SiteID;
            $request->siteinfo = SiteService::instance()->getSiteById($SiteID);
        }

        return $next($request);
    }
}