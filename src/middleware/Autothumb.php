<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare(strict_types=1);

namespace think\admin\middleware;

use think\admin\extend\ImageExtend;
/**
 * 缩略图
 * Class Autothumb
 * @package think\admin\middleware
 */
class Autothumb
{
    protected $allowext = ['png','jpg','bmp','jpeg','gif'];
    /**
     * 自动缩略图中间件
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $pathinfo = $request->pathinfo();
        $extension = pathinfo($pathinfo,PATHINFO_EXTENSION);
        if(!file_exists($pathinfo) && in_array($extension,$this->allowext)){
            return $this->compressImg($pathinfo);
        }
        return $next($request);
    }
    //图片压缩
    private function compressImg($path){
        $path = '/'.$path;
        $realPath = '/static/theme/img/image.png';
        $width = 88;
        //正则获取需要放缩的图片大小，格式：/file/abc.jpg.w320.jpg
        if (preg_match ( "/([^\.]+\.(png|jpg|jpeg|gif))\.w([\d]+)\.(png|jpg|jpeg|gif)/i", $path, $m )) {
            $realPath = $m [1];
            $width = $m [3];
        }
        $staticPath = realpath('.'.$realPath);
        if($staticPath && file_exists ( $staticPath )){
            return ImageExtend::showImage($realPath,$width);
        }else{
            return response(header("status: 404 Not Found"));
        }  
    }
}