/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : lsr_qhxckj

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-09-24 19:16:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for system_auth
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_auth`;
CREATE TABLE `yy_system_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '权限名称',
  `utype` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '身份权限',
  `desc` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注说明',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序权重',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '权限状态(1使用,0禁用)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_auth_status` (`status`) USING BTREE,
  KEY `idx_system_auth_title` (`title`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统-权限';

-- ----------------------------
-- Records of system_auth
-- ----------------------------

-- ----------------------------
-- Table structure for system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_auth_node`;
CREATE TABLE `yy_system_auth_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) unsigned DEFAULT '0' COMMENT '角色',
  `node` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '节点',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_auth_auth` (`auth`) USING BTREE,
  KEY `idx_system_auth_node` (`node`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统-授权';

-- ----------------------------
-- Records of system_auth_node
-- ----------------------------

-- ----------------------------
-- Table structure for system_base
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_base`;
CREATE TABLE `yy_system_base` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '数据类型',
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '数据代码',
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '数据名称',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '数据内容',
  `sort` bigint(20) DEFAULT '0' COMMENT '排序权重',
  `status` tinyint(1) DEFAULT '1' COMMENT '数据状态(0禁用,1启动)',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '删除状态(0正常,1已删)',
  `deleted_at` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '删除时间',
  `deleted_by` bigint(20) DEFAULT '0' COMMENT '删除用户',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_base_type` (`type`) USING BTREE,
  KEY `idx_system_base_code` (`code`) USING BTREE,
  KEY `idx_system_base_name` (`name`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='系统-字典';

-- ----------------------------
-- Records of system_base
-- ----------------------------
INSERT INTO `yy_system_base` VALUES ('1', '模块类型', 'article', '内容扩展模块', '', '0', '1', '0', '', '0', '2021-09-02 17:41:11');
INSERT INTO `yy_system_base` VALUES ('2', '模块类型', 'family', '居民管理模块', '', '0', '1', '0', '', '0', '2021-09-02 17:41:55');
INSERT INTO `yy_system_base` VALUES ('3', '模块类型', 'other', '独立模块', '', '0', '1', '0', '', '0', '2021-09-02 17:42:09');
INSERT INTO `yy_system_base` VALUES ('4', '身份权限', 'user', '普通用户', '', '0', '1', '0', '', '0', '2021-09-02 17:42:59');
INSERT INTO `yy_system_base` VALUES ('5', '身份权限', 'cun', '社区/村管理员', '', '0', '1', '0', '', '0', '2021-09-02 17:43:55');
INSERT INTO `yy_system_base` VALUES ('6', '身份权限', 'site_manage', '站点管理员', '', '0', '1', '0', '', '0', '2021-09-02 17:44:26');

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_config`;
CREATE TABLE `yy_system_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置分类',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置名称',
  `value` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置内容',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_config_type` (`type`) USING BTREE,
  KEY `idx_system_config_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='系统-配置';

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `yy_system_config` VALUES ('1', 'base', 'app_name', '鲁沙尔镇综合信息服务平台');
INSERT INTO `yy_system_config` VALUES ('2', 'base', 'app_version', 'v1');
INSERT INTO `yy_system_config` VALUES ('3', 'base', 'beian', '');
INSERT INTO `yy_system_config` VALUES ('4', 'base', 'miitbeian', '');
INSERT INTO `yy_system_config` VALUES ('5', 'base', 'site_copy', '©版权所有 2014-2020 云音信息');
INSERT INTO `yy_system_config` VALUES ('6', 'base', 'site_icon', 'https://v6.thinkadmin.top/upload/f4/7b8fe06e38ae9908e8398da45583b9.png');
INSERT INTO `yy_system_config` VALUES ('7', 'base', 'site_name', '鲁沙尔镇综合信息服务平台');
INSERT INTO `yy_system_config` VALUES ('8', 'base', 'xpath', 'admin');
INSERT INTO `yy_system_config` VALUES ('9', 'storage', 'allow_exts', 'doc,gif,icon,jpg,mp3,mp4,p12,pem,png,rar,xls,xlsx');
INSERT INTO `yy_system_config` VALUES ('10', 'storage', 'link_type', 'none');
INSERT INTO `yy_system_config` VALUES ('11', 'storage', 'local_http_domain', '');
INSERT INTO `yy_system_config` VALUES ('12', 'storage', 'local_http_protocol', 'follow');
INSERT INTO `yy_system_config` VALUES ('13', 'storage', 'qiniu_http_protocol', 'http');
INSERT INTO `yy_system_config` VALUES ('14', 'storage', 'txcos_http_protocol', 'http');
INSERT INTO `yy_system_config` VALUES ('15', 'storage', 'type', 'local');
INSERT INTO `yy_system_config` VALUES ('16', 'wechat', 'thr_appid', 'wx60a43dd8161666d4');
INSERT INTO `yy_system_config` VALUES ('17', 'wechat', 'thr_appkey', '7d0e4a487c6258b2232294b6ef0adb9e');
INSERT INTO `yy_system_config` VALUES ('18', 'wechat', 'type', 'thr');
INSERT INTO `yy_system_config` VALUES ('19', 'storage', 'alioss_http_protocol', 'http');
INSERT INTO `yy_system_config` VALUES ('40', 'base', 'site_host', 'http://lsr.qhxckj.com');
INSERT INTO `yy_system_config` VALUES ('41', 'base', 'authorized', '181c185w524z5o5t6h5y4k5m4n4w644j4s5j6j586k4y6h4x5a51605p6w5e6f0x18');

-- ----------------------------
-- Table structure for system_data
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_data`;
CREATE TABLE `yy_system_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '配置名',
  `value` longtext COLLATE utf8mb4_unicode_ci COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_data_name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统-数据';

-- ----------------------------
-- Records of system_data
-- ----------------------------

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_menu`;
CREATE TABLE `yy_system_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned DEFAULT '0' COMMENT '上级ID',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '菜单图标',
  `node` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '节点代码',
  `url` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '链接节点',
  `params` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '_self' COMMENT '打开方式',
  `sort` int(11) unsigned DEFAULT '0' COMMENT '排序权重',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_menu_status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='系统-菜单';

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `yy_system_menu` VALUES ('2', '0', '系统管理', '', '', '#', '', '_self', '100', '1', '2018-09-05 18:04:52');
INSERT INTO `yy_system_menu` VALUES ('3', '4', '系统菜单管理', 'layui-icon layui-icon-layouts', '', 'admin/menu/index', '', '_self', '1', '1', '2018-09-05 18:05:26');
INSERT INTO `yy_system_menu` VALUES ('4', '2', '系统配置', '', '', '#', '', '_self', '20', '1', '2018-09-05 18:07:17');
INSERT INTO `yy_system_menu` VALUES ('5', '12', '系统用户管理', 'layui-icon layui-icon-username', '', 'admin/user/index', '', '_self', '1', '1', '2018-09-06 11:10:42');
INSERT INTO `yy_system_menu` VALUES ('7', '12', '访问权限管理', 'layui-icon layui-icon-vercode', '', 'admin/auth/index', '', '_self', '2', '1', '2018-09-06 15:17:14');
INSERT INTO `yy_system_menu` VALUES ('8', '4', '数据字典管理', 'layui-icon layui-icon-code-circle', 'admin/base/index', 'admin/base/index', '', '_self', '2', '1', '2021-07-29 14:17:42');
INSERT INTO `yy_system_menu` VALUES ('11', '4', '系统参数配置', 'layui-icon layui-icon-set', '', 'admin/config/index', '', '_self', '5', '1', '2018-09-06 16:43:47');
INSERT INTO `yy_system_menu` VALUES ('12', '2', '权限管理', '', '', '#', '', '_self', '10', '1', '2018-09-06 18:01:31');
INSERT INTO `yy_system_menu` VALUES ('27', '4', '系统任务管理', 'layui-icon layui-icon-log', '', 'admin/queue/index', '', '_self', '4', '1', '2018-11-29 11:13:34');
INSERT INTO `yy_system_menu` VALUES ('49', '4', '系统日志管理', 'layui-icon layui-icon-form', '', 'admin/oplog/index', '', '_self', '3', '1', '2019-02-18 12:56:56');
INSERT INTO `yy_system_menu` VALUES ('56', '0', '微信管理', '', '', '#', '', '_self', '200', '1', '2019-12-09 11:00:37');
INSERT INTO `yy_system_menu` VALUES ('57', '56', '微信管理', '', '', '#', '', '_self', '0', '1', '2019-12-09 13:56:58');
INSERT INTO `yy_system_menu` VALUES ('58', '57', '微信接口配置', 'layui-icon layui-icon-set', '', 'wechat/config/options', '', '_self', '0', '1', '2019-12-09 13:57:28');
INSERT INTO `yy_system_menu` VALUES ('59', '57', '微信支付配置', 'layui-icon layui-icon-rmb', '', 'wechat/config/payment', '', '_self', '0', '1', '2019-12-09 13:58:42');
INSERT INTO `yy_system_menu` VALUES ('60', '56', '微信定制', '', '', '#', '', '_self', '0', '1', '2019-12-09 18:35:16');
INSERT INTO `yy_system_menu` VALUES ('61', '60', '微信粉丝管理', 'layui-icon layui-icon-username', '', 'wechat/fans/index', '', '_self', '0', '1', '2019-12-09 18:35:37');
INSERT INTO `yy_system_menu` VALUES ('62', '60', '微信图文管理', 'layui-icon layui-icon-template-1', '', 'wechat/news/index', '', '_self', '0', '1', '2019-12-09 18:43:51');
INSERT INTO `yy_system_menu` VALUES ('63', '60', '微信菜单配置', 'layui-icon layui-icon-cellphone', '', 'wechat/menu/index', '', '_self', '0', '1', '2019-12-09 22:49:28');
INSERT INTO `yy_system_menu` VALUES ('64', '60', '回复规则管理', 'layui-icon layui-icon-engine', '', 'wechat/keys/index', '', '_self', '0', '1', '2019-12-14 14:09:04');
INSERT INTO `yy_system_menu` VALUES ('65', '60', '关注回复配置', 'layui-icon layui-icon-senior', '', 'wechat/keys/subscribe', '', '_self', '0', '1', '2019-12-14 14:10:31');
INSERT INTO `yy_system_menu` VALUES ('66', '60', '默认回复配置', 'layui-icon layui-icon-util', '', 'wechat/keys/defaults', '', '_self', '0', '1', '2019-12-14 14:11:18');
INSERT INTO `yy_system_menu` VALUES ('67', '0', '控制台', '', '', '#', '', '_self', '300', '1', '2020-07-13 06:51:46');
INSERT INTO `yy_system_menu` VALUES ('68', '67', '数据管理', '', '', '#', '', '_self', '301', '1', '2020-07-13 06:51:54');
INSERT INTO `yy_system_menu` VALUES ('99', '60', '关注自动回复', 'layui-icon layui-icon-release', 'wechat/auto/index', 'wechat/auto/index', '', '_self', '0', '1', '2021-04-10 15:56:54');
INSERT INTO `yy_system_menu` VALUES ('100', '0', '首页', '', 'admin/module/index', 'admin/module/index', '', '_self', '0', '1', '2021-09-02 09:24:34');
INSERT INTO `yy_system_menu` VALUES ('101', '4', '系统模块管理', 'layui-icon layui-icon-windows', 'admin/module/index', 'admin/module/index', '', '_self', '0', '0', '2021-09-02 14:05:04');
INSERT INTO `yy_system_menu` VALUES ('102', '4', '自定义模块管理', 'layui-icon layui-icon-windows', 'data/sys.module/index', 'data/sys.module/index', '', '_self', '0', '1', '2021-09-02 16:41:55');
INSERT INTO `yy_system_menu` VALUES ('103', '68', '家庭成员管理', '', 'data/sys.extend/index', 'data/sys.extend/index', 'mid=20', '_self', '0', '1', '2021-09-02 17:30:15');
INSERT INTO `yy_system_menu` VALUES ('104', '68', '生产生活条件', '', 'data/sys.extend/index', 'data/sys.extend/index', 'mid=19', '_self', '0', '1', '2021-09-02 17:38:12');
INSERT INTO `yy_system_menu` VALUES ('105', '4', '系统地区管理', 'layui-icon layui-icon-share', 'common/region/index', 'common/region/index', '', '_self', '0', '1', '2021-09-16 16:32:14');
INSERT INTO `yy_system_menu` VALUES ('106', '4', '上传附件管理', 'layui-icon layui-icon-picture', 'common/attachment/index', 'common/attachment/index', '', '_self', '0', '1', '2021-09-16 16:51:35');

-- ----------------------------
-- Table structure for system_oplog
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_oplog`;
CREATE TABLE `yy_system_oplog` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `geoip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `action` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作行为名称',
  `content` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作内容描述',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统-日志';


-- ----------------------------
-- Table structure for system_queue
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_queue`;
CREATE TABLE `yy_system_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务编号',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `command` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '执行指令',
  `exec_pid` bigint(20) DEFAULT '0' COMMENT '执行进程',
  `exec_data` longtext COLLATE utf8mb4_unicode_ci COMMENT '执行参数',
  `exec_time` bigint(20) DEFAULT '0' COMMENT '执行时间',
  `exec_desc` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '执行描述',
  `enter_time` decimal(20,4) DEFAULT '0.0000' COMMENT '开始时间',
  `outer_time` decimal(20,4) DEFAULT '0.0000' COMMENT '结束时间',
  `loops_time` bigint(20) DEFAULT '0' COMMENT '循环时间',
  `attempts` bigint(20) DEFAULT '0' COMMENT '执行次数',
  `rscript` tinyint(1) DEFAULT '1' COMMENT '任务类型(0单例,1多例)',
  `status` tinyint(1) DEFAULT '1' COMMENT '任务状态(1新任务,2处理中,3成功,4失败)',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_queue_code` (`code`) USING BTREE,
  KEY `idx_system_queue_title` (`title`) USING BTREE,
  KEY `idx_system_queue_status` (`status`) USING BTREE,
  KEY `idx_system_queue_rscript` (`rscript`) USING BTREE,
  KEY `idx_system_queue_create_at` (`create_at`) USING BTREE,
  KEY `idx_system_queue_exec_time` (`exec_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='系统-任务';

-- ----------------------------
-- Records of system_queue
-- ----------------------------

-- ----------------------------
-- Table structure for system_region
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_region`;
CREATE TABLE `yy_system_region` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) unsigned DEFAULT '0' COMMENT '父级ID',
  `zcode` varchar(20) DEFAULT '' COMMENT '行政区划代码',
  `name` varchar(100) DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态',
  `sort` int(10) unsigned DEFAULT '0' COMMENT '排序',
  `level` int(11) DEFAULT '1' COMMENT '级别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统-地区';


-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `yy_system_user`;
CREATE TABLE `yy_system_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `usertype` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户类型',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户账号',
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户密码',
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户昵称',
  `headimg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像地址',
  `authorize` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '权限授权',
  `contact_qq` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '联系QQ',
  `contact_mail` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '联系邮箱',
  `contact_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '联系手机',
  `login_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '登录地址',
  `login_at` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '登录时间',
  `login_num` bigint(20) DEFAULT '0' COMMENT '登录次数',
  `describe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注说明',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `sort` bigint(20) DEFAULT '0' COMMENT '排序权重',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '删除(1删除,0未删)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_system_user_status` (`status`) USING BTREE,
  KEY `idx_system_user_username` (`username`) USING BTREE,
  KEY `idx_system_user_deleted` (`is_deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='系统-用户';

-- ----------------------------
-- Table structure for sys_extend_shenghuo
-- ----------------------------
DROP TABLE IF EXISTS `yy_sys_extend_shenghuo`;
CREATE TABLE `yy_sys_extend_shenghuo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yinhualu` varchar(32) DEFAULT NULL COMMENT '硬化路是否到户',
  `cesuo` varchar(32) DEFAULT NULL COMMENT '有无卫生厕所',
  `zhufang` varchar(32) DEFAULT NULL COMMENT '住房是否安全',
  `yongdian` varchar(32) DEFAULT NULL COMMENT '是否通生活用电',
  `gengdi` decimal(20,2) DEFAULT NULL COMMENT '耕地面积',
  `guanggai` decimal(20,2) DEFAULT NULL COMMENT '灌溉面积',
  `lindi` decimal(20,2) DEFAULT NULL COMMENT '林地面积',
  `tghl` decimal(20,2) DEFAULT NULL COMMENT '退耕还林',
  `linguo` decimal(20,2) DEFAULT NULL COMMENT '林果面积',
  `mucao` decimal(20,2) DEFAULT NULL COMMENT '牧草地面积',
  `shuimian` decimal(20,2) DEFAULT NULL COMMENT '水面面积',
  `yinshui` varchar(32) DEFAULT NULL COMMENT '饮水是否安全',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_sys_extend_shenghuo_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='生产生活条件';

-- ----------------------------
-- Records of sys_extend_shenghuo
-- ----------------------------

-- ----------------------------
-- Table structure for sys_extend_user
-- ----------------------------
DROP TABLE IF EXISTS `yy_sys_extend_user`;
CREATE TABLE `yy_sys_extend_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `ucode` varchar(128) DEFAULT NULL COMMENT '人编号',
  `name` varchar(128) DEFAULT NULL COMMENT '姓名',
  `idcard` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `sex` varchar(32) DEFAULT NULL COMMENT '性别',
  `minzu` varchar(32) DEFAULT NULL COMMENT '民族',
  `tel` varchar(128) DEFAULT NULL COMMENT '联系电话',
  `company` varchar(128) DEFAULT NULL COMMENT '工作单位',
  `relationship` varchar(32) DEFAULT NULL COMMENT '与户主关系',
  `political` varchar(32) DEFAULT NULL COMMENT '政治面貌',
  `marital` varchar(32) DEFAULT NULL COMMENT '婚姻状况',
  `hold_type` varchar(32) DEFAULT NULL COMMENT '户口性质',
  `residence` varchar(128) DEFAULT NULL COMMENT '户籍地',
  `edu_status` varchar(32) DEFAULT NULL COMMENT '教育状态',
  `work_status` varchar(32) DEFAULT NULL COMMENT '就业状态',
  `tech_level` varchar(32) DEFAULT NULL COMMENT '技能等级',
  `income` varchar(32) DEFAULT NULL COMMENT '收入情况',
  `yanglao` varchar(32) DEFAULT NULL COMMENT '养老保险办理情况',
  `yanglao_money` decimal(20,2) DEFAULT NULL COMMENT '居民养老金额',
  `yiliao` varchar(32) DEFAULT NULL COMMENT '医疗保险办理情况',
  `yiliao_money` decimal(20,2) DEFAULT NULL COMMENT '医保缴纳金额',
  `disability_type` varchar(32) DEFAULT NULL COMMENT '残疾类别',
  `disability_level` varchar(32) DEFAULT NULL COMMENT '残疾级别',
  `hcode` varchar(128) DEFAULT NULL COMMENT '户编号 ',
  `ttt` decimal(20,2) DEFAULT NULL COMMENT 'ttt ',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_sys_extend_user_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='家庭成员';

-- ----------------------------
-- Records of sys_extend_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_field
-- ----------------------------
DROP TABLE IF EXISTS `yy_sys_field`;
CREATE TABLE `yy_sys_field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mid` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(32) DEFAULT '' COMMENT '标识名',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '名称',
  `tips` varchar(500) NOT NULL DEFAULT '' COMMENT '说明',
  `options` text,
  `default` varchar(32) NOT NULL DEFAULT '' COMMENT '默认值',
  `hidden` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '隐藏',
  `list` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '列表',
  `search` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'search',
  `required` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'required',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  `define` varchar(255) DEFAULT NULL COMMENT '字段属性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网格化扩展模块字段';

-- ----------------------------
-- Table structure for sys_module
-- ----------------------------
DROP TABLE IF EXISTS `yy_sys_module`;
CREATE TABLE `yy_sys_module` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) DEFAULT '' COMMENT '扩展标识名',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '扩展名称',
  `tips` varchar(500) NOT NULL DEFAULT '' COMMENT '扩展说明',
  `type` varchar(20) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='网格化扩展模块';

-- ----------------------------
-- Table structure for sys_upload
-- ----------------------------
DROP TABLE IF EXISTS `yy_sys_upload`;
CREATE TABLE `yy_sys_upload` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT '0' COMMENT '上传用户',
  `name` varchar(50) DEFAULT NULL COMMENT '图片名称',
  `key` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `ext` varchar(10) DEFAULT NULL COMMENT '扩展名',
  `geoip` varchar(15) DEFAULT NULL COMMENT '上传者IP',
  `size` bigint(20) DEFAULT '0' COMMENT '图片分组',
  `md5` varchar(50) DEFAULT NULL COMMENT '图片MD5',
  `type` varchar(20) DEFAULT NULL COMMENT '上传类型image,file',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传时间',
  PRIMARY KEY (`id`),
  KEY `idx_system_upload_url` (`key`) USING BTREE,
  KEY `idx_system_upload_uid` (`uid`),
  KEY `idx_system_upload_group` (`size`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `yy_user_token`;
CREATE TABLE `yy_user_token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT '0' COMMENT '用户UID',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '授权类型',
  `time` bigint(20) DEFAULT '0' COMMENT '有效时间',
  `token` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '授权令牌',
  `tokenv` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '授权验证',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_data_user_token_type` (`type`) USING BTREE,
  KEY `idx_data_user_token_time` (`time`) USING BTREE,
  KEY `idx_data_user_token_token` (`token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='数据-用户-认证';

-- ----------------------------
-- Table structure for wechat_auto
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_auto`;
CREATE TABLE `yy_wechat_auto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '类型(text,image,news)',
  `time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '延迟时间',
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '消息编号',
  `appid` char(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '公众号APPID',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '文本内容',
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片链接',
  `voice_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '语音链接',
  `music_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐标题',
  `music_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐链接',
  `music_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '缩略图片',
  `music_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐描述',
  `video_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频标题',
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频URL',
  `video_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频描述',
  `news_id` bigint(20) unsigned DEFAULT NULL COMMENT '图文ID',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `create_by` bigint(20) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_wechat_auto_type` (`type`) USING BTREE,
  KEY `idx_wechat_auto_keys` (`time`) USING BTREE,
  KEY `idx_wechat_auto_code` (`code`) USING BTREE,
  KEY `idx_wechat_auto_appid` (`appid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-回复';

-- ----------------------------
-- Records of wechat_auto
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_fans
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_fans`;
CREATE TABLE `yy_wechat_fans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '公众号APPID',
  `unionid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '粉丝unionid',
  `openid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '粉丝openid',
  `tagid_list` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '粉丝标签id',
  `is_black` tinyint(1) unsigned DEFAULT '0' COMMENT '是否为黑名单状态',
  `subscribe` tinyint(1) unsigned DEFAULT '0' COMMENT '关注状态(0未关注,1已关注)',
  `nickname` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户昵称',
  `sex` tinyint(1) unsigned DEFAULT '0' COMMENT '用户性别(1男性,2女性,0未知)',
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户所在国家',
  `province` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户所在省份',
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户所在城市',
  `language` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户的语言(zh_CN)',
  `headimgurl` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户头像',
  `subscribe_time` bigint(20) unsigned DEFAULT '0' COMMENT '关注时间',
  `subscribe_at` datetime DEFAULT NULL COMMENT '关注时间',
  `remark` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '备注',
  `subscribe_scene` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '扫码关注场景',
  `qr_scene` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '二维码场景值',
  `qr_scene_str` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '二维码场景内容',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_fans_openid` (`openid`) USING BTREE,
  KEY `index_wechat_fans_unionid` (`unionid`) USING BTREE,
  KEY `index_wechat_fans_isblack` (`is_black`) USING BTREE,
  KEY `index_wechat_fans_subscribe` (`subscribe`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-粉丝';

-- ----------------------------
-- Records of wechat_fans
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_fans_tags
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_fans_tags`;
CREATE TABLE `yy_wechat_fans_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `appid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '公众号APPID',
  `name` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标签名称',
  `count` bigint(20) unsigned DEFAULT '0' COMMENT '总数',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  KEY `index_wechat_fans_tags_id` (`id`) USING BTREE,
  KEY `index_wechat_fans_tags_appid` (`appid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-标签';

-- ----------------------------
-- Records of wechat_fans_tags
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_keys
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_keys`;
CREATE TABLE `yy_wechat_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appid` char(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '公众号APPID',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '类型(text,image,news)',
  `keys` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关键字',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '文本内容',
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '图片链接',
  `voice_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '语音链接',
  `music_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐标题',
  `music_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐链接',
  `music_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '缩略图片',
  `music_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '音乐描述',
  `video_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频标题',
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频URL',
  `video_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '视频描述',
  `news_id` bigint(20) unsigned DEFAULT NULL COMMENT '图文ID',
  `sort` bigint(20) unsigned DEFAULT '0' COMMENT '排序字段',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(0禁用,1启用)',
  `create_by` bigint(20) unsigned DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_keys_appid` (`appid`) USING BTREE,
  KEY `index_wechat_keys_type` (`type`) USING BTREE,
  KEY `index_wechat_keys_keys` (`keys`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-规则';

-- ----------------------------
-- Records of wechat_keys
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_media
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_media`;
CREATE TABLE `yy_wechat_media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `md5` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文件md5',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '媒体类型',
  `appid` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '公众号ID',
  `media_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '本地文件链接',
  `media_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '远程图片链接',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_media_appid` (`appid`) USING BTREE,
  KEY `index_wechat_media_md5` (`md5`) USING BTREE,
  KEY `index_wechat_media_type` (`type`) USING BTREE,
  KEY `index_wechat_media_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-素材';

-- ----------------------------
-- Records of wechat_media
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_news
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_news`;
CREATE TABLE `yy_wechat_news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `media_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '永久素材MediaID',
  `local_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '永久素材外网URL',
  `article_id` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '关联图文ID(用英文逗号做分割)',
  `is_deleted` tinyint(1) unsigned DEFAULT '0' COMMENT '删除状态(0未删除,1已删除)',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_wechat_news_artcle_id` (`article_id`) USING BTREE,
  KEY `index_wechat_news_media_id` (`media_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-图文';

-- ----------------------------
-- Records of wechat_news
-- ----------------------------

-- ----------------------------
-- Table structure for wechat_news_article
-- ----------------------------
DROP TABLE IF EXISTS `yy_wechat_news_article`;
CREATE TABLE `yy_wechat_news_article` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '素材标题',
  `local_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '永久素材显示URL',
  `show_cover_pic` tinyint(4) unsigned DEFAULT '0' COMMENT '显示封面(0不显示,1显示)',
  `author` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '文章作者',
  `digest` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '摘要内容',
  `content` longtext COLLATE utf8mb4_unicode_ci COMMENT '图文内容',
  `content_source_url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '原文地址',
  `read_num` bigint(20) unsigned DEFAULT '0' COMMENT '阅读数量',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='微信-文章';

-- ----------------------------
-- Records of wechat_news_article
-- ----------------------------
