<?php

namespace think\admin\install;

use think\App;
use think\Request;
use think\facade\Config;
use think\facade\Db;
use think\facade\View;
use think\admin\Library;


class Index
{
    public $app;
    public $request;
    protected $_title;
    protected $_appName;
    protected $_appVersion;
    protected $_headJeep;
    protected $_tips_title;
    protected $_tips_installed;
    protected $_tips_phpinfo;
    protected $_tips_info;
    protected $_ad;
    protected $_website;
    protected $_support;
    protected $_copyright;
    protected $_icp;
    protected $_gitee;
    protected $_homepage;
    protected $_adminpage;
    protected $_outData = [];
    private $_verHash;
    private $_agreement;
    protected $_table_pre = 'yy_';
    protected $config = [];


    public function __construct(App $app)
    {
        //初始化相关数据
        @set_time_limit(1000);
        $this->app = $app;
        $this->request = $app->request;
        
        //软件名称
        $this->_appName = lang('i_app_name');
        //软件版本号
        $this->_appVersion = Library::VERSION;
        //网页标题
        $this->_title = $this->_appName . $this->_appVersion;
        //网页自动跳转
        $this->_headJeep = '';
        //网页提示主标题
        $this->_tips_title = $this->_title;
        //网页提示已安装内容
        $this->_tips_installed = lang('i_tips_installed', ['根目录', 'install.lock']);
        //网页PHP版本过低提示内容
        $this->_tips_phpinfo = lang('i_tips_phpinfo', [' <b style="color: red">7.1.0</b> ']);
        //网页PHP版本过低提示内容简要
        $this->_tips_info = '';
        //页面加载广告
        $this->_ad = lang('i_ads', ['www.cnzaz.com/ads/baidu/640_60.html', 'baiduad']);
        //官网
        $this->_website = lang('i_official_Website', ['//www.yyinfos.com', '云音信息']);
        //码云
        $this->_gitee = lang('i_gitee', ['//gitee.com/qhweb/think-lib', '开源下载']);
        //技术支持
        $this->_support = lang('i_technical_support', ['//www.yyinfos.com', '云音信息']);
        //首页
        $this->_homepage = lang('i_homepage', ['//'.$this->request->host(), '访问网站首页']);
        //后台
        $this->_adminpage = lang('i_adminpage', ['//'.$this->request->host().'/admin', '访问网站后台']);
        //版权
        $this->_copyright = lang('i_copyright', ['2017-2099', 'yyinfos.com']);
        //备案
        $this->_icp = lang('i_icp', ['青ICP备12059123号']);
        //hash码
        $this->_verHash = substr(sha1(time()), mt_rand(0, 10), 3);
        //软件许可协议
        $this->_agreement = lang('i_agreement', [
            //公司名称
            'company' => '青海云音信息技术有限公司',
            //公司简称
            'companyabbreviation' => '云音信息',
            //软件名称
            'softname' => $this->_appName,
            //软件简称
            'softAbbreviation' => 'YYAdmin',
            //公司官网
            'website' => 'www.yyinfos.com',
            //服务器所在地
            'serverLocation' => '阿里云',
            //版权时效
            'copyrightTime' => '2017-2099'
        ]);
        //页面返回的处理数据
        $this->_outData = [
            'appName' => $this->_appName,
            'appVersion' => $this->_appVersion,
            'title' => $this->_title . lang('i_app_guide'),
            'headJeep' => $this->_headJeep,
            'tips_title' => $this->_tips_title,
            'ad' => $this->_ad,
            'website' => $this->_website,
            'support' => $this->_support,
            'gitee' => $this->_gitee,
            'copyright' => $this->_copyright,
            'icp' => $this->_icp,
            'homepage'=>$this->_homepage,
            'adminpage'=>$this->_adminpage,
            'verHash' => $this->_verHash,
            'agreement' => $this->_agreement,
        ];
        $this->config = [
            'env_items'=>[
                'os' => array('c' => 'PHP_OS','n'=>'操作系统', 'r' => 'noRestriction', 'b' => '建议使用Linux系统以提升程序性能','rs'=>'正在检测'),
                'server' => array('c' => 'SERVER_SOFTWARE','n'=>'服务器环境', 'r' => 'apache/nginx', 'b' => 'apache2.0以上/nginx1.6以上','rs'=>'正在检测'),
                'php' => array('c' => 'PHP_VERSION','n'=>'PHP版本', 'r' => '7.1.0', 'b' => '>7.1.0','rs'=>'正在检测'),
                'pdo' => array('c' => 'pdo','n'=>'PDO扩展', 'r' => '启用', 'b' => '必须开启','rs'=>'正在检测'),
                'session' => array('c' => 'session','n'=>'session', 'r' => '开启', 'b' => '开启 session.auto_start','rs'=>'正在检测'),
                'safe_mode' => array('c' => 'safe_mode','n'=>'safe_mode', 'r' => 'noRestriction', 'b' => '基础配置','rs'=>'正在检测'),
        
        
                'gd' => array('c' => 'gd_info','n'=>'GD库', 'r' => '1.0', 'b' => '必须开启','rs'=>'正在检测'),
                'curl' => array('c' => 'curl','n'=>'CURL库', 'r' => '启用', 'b' => '必须扩展','rs'=>'正在检测'),
                'fileinfo' => array('c' => 'finfo_open','n'=>'fileinfo库', 'r' => '启用', 'b' => '必须扩展','rs'=>'正在检测'),
                'openssl' => array('c' => 'openssl','n'=>'openssl库', 'r' => '启用', 'b' => '必须扩展','rs'=>'正在检测'),
                'bcmath' => array('c' => 'bcmath','n'=>'bcmath库', 'r' => '启用', 'b' => '必须扩展','rs'=>'正在检测'),
        
                'file_uploads' => array('c' => 'file_uploads','n'=>'附件上传', 'r' => 'noRestriction', 'b' => '>2M','rs'=>'正在检测'),
        
                'disk_space' => array('c' => 'disk_free_space','n'=>'硬盘空间', 'r' => format_bytes(30 * 1048576), 'b' => '>100MB','rs'=>'正在检测'),
        
            ],
            'dir_items'=>[
                'config/database.php',
                'public',
                'runtime',
                'runtime/cache',
                'data',
            ],
            'function_items'=>[
                'pdo_mysql' => array('c' => 'pdo_mysql','n'=>'pdo_mysql()', 'b' => '必须','rs'=>'正在检测'),
                'file_put_contents' => array('c' => 'file_put_contents','n'=>'file_put_contents()',  'b' => '必须'),
                'file_get_contents' => array('c' => 'file_get_contents','n'=>'file_get_contents()',  'b' => '必须'),
                'xml_parser_create' => array('c' => 'xml_parser_create','n'=>'xml_parser_create()',  'b' => '必须'),
                'gethostbyname' => array('c' => 'gethostbyname','n'=>'gethostbyname()',  'b' => '必须'),
                'fsockopen' => array('c' => 'fsockopen','n'=>'fsockopen()',  'b' => '必须'),
                'imagettftext' => array('c' => 'imagettftext','n'=>'imagettftext()',  'b' => '必须'),
            ],
            'data_items'=>[
                'mysql'=>[
                    'name'=>'数据库连接设置',
                    'host'=>'localhost',
                    'port'=>3306,
                    'user'=>'root',
                    'password'=>'root',
                    'id'=>'mysql',
                    'dbname'=>'yy_admin',
                    'prefix'=>'yy_',
                    'dbfile'=>'install.sql',
                ],
            ]
        ];        
        // 设置安装静态资源和模板目录
        $this->checkStaticFile();
    }
    public function _empty()
    {
        return $this->index();
    }
    /**
     * 检查静态资源
     */
    private function checkStaticFile()
    {
        $root_path = $this->app->getRootPath();
        $app_path = $this->app->getBasePath();
        $install_path = dirname(__DIR__).DIRECTORY_SEPARATOR.'install'.DIRECTORY_SEPARATOR;
        $static_path = public_path() . "static".DIRECTORY_SEPARATOR."install".DIRECTORY_SEPARATOR;
        // 检测程序安装
        if (!is_dir($root_path . "data")) {
            mkdir($root_path . "data");
        }
        //安装静态资源文件
        if (!is_dir($static_path) && !file_exists($root_path . "./data/copyInstall.lock")) {
            copyDir($install_path . "assets", public_path() . "static".DIRECTORY_SEPARATOR."install");
            $flag = @touch($root_path . './data/copyInstall.lock');
        }
        //设置模板路径
        $this->view = View::engine('Think');
        $this->view->config([
            'view_path' => $install_path . 'template'.DIRECTORY_SEPARATOR,
            'tpl_replace_string'       => [
                '{__PUBLIC_PATH__}' =>  '/',              //public 目录
                '{__STATIC_INSTALL_PATH__}' =>  '/static/install/',       //全局静态目录
                '{__ADMIN_PATH__}'  =>  '/admin',        //后台目录
            ]
        ]);
    }

    //基本环境检测
    private function check($r = "", $type = '')
    {
        if (file_exists(root_path('data') . './install.lock') || phpversion() < '7.1.0') {

            if (phpversion() < '7.1.0') {
                //dump($this->_outData);
                $this->_outData['headJeep'] = lang('i_head_jeep', ['50', '/']);
                $this->_outData['tips_info'] = $this->_tips_phpinfo;
                $this->_outData['tips_subheading'] = lang('i_tips_php');

            }
            if (file_exists(root_path('data') . './install.lock')) {
                $this->_outData['headJeep'] = lang('i_head_jeep', ['50', '/']);
                $this->_outData['tips_info'] = $this->_tips_installed;
                $this->_outData['tips_subheading'] = lang('i_tips_install');

            }

            return view('\tips', $this->_outData);

        }

        $check = $r->checkToken('__token__');
        //dump($check);

        if ($type == 'checkdb') {
            if (false === $check || !$r->param('dbcheckok')) {
                $this->_outData['headJeep'] = lang('i_head_jeep', ['50', '/']);
                $this->_outData['tips_info'] = "";
                $this->_outData['tips_subheading'] = "非法访问，本页不能直接访问！";
                return view('\tips', $this->_outData);
            }

        }

        return true;


    }

    //安装防跳步骤
    private function checkdbok($r)
    {

        if (!$r->param('dbcheckok')) {
            $this->_outData['headJeep'] = lang('i_head_jeep', ['50', '/']);
            $this->_outData['tips_info'] = "";
            $this->_outData['tips_subheading'] = "非法访问，本页不能直接访问！";
            return view('\tips', $this->_outData);
        }


        return true;
    }
    private function checkFinishOk($r)
    {
        $check = $r->checkToken('__token__');
        //dump($check);
        if ($check) {
            $this->_outData['headJeep'] = lang('i_head_jeep', ['50', '/']);
            $this->_outData['tips_info'] = "";
            $this->_outData['tips_subheading'] = "非法访问，本页不能直接访问！";
            return view('\tips', $this->_outData);
        }


        return true;
    }

    //安装默认入口
    public function index(Request $r)
    {
        if ($this->check($r) !== true) {
            return $this->check($r);
        }
        return view('\main', $this->_outData);
    }

    //页面定位测试
    public function hello($name = 'word')
    {
        return 'install hello,' . $name;
    }

    //环境变量一重检测
    private function env_item_check($item)
    {
        $item = array_merge($item, [
            'r' => $item['r'] == 'noRestriction' ? lang('noRestriction') : $item['r'],
            'b' => $item['b'] == 'noRestriction' ? lang('noRestriction') : $item['b'],
            'rs' => self::env_item_check_id($item)
        ]);

        return $item;

    }

    //环境变量二重检测
    private function env_item_check_id($c)
    {

        switch ($c['c']) {
            case 'PHP_OS':
                return '<span class="correct_span">&radic;</span> ' . PHP_OS;
                break;
            case 'SERVER_SOFTWARE':
                return '<span class="correct_span">&radic;</span> ' . $_SERVER[$c['c']];
                break;
            case 'PHP_VERSION':
                if (PHP_VERSION < $c['r']) {
                    //记录没达到环境要求标志,存入session

                    session('install_check_err', true);
                    return '<span class="error_span">&radic;</span> ' . PHP_VERSION;
                } else {
                    return '<span class="correct_span">&radic;</span> ' . PHP_VERSION;
                }
                break;
            case 'pdo':
                if (extension_loaded('pdo')) {
                    return '<span class="correct_span">&radic;</span> 已安装';
                } else {

                    session('install_check_err', true);
                    return '<span class="correct_span error_span">&radic;</span> 请安装PDO扩展';
                }
                break;
            case 'session':
                if (function_exists('session_start')) {
                    if (ini_get('session.auto_start')) {
                        return '<font color=green><i class="fa fa-check-square-o"></i> On</font>';
                    } else {
                        return '<span class="correct_span">&radic;</span> 已安装 <font color=#9acd32><i class="fa fa-exclamation-triangle"></i> session.auto_start Off</font>';
                    }

                } else {
                    session('install_check_err', true);
                    return '<span class="correct_span error_span">&radic;</span> 请安装SESSION扩展';
                }
                break;
            case 'safe_mode':
                if (ini_get('safe_mode')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On</font>';
                } else {
                    return '<font color=#9acd32><i class="fa fa-exclamation-triangle"></i> Off</font> 忽略';
                }
                break;
            case 'file_uploads':
                return @ini_get('file_uploads') ? '<span class="correct_span">&radic;</span> ' . ini_get('upload_max_filesize') : '<span class="error_span">&radic;</span> ' . lang('unknown');
                break;
            case 'disk_free_space':
                if (function_exists('disk_free_space')) {
                    return disk_free_space(root_path()) >= mitobyte($c['r']) ? '<span class="correct_span">&radic;</span> ' . format_bytes(disk_free_space(root_path())) : '<span class="error_span">&radic;</span> ' . format_bytes(disk_free_space(root_path()));
                } else {
                    return '<span class="error_span">&radic;</span> ' . lang('unknown');
                }
                break;
            case 'gd_info':
                $tmp = function_exists('gd_info') ? gd_info() : array();
                if (empty($tmp['GD Version'])) {

                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-exclamation"></i> Off</font>';

                } else {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On</font> ' . $tmp['GD Version'];
                }
                break;
            case 'curl':
                if (extension_loaded('curl')) {
                    if (function_exists('curl_init')) {
                        return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                    } else {
                        session('install_check_err', true);
                        return '<font color=red><span class="correct_span">&radic;</span> 已安装 <i class="fa fa-exclamation"></i> Off 不支持</font>';
                    }

                } else {
                    session('install_check_err', true);
                    return '<span class="error_span">&radic;</span> 未安装';
                }
                break;
            case 'finfo_open':
                if (function_exists('finfo_open')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><span class="correct_span">&radic;</span> 已安装 <i class="fa fa-exclamation"></i> Off 不支持</font>';
                }

                break;
            case 'openssl':
                if (extension_loaded('openssl')) {
                    if (function_exists('openssl_encrypt')) {
                        return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                    } else {
                        session('install_check_err', true);
                        return '<font color=red><span class="correct_span">&radic;</span> 已安装 <i class="fa fa-close"></i> Off 不支持</font>';
                    }

                } else {
                    session('install_check_err', true);
                    return '<span class="error_span">&radic;</span> 未安装';
                }
                break;
            case 'bcmath':
                if (function_exists('bcadd')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }

                break;
            case 'opcache_get_configuration':
                if (function_exists('opcache_get_configuration')) {
                    if (opcache_get_configuration()['directives']['opcache.enable']) {
                        return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                    } else {
                        session('install_check_err', true);
                        return '<font color=red><span class="correct_span">&radic;</span> 已安装 <i class="fa fa-close"></i> Off 不支持</font>';
                    }

                } else {
                    session('install_check_err', true);
                    return '<span class="error_span">&radic;</span> 未安装';
                }
                break;
            case 'pdo_mysql':
                if (extension_loaded('pdo_mysql')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'file_put_contents':
                if (function_exists('file_put_contents')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'file_get_contents':
                if (function_exists('file_get_contents')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'xml_parser_create':
                if (function_exists('xml_parser_create')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'gethostbyname':
                if (function_exists('gethostbyname')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'fsockopen':
                if (function_exists('fsockopen')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;
            case 'imagettftext':
                if (function_exists('imagettftext')) {
                    return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
                } else {
                    session('install_check_err', true);
                    return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
                }
                break;

            default:
                session('install_check_err', true);
                return '<span class="error_span">&radic;</span>  无此检测项数据！';
        }

    }

    //环境变量检测
    private function env_check()
    {
        define('PHP_EDITION', '7.1.0');
        $reData = [];
        $err = 0;
        $env_items = $this->config['env_items'];
        session('install_check_err', null);
        foreach ($env_items as $key => $item) {
            $reData[$key] = self::env_item_check($item);
        }

        return $reData;

    }

    //依赖函数检测
    private function function_check()
    {
        $reData = [];
        $function__items = $this->config['function_items'];

        session('install_check_err', null);
        foreach ($function__items as $key => $item) {
            $reData[$key] = self::function_item_check($item);
        }
        return $reData;

    }

    //依赖函数一重检测
    private function function_item_check($item)
    {
        $item = array_merge($item, [
            'b' => $item['b'] == 'noRestriction' ? lang('noRestriction') : $item['b'],
            'rs' => self::function_item_check_id($item['c'])
        ]);

        return $item;

    }

    //依赖函数二重检测
    private function function_item_check_id($func_item)
    {

        if (function_exists($func_item) || extension_loaded($func_item)) {
            return '<font color=green><i class="fa fa-check-square-o"></i> On 支持</font> ';
        } else {
            session('install_check_err', true);
            return '<font color=red><i class="fa fa-close"></i> Off 不支持</font>';
        }
    }
    //test


    //安装步骤检测，防止跳步骤操作
    public function check_step($complete_step, $current_step = '')
    {
        //halt($complete_step);

        switch ($complete_step) {
            case 'start':
                if ($current_step !== 'check') {
                    header('location:/install/step/check');
                }
                break;
            case 'check':
                if ($current_step !== 'db') {
                    header('location:/install/step/db');
                }
                break;
            default:
                //die("ok");
                session(null);
                header('location:/install');
        }

    }

    //安装步骤
    public function step(Request $r, $step = 'check')
    {
        //当前完成步骤
        $complete_step = session('step');
        //当前步骤
        $current_step = $step;

        //检测基本环境
        if ($this->check($r) !== true) {
            return $this->check($r);
        }
        // dump($step);
        //dump($r);


        switch ($step) {

            case 'check':
                //环境变量返回数据
                $re_env_data = self::env_check();
                $re_function_data = self::function_check();

                //检测数据合并
                //$data=array_merge($data,$re_env_data);

                //合并初始数据
                View::assign($this->_outData);
                $reData = [
                    'list' => $re_env_data,
                    'folder' => $this->config['dir_items'],
                    'func' => $re_function_data,
                ];
                //渲染输出
                session('step', 'check');
                return view('check', $reData);
                break;
            case 'db':
                View::assign($this->_outData);
                $data_items = $this->config['data_items'];
                $data_items['mysql'] = array_merge($data_items['mysql'],[
                    'host'=>config('database.connections.mysql.hostname'),
                    'port'=>config('database.connections.mysql.hostport'),
                    'user'=>config('database.connections.mysql.username'),
                    'password'=>config('database.connections.mysql.password'),
                    'id'=>config('database.connections.mysql.type'),
                    'dbname'=>config('database.connections.mysql.database'),
                    'prefix'=>config('database.connections.mysql.prefix'),
                ]);
                
                $reData = [
                    'formlist' => $data_items,
                    'dbKeys' => implode(',', array_keys($this->config['data_items']))
                ];
                session('step', 'db');
                return view('db', $reData);
                break;
            case 'create':

                if ($this->checkdbok($r) !== true) {
                    return $this->checkdbok($r);
                }

                View::assign('data', json($r->post()));
                return view('create', $this->_outData);
                break;
            case 'finish':
                if ($this->checkFinishOk($r) !== true) {
                    return $this->checkFinishOk($r);
                }
                $flag = @touch(root_path('data') . './install.lock');
                return view('finish', $this->_outData);
                break;
            default:
                return redirect('/install');
        }
    }

    //测试数据库连接
    public function test(Request $r, $test = 'test')
    {
        $getRData = $r->param();
        $test = $getRData['test'];
        $dbFlag = [];

        switch ($test) {
            case 'pdoLink':

                // 数据库设定
                $config = array(
                    'host' => $getRData['host'],
                    'dbname' => '',
                    'port' => $getRData['port'],
                    'user' => $getRData['user'],
                    'password' => $getRData['password'],
                    'pconnect' => 0,
                    'charset' => ''
                );
                // 创建数据连接
                $testDbConn = \think\qhweb\Db::get_mysqlconn($config);
                // 判断连接是否有效
                $status = false;
                if ($testDbConn) {
                    $status = pdo_ping($testDbConn);

                }
                $testDbConn = null;
                return json($status);
                break;
            case 'testDb':
                $testDbs = explode(',', $getRData['dbs']);

                foreach ($testDbs as $testDb) {
                    $dbFlag[$testDb] = self::check_db($testDb, $getRData);
                }
                return json($dbFlag);

                //return json($getRData);
                break;
            default:
                return json(false);
        }
        //return 'test';
    }

    private function check_db($db, $r)
    {
        // 数据库设定
        $config = array(
            'host' => $r[$db . '_db_host'],
            'dbname' => $r[$db . '_db_name'],
            'port' => $r[$db . '_db_port'],
            'user' => $r[$db . '_db_user'],
            'password' => $r[$db . '_db_password'],
            'pconnect' => 0,
            'charset' => ''
        );
        // 创建数据连接
        $host = $config['host'];
        $dbname = $config['dbname'];
        $port = $config['port'];
        $user = $config['user'];
        $password = $config['password'];
        //$testDbConn = TestDb::get_mysqlconn($config);
        $pdoconn = new \PDO("mysql:host=$host;port=$port", $user, $password);
        // 判断连接是否有效
        $status = pdo_ping($pdoconn);
        if ($status) {
            $query = $pdoconn->prepare("SHOW TABLES FROM $dbname")->execute();
            $pdoconn = null;
            if (!$query) {
                return true;
            }
            return false;
        } else {
            $pdoconn = null;
            return false;
        }
    }

    private function resMsg($s, $title, $content, $flag = true)
    {
        if ($flag) {
            $cs = 'i-success';
            $csfa = 'fa fa-check';
        } else {
            $cs = 'i-error';
            $csfa = 'fa fa-times';
        }
        $msg = '<li class="' . $cs . '"><span class="i-step">【第' . $s[0] . '步】</span>  <i><span class="i-title">##' . $title . '##</span> </i>  <span class="i-content">' . $s[0] . '-' . $s[1] . '------' . $content . '------   [<i class="' . $csfa . '" aria-hidden="true"></i>] </span> <i class="i-time">' . date('Y-m-d H:i:s') . '</i></li>';
        return $msg;

    }

    public function build(Request $r)
    {
        
        //dump($r);

        $res = [];
        $i = 0;
        $m = 0;
        $step = 0;
        $installData = [];
        //session('collection', null);
        if (!session('?collection')) {
            session('collection', []);
        }
        if (!session('?managerConfig')) {
            session('managerConfig', []);
        }

        $s = $r->param('s');//过程步骤
        $ss = intval($r->param('ss'));//过程步骤
        $status = $r->param('status');//过程状态记录器
        //return $ss;


        $res['status'] = $status;
        //$res['m']=$m;
        //return (intval($s[1]));
        //第一步数据初始化处理
        if ($s[0] == ++$step && !intval($s[3])) {
            //1-1
            if ($s[1] == $m++) {
                $s = [$step, $m, 'ready_to_load_predefined_data', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '数据初始化处理', '准备载入预定义数据！');
                return json($res);
            }

            $installArr = $this->config['data_items'];
            $installArrNum = count($installArr);
            if ($s[1] == $m++) {
                $s = [$step, $m, 'complete_pre-processing_of_loading_predefined_data', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $res['msg'] = $this->resMsg($s, '数据初始化处理', '完成载入预定义数据预处理！准备安装数据采集处理！');
                return json($res);
            }

            //1-2
            foreach ($installArr as $key => $value) {
                $i++;

                if ($s[1] == $m++) {
                    $s = [$step, $m, 'collection_' . $key . '_start', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $res['msg'] = $res['msg'] = $this->resMsg($s, '数据初始化处理', '采集提交给{' . $key . '}层的数据；！');
                    return json($res);
                }

                if ($s[1] == $m++) {
                    $installData[$value['id']]['dbId'] = $r->param($value['id'] . '_db_id');
                    $installData[$value['id']]['dbHost'] = $r->param($value['id'] . '_db_host');
                    $installData[$value['id']]['dbPort'] = $r->param($value['id'] . '_db_port');
                    $installData[$value['id']]['dbName'] = $r->param($value['id'] . '_db_name');
                    $installData[$value['id']]['dbUser'] = $r->param($value['id'] . '_db_user');
                    $installData[$value['id']]['dbPassword'] = $r->param($value['id'] . '_db_password');
                    $installData[$value['id']]['dbPrefix'] = $r->param($value['id'] . '_db_prefix');
                    $installData[$value['id']]['sqlFile'] = $value['dbfile'];
                    session('collection', array_merge(session('collection'), $installData));

                    $s = [$step, $m, 'collection_' . $key . '_end', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $res['msg'] = $res['msg'] = $this->resMsg($s, '数据初始化处理', '完成采集提交给{' . $key . '}的数据!');
                    return json($res);
                }
            }
            if ($i == $installArrNum) {
                $i = 0;
            }
            if ($s[1] == $m++) {

                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '数据初始化处理', '准备处理采集提交给{管理员设置}的数据；');
                return json($res);
            }


            if ($s[1] == $m++) {
                $manager = [
                    'name' => $r->param('manager'),
                    'email' => $r->param('email'),
                    'password' => $r->param('manager_password'),
                    'manager_check_password' => $r->param('manager_check_password'),
                    'managerDbSource' => $r->param('manager_db_select'),
                    'managerTable' => $r->param('manager_insertTable'),
                ];
                session('managerConfig', $manager);
                //dump(session('collection'));
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '数据初始化处理', '完成采集提交给{管理员设置}的数据处理；');
                return json($res);
            }
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['msg'] = $this->resMsg($s, '数据初始化处理', '完成第' . $step . '步所有数据的采集！准备第' . ($step + 1) . '步的数据库创建');
                $s = [++$step, 0, '', '0'];
                $res['s'] = $s;
                return json($res);
            }

        }

        //第二步创建数据库
        //$step++;
        if ($s[0] == ++$step && !intval($s[3])) {
            //return json($m);
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建数据库', '判断是否安装PDO_MYSQL扩展');
                return json($res);
            }
            //return "ok";
            //
            if ($s[1] == $m++) {
                if (!extension_loaded('pdo_mysql')) {
                    $s = [$step, $m, '', '0'];
                    $res['status'] = -2;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建数据库', '请安装 pdo_mysql 扩展!', 0);
                    return json($res);
                }
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建数据库', 'PDO_MYSQL扩展存在，准备采集处理数据库预定义参数!');
                return json($res);
            }
            //
            if ($s[1] == $m++) {
                if (!session('?collection')) {
                    $s = [$step, $m, '', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建数据库', '采集的数据库预定义参数不存在, 返回第一步重新采集!', 0);
                    $s = ['1', '0', '', '0'];
                    $res['s'] = $s;
                    return json($res);
                }

                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建数据库', '采集的数据库预定义参数已载入，准备安装数据库!');
                return json($res);
            }
            //
            $installDb = session('collection');
            $installDbNum = count($installDb);
            foreach ($installDb as $key => $db) {
                $i++;
                if ($s[1] == $m++) {
                    $s = [$step, $m, 'createDB_' . $key . '_start', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建数据库', '采集数据库配置信息给{' . $key . '}处理!');
                    return json($res);
                }
                if ($s[1] == $m++) {
                    $conn = new \PDO("mysql:host=" . $db['dbHost'] . ";port=" . $db['dbPort'] . "", $db['dbUser'], $db['dbPassword']);
                    $connStatus = pdo_ping($conn);
                    if ($connStatus) {
                        if ($s[2] == 'createDB_' . $key . '_start') {
                            $s = [$step, $m - 1, 'createDB_' . $key . '_conn', '0'];
                            $res['status'] = 0;
                            $res['s'] = $s;
                            $res['msg'] = $this->resMsg($s, '创建数据库', '连接数据库成功，开始创建{' . $db['dbName'] . '}数据库!');
                            return json($res);
                        }
                        $db_isCreated = $conn->prepare("SHOW TABLES FROM `" . $db['dbName'] . "`")->execute();
                        if ($db_isCreated) {//存在
                            $s = [$step, $m, 'createDB_' . $key . '_end', '0'];//createDB_' . $key . '_del
                            $res['status'] = 0;
                            $res['s'] = $s;
                            $res['msg'] = $this->resMsg($s, '创建数据库', '{' . $db['dbName'] . '}数据库存在，准备第' . ($step + 1) . '步数据表创建!');
                            return json($res);
                        } else {//不存在
                            //return "不存在";
                            $createDB = $conn->prepare("CREATE DATABASE IF NOT EXISTS  `" . $db['dbName'] . "`")->execute();
                            if ($createDB) {
                                $s = [$step, $m, 'createDB_' . $key . '_end', '0'];
                                $res['status'] = 0;
                                $res['s'] = $s;
                                $res['msg'] = $this->resMsg($s, '创建数据库', '创建数据库{' . $db['dbName'] . '}成功!');
                                return json($res);
                            } else {
                                $s = [$step, $m, 'createDB__' . $key . '_fail', '0'];
                                $res['status'] = -2;
                                $res['s'] = $s;
                                $res['msg'] = $this->resMsg($s, '创建数据库', '创建数据库{' . $db['dbName'] . '}失败，不能连接数据库!', 0);
                                return json($res);

                            }

                        }
                    } else {
                        $s = [$step, $m, 'createDB__' . $key . '_fail', '0'];
                        $res['status'] = -2;
                        $res['s'] = $s;
                        $res['msg'] = $this->resMsg($s, '创建数据库', '创建数据库{' . $db['dbName'] . '}失败，不能连接数据库!', 0);
                        return json($res);

                    }

                }
            }
            if ($i == $installDbNum) {
                $i = 0;
            }
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['msg'] = $this->resMsg($s, '创建数据库', '完成第' . $step . '步所有数据库创建！准备第' . ($step + 1) . '步的数据表创建!');
                $s = [++$step, 0, '', '0'];
                $res['s'] = $s;
                return json($res);
            }

        }
        //第三步创建数据库
        if ($s[0] == ++$step && !intval($s[3])) {
            if ($s[1] == $m++) {
                if (!session('?collection')) {
                    $s = [$step, $m, '', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建数据表', '采集的数据库预定义参数不存在, 返回第一步重新采集！!', 0);
                    $s = ['1', '0', '', '0'];
                    $res['s'] = $s;
                    return json($res);
                }

                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建数据表', '采集的数据库预定义参数已载入，准备获取数据表安装目录!');
                return json($res);
            }
            $dbDir = __DIR__;
            $installTableDb = session('collection');
            $installTableDbNum = count($installTableDb);
            foreach ($installTableDb as $id => $item) {
                $i++;
                if ($s[1] == $m++) {
                    $s = [$step, $m, 'createTable_' . $id . '_start', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建数据表', '准备采集数据库配置信息给{' . $id . '}处理!');
                    return json($res);
                }

                // 数据库设定
                if ($s[1] == $m++) {
                    $config = array(
                        'host' => $item['dbHost'],
                        'port' => $item['dbPort'],
                        'dbname' => $item['dbName'],
                        'user' => $item['dbUser'],
                        'password' => $item['dbPassword'],
                        'pconnect' => 0,
                        'charset' => 'utf8'

                    );
                    $conn = \think\qhweb\Db::get_mysqlconn($config);
                    $connStatus = pdo_ping($conn);
                    if ($connStatus) {
                        //读取数据文件
                        if ($s[2] == 'createTable_' . $id . '_start') {
                            $s = [$step, $m - 1, 'createTable_' . $id . '_connOK', '0'];
                            $res['status'] = 0;
                            $res['s'] = $s;
                            $res['msg'] = $this->resMsg($s, '创建数据表', '连接{' . $item['dbName'] . '}数据库成功，准备创建{' . $item['dbName'] . '}数据库的数据表!');
                            return json($res);
                        }
                        
                        $installTableSqlFile = realpath($dbDir . $item['sqlFile']) ? file_get_contents(realpath($dbDir . $item['sqlFile'])) : false;
                        $sqlFormat = self::sql_split($installTableSqlFile, $item['dbPrefix']);
                        
                        //创建写入sql数据库文件到库中 结束
                        /*** 执行SQL语句***/
                        $counts = count($sqlFormat);
                        for ($n = $ss; $n < $counts; $n++) {
                            $sql = trim($sqlFormat[$n]);
                            if (strstr($sql, 'CREATE TABLE')) {
                                if (strstr($sql, 'CREATE TABLE IF NOT EXISTS')) {
                                    preg_match('/CREATE TABLE IF NOT EXISTS `([A-Za-z0-9]+)_([^ ]*)`/is', $sql, $matches);
                                } else {
                                    preg_match('/CREATE TABLE `([A-Za-z0-9]+)_([^ ]*)`/is', $sql, $matches);
                                }
                                
                                $sql = str_replace('`' . $matches[1] . '_', '`' . $item['dbPrefix'], $sql);//替换表前缀
                                
                                $ret = $conn->prepare($sql)->execute();
                                if ($ret) {
                                    $s = [$step, $m - 1, 'CREATE_TABLE_' . $matches[2] . '_OK', '0'];
                                    $res['status'] = 0;
                                    $res['s'] = $s;
                                    $res['ss'] = ++$n;
                                    $res['msg'] = $this->resMsg($s, '创建数据表' . $matches[1] . '_' . $matches[2] . ':', '-第' . $n . '条SQL语句:创建数据表' . $matches[1] . '_' . $matches[2] . '------执行SQL创建成功！-');
                                    return json($res);
                                } else {
                                    $s = [$step, $m - 1, 'CREATE_TABLE_' . $matches[1] . '_fail', '0'];
                                    $res['status'] = -2;
                                    $res['s'] = $s;
                                    $res['ss'] = ++$n;
                                    $res['msg'] = $this->resMsg($s, '创建数据表' . $matches[1] . '_' . $matches[2] . ':', '-第' . $n . '条SQL语句:创建数据表' . $matches[1] . '_' . $matches[2] . '------执行SQL创建失败！-', 0);
                                    return json($res);
                                }
                            } else {
                                if (trim($sql) == '') continue;
                                if (strstr($sql, 'DROP TABLE')) {
                                    preg_match('/DROP TABLE IF EXISTS `([A-Za-z0-9]+)_([^ ]*)`/is', $sql, $matches);
                                } elseif (strstr($sql, 'INSERT INTO')) {
                                    preg_match('/INSERT INTO `([A-Za-z0-9]+)_([^ ]*)`/is', $sql, $matches);
                                } else {
                                    continue;
                                }
                                $sql = str_replace('`' . trim($matches[1]) . '_', '`' . $item['dbPrefix'], $sql);//替换表前缀
                                $ret = $conn->prepare($sql)->execute();
                                if ($ret) {
                                    $s = [$step, $m - 1, 'ExecuteSQL_' . $id . '_OK', '0'];
                                    $res['sql'] = $sql;
                                    $res['status'] = 0;
                                    $res['s'] = $s;
                                    $res['ss'] = ++$n;
                                    $res['msg'] = $this->resMsg($s, '创建数据表' . $matches[1] . '_' . $matches[2] . ':', '-第' . $n . '条SQL语句:`' . mb_substr($sql, 0, 12) . '...`------执行SQL成功！-');
                                    return json($res);
                                } else {
                                    $s = [$step, $m - 1, 'ExecuteSQL_' . $id . '_fail', '0'];
                                    $res['status'] = 0;
                                    $res['s'] = $s;
                                    $res['ss'] = ++$n;
                                    $res['msg'] = $this->resMsg($s, '创建数据表' . $matches[1] . '_' . $matches[2] . ':', '-第' . $n . '条SQL语句:`' . mb_substr($sql, 0, 12) . '...`------执行SQL失败！-', 0);
                                    return json($res);
                                }

                            }
                        }
                        //创建成功

                        $s = [$step, $m, 'createTable_' . $id . '_end', '0'];
                        $res['status'] = 0;
                        $res['s'] = $s;
                        $res['ss'] = 0;
                        $res['msg'] = $this->resMsg($s, '创建数据表:', '创建{' . $item['dbName'] . '}数据库的数据表全部创建成功!');
                        return json($res);


                    } else {
                        $s = [$step, $m, 'createTable__' . $id . '_fail', '0'];
                        $res['status'] = -2;
                        $res['s'] = $s;
                        $res['ss'] = 0;
                        $res['msg'] = $this->resMsg($s, '创建数据表:', '创建数据库{' . $item['dbName'] . '}的数据表失败，不能连接数据库!', 0);
                        return json($res);
                    }

                }

            }
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['msg'] = $this->resMsg($s, '创建数据表:', '完成第' . $step . '步所有数据库的数据表创建！准备第' . ($step + 1) . '步的配置文件的安装!');
                $s = [++$step, 0, '', '0'];
                $res['ss'] = 0;
                $res['s'] = $s;
                return json($res);
            }
        }
        //第四步安装配置文件
        if ($s[0] == ++$step && !intval($s[3])) {
            // return "第四步";
            if ($s[1] == $m++) {
                if (!session('?collection')) {
                    $s = [$step, $m, '', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建配置文件', '采集的数据库配置文件预定义参数不存在, 返回第一步重新采集！!', 0);
                    $s = ['1', '0', '', '0'];
                    $res['s'] = $s;
                    return json($res);
                }

                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建配置文件', '采集的数据库配置文件预定义参数已载入，准备获取配置文件安装目录!');
                return json($res);
            }
            $config_path = config_path();
            if ($s[1] == $m++) {
                $s = [$step, $m, 'get_config_path_ok', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建配置文件', '获取配置文件安装目录并初始配置成功!' . $config_path);
                return json($res);
            }
            $copyDataBaseFlag = copy($config_path . 'database.php', $config_path . 'database.default.php');
            if ($copyDataBaseFlag) {
                if ($s[1] == $m++) {
                    $s = [$step, $m, 'get_config_database_copy_ok', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建配置文件', '备份原始数据库配置文件成功!');
                    return json($res);
                }
            } else {
                if ($s[1] == $m++) {
                    $s = [$step, $m, 'get_config_database_copy_fail', '0'];
                    $res['status'] = -2;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建配置文件', '备份原始数据库配置文件失败!', 0);
                    return json($res);
                }
            }
            if ($s[1] == $m++) {
                $s = [$step, $m, 'get_config_database_write_ready', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建配置文件', '准备读取用户数据库配置参数!');
                return json($res);
            }


            if ($s[1] == $m++) {
                $test = $this->make_config_database('database.php', $r->param(), $r->param('DbDefaultLink'));
                $s = [$step, $m, 'write_config_database_ok', '0'];
                $res['status'] = 0;
                $res['test'] = $test;
                $res['r'] = $_POST;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '创建配置文件', '写入数据库配置文件成功!');
                return json($res);
            }
            if ($s[1] == $m++) {
                if (!empty($r->param('dbConfigSign'))) {
                    foreach ($r->param('dbConfigSign') as $k => $v) {
                        $makeFlag = $this->make_config_database('database.php', $r->param(), $v, $v);
                    }
                }else{
                    $makeFlag = 'mysql';
                }
                if ($makeFlag) {
                    $s = [$step, $m, 'write_app_config_database_ok', '0'];
                    $res['status'] = 0;
                    $res['type'] = $makeFlag;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建配置文件', '写入单独数据库配置文件成功!');
                    return json($res);
                } else {
                    $s = [$step, $m, 'write_app_config_database_fail', '0'];
                    $res['status'] = -2;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '创建配置文件', '写入单独数据库配置文件成功!', 0);
                    return json($res);
                }
            }

            //last
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['msg'] = $this->resMsg($s, '创建配置文件:', '完成第' . $step . '步所有配置文件创建！准备第' . ($step + 1) . '步的管理员创始人等初始数据的安装!');
                $s = [++$step, 0, '', '0'];
                $res['ss'] = 0;
                $res['s'] = $s;
                return json($res);
            }
        }
        if ($s[0] == ++$step && !intval($s[3])) {
            //return "第五步";完成结束
            if ($s[1] == $m++) {
                $s = [$step, $m, 'get_manage_setting_ready', '0'];
                $res['status'] = 0;
                $res['s'] = $s;
                $res['msg'] = $this->resMsg($s, '添加管理员', '准备读取提交的管理员配置参数!');
                return json($res);
            }
            if ($s[1] == $m++) {
                if (!empty(session('managerConfig'))) {
                    //dump(session('managerConfig'));
                    $s = [$step, $m, 'check__manage_setting_ready', '0'];
                    $res['status'] = 0;
                    $res['manage'] = session('managerConfig');
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '添加管理员', '获取管理员配置参数成功,准备检测数据可用性!');
                    return json($res);
                } else {
                    $s = [$step, $m, 'check__manage_setting_ready_fail', '0'];
                    $res['status'] = -2;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '添加管理员', '获取管理员配置参数失败,请检查数据库配置文件!');
                }
            }
            $check_manage_param_flag = $this->check_manage_param(session('managerConfig'), session('collection'));
            if ($s[1] == $m++) {
                if ($check_manage_param_flag === true) {
                    $s = [$step, $m, 'check__manage_setting_ok', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '添加管理员', '检测管理员数据兼可用,准备向指定数据库写入管理员信息!');
                    return json($res);
                } else {
                    $s = [$step, $m, 'check__manage_setting_fail', '0'];
                    $res['status'] = -2;
                    $res['s'] = $s;
                    $res['conn'] = $check_manage_param_flag;
                    $res['returnPage'] = $check_manage_param_flag['returnPage'];
                    $res['msg'] = $this->resMsg($s, '添加管理员', '检测管理员数据不可用,检测到' . $check_manage_param_flag['msg'] . '!', 0);
                    return json($res);
                }
            }
            if ($s[1] == $m++) {
                $add_managerFlag=self::add_manager(session('managerConfig'));
                if($add_managerFlag){
                    $s = [$step, $m, 'add__manage_ok', '0'];
                    $res['status'] = 0;
                    $res['s'] = $s;
                    $res['flag'] = $add_managerFlag;
                    $res['msg'] = $this->resMsg($s, '添加管理员', '写入管理员信息成功!');
                    return json($res);
                }else{
                    $s = [$step, $m, 'add__manage_fail', '0'];
                    $res['status'] =-2;
                    $res['s'] = $s;
                    $res['msg'] = $this->resMsg($s, '添加管理员', '写入管理员信息失败!',0);
                    return json($res);
                }
            }
            if ($s[1] == $m++) {
                $s = [$step, $m, '', '0'];
                $res['status'] = 0;
                $res['msg'] = $this->resMsg($s, '成功添加管理员', '完成第' . $step . '步所有信息创建！!');
                $s = [++$step, 0, '', '0'];
                $res['s'] = $s;
                return json($res);
            }
        }
        $arr['step'] = $step;
        //$arr['data'] = session('collection');
        $arr['status'] = 'finish';
        $arr['msg'] = '完成';
        return json($arr);


    }

    private function check_manage_param($r, $c)
    {
        //return $r['password'];
        $res = [];
        //检测密码是否相同
        if ($r['password'] != $r['manager_check_password']) {
            $res['msg'] = "传入的管理员两个密码不同,请返回数据库配置页重新设置";
            $res['returnPage'] = '/install/step/check';
            return $res;
        }
        //检测连接
        $config = array(
            'host' => $c['mysql']['dbHost'],
            'port' => $c['mysql']['dbPort'],
            'dbname' => $c['mysql']['dbName'],
            'user' => $c['mysql']['dbUser'],
            'password' => $c['mysql']['dbPassword'],
            'pconnect' => 0,
            'charset' => 'utf8'

        );
        $conn = \think\qhweb\Db::get_mysqlconn($config);
        $connStatus = pdo_ping($conn);
        if (!$connStatus) {
            $res['msg'] = "传入的管理员参数数据库" . $c['mysql']['dbName'] . "连接失败，请返回数据库配置页重新设置";
            $res['returnPage'] = '/install/step/check';
            return $res;
        }

        //检测数据表
        $query = $conn->query("SHOW TABLES LIKE '" . $c['mysql']['dbPrefix'] . 'system_user' . "'")->fetchAll();
        if (!$query) {
            $res['msg'] = "传入的管理员参数" . $c['mysql']['dbPrefix'] . 'system_user' . "数据表不存在，请返回数据库配置页重新设置";
            $res['returnPage'] = '/install/step/check';
            return $res;
        }
        return true;

    }

    public function add_manager($r)
    {
        //添加管理员
        $data=[
            'username'=>$r['name'],
            'password'=>md5($r['password']),
            'nickname'=>'系统管理员',
            'id'=>'10000',
            'contact_mail'=>$r['email'],
        ];
        
        $res=Db::name('system_user')->insert($data);
        return $res;

    }

    private function make_config_database($filename, $r, $defaultDbLink, $type = "")
    {
        //return $type;
        //return base_path($type);
        if (!is_dir(base_path($type) . 'config')) {
            //return base_path($type).'config';
            mkdir(iconv("UTF-8", "GBK", base_path($type) . 'config'), 0777, true);
        }
        //return "ok";
        if (!$type) {
            $filePath = root_path('config') . $filename;
        } else {
            $filePath = base_path($type) . 'config/' . $filename;
        }
        if (!is_file($filePath)) {
            @chmod($filePath, 0777);
        }
        $return_array = "<?php\n";
        $return_array .= "// +----------------------------------------------------------------------
// | Library for YYAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海云音信息技术有限公司 [ http://www.yyinfos.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/qhweb/ThinkLib
// +----------------------------------------------------------------------\n
use think\\facade\Env;\n";
        $return_array .= "return[\n";
        $return_array .= "// 默认使用的数据库连接配置\n'default'         => '" . $defaultDbLink . "',\n";
        //$return_array.="return[".var_export($data, true)."];";
        foreach (session('collection') as $key => $value) {
            $connections[$key] = [
                'type' => "mysql",
                'hostname' => $r[$key . '_db_host'],
                'database' => $r[$key . '_db_name'],
                'username' => $r[$key . '_db_user'],
                'password' => $r[$key . '_db_password'],
                'hostport' => $r[$key . '_db_port'],
                'dsn' => '',
                'params' => [],
                'charset' => 'utf8',
                'prefix' => $r[$key . '_db_prefix'],
                'debug' => true,
                'deploy' => 0,
                'rw_separate' => false,
                'master_num' => 1,
                'slave_no' => '',
                'fields_strict' => true,
                'sql_explain' => false,
                'builder' => '',
                'query' => '',
                'break_reconnect' => true,
            ];
        }
        $return_array .= "'connections'     =>" . var_export($connections, true) . ",\n";
        $return_array .= "'time_query_rule' => [],\n";
        $return_array .= "'auto_timestamp'  => 'timestamp',\n";
        $return_array .= "'datetime_format' => 'Y-m-d H:i:s',\n";
        $return_array .= "];";
        $flag = file_put_contents($filePath, $return_array); //数据库配置文件的地址
        return $r;

    }

    private function sql_split($sql, $prefix)
    {
        //去掉注释 /* */：/\*{1,2}[\s\S]*?\*/    /\*[\s\S]+?\*/   /(\/\*.*\*\/)|(#.*?\n)/s
        $sql = preg_replace('/(\/\*.*\*\/)/s', '', $sql);
        //删除空白行 ^\s*\n
        //$sql=preg_replace('/($\s*$)|(^\s*^)/m','\n',$sql);
        //删除 注释 //：//[\s\S]*?\n
        //$sql=preg_replace('//[\s\S]*?\n','',$sql);
        //替换前缀
        //$sql = str_replace("\r", "\n", str_replace(' `'.$this->_table_pre, ' `'.$prefix, $sql));
        //$sql=preg_replace('/ `[\S](.*)_/isU',' `'.$prefix,$sql);
        //替换 DROP TABLE IF EXISTS
        $sql = preg_replace('/DROP TABLE IF EXISTS `([\S]+_)/isU', 'DROP TABLE IF EXISTS `' . $prefix, $sql);
        //替换 CREATE TABLE
        $sql = preg_replace('/CREATE TABLE `([\S]+_)/isU', 'CREATE TABLE `' . $prefix, $sql);
        //替换 INSERT INTO `
        $sql = preg_replace('/INSERT INTO `([\S]+_)/isU', 'INSERT INTO `' . $prefix, $sql);

        //替换
        $sql = preg_replace("/TYPE=(InnoDB|MyISAM|MEMORY)( DEFAULT CHARSET=[^; ]+)?/", "ENGINE=\\1 DEFAULT CHARSET=utf8", $sql);
        //替换换行符
        $sql = str_replace("\r", "\n", $sql);
        //return $sql;
        $ret = array();
        $num = 0;
        $queriesArr = explode(";\n", trim($sql));
        // unset($queriesArr['BEGIN']);
        //unset($queriesArr['COMMIT']);
        unset($sql);
        //return $queriesArr;
        foreach ($queriesArr as $query) {
            $ret[$num] = '';
            $queries = explode("\n", trim($query));
            $queries = array_filter($queries);

            foreach ($queries as $query) {
                $str1 = substr($query, 0, 1);
                if ($str1 != '#' && $str1 != '-' && $query != 'BEGIN' && $query != 'COMMIT')
                    $ret[$num] .= $query;
            }
            $num++;
        }
        return $ret;

    }
    //删除安装文件
    public function delInstall(){
        if(is_file(root_path('data').'copyInstall.lock')){
            $flag=@unlink(root_path('data').'copyInstall.lock');
        }
        if($flag){
            $res['msg']="安装文件删除成功！";
            $res['status']=1;
        }else{
            $res['msg']="安装文件删除失败！";
            $res['status']=-1;
        }

        return json($res);
    }
}
