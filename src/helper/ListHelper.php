<?php

// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/zoujingli/ThinkLibrary
// | github 仓库地址 ：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\helper;

use think\helper\Str;
use think\exception\HttpException;
use think\admin\Helper;
use think\admin\service\FieldService;

/**
 * 数据列表管理器
 * Class ListHelper
 * @package think\admin\helper
 */
class ListHelper extends Helper
{

    // 数据标识
    protected $name;
    // 字段
    protected $fields;
    // 模板变量
    protected $data;

    /**
     * 逻辑器初始化
     * @param string $name 数据标识
     * @param array $fields 字段信息
     * @param callable|null $callable 初始回调
     * @return $this
     * @throws \think\db\exception\DbException
     */
    public function init($name, $fields = [], ?callable $callable = null): ListHelper
    {
        $this->name = $name;
        $this->fields = $fields;
        $this->data = input('get.*')??[];
        if (is_callable($callable)) {
            call_user_func($callable, $this, $fields);
        }
        return $this;
    }

    /**
     * 设置搜索字段
     * @param string $name 插件名
     * @return mixed|null
     */
    public function setSearch()
    {
        $this->data['searchFields'] = is_array($this->fields) ? FieldService::instance()->formatFields(searchArray($this->fields,'search',1),[]) : [];
        return $this;
    }
    /**
     * 设置按钮
     * @param array $data
     * @return mixed|null
     */
    public function topButton($data=[])
    {
        $this->data['topButton']= FieldService::instance()->setButton($data,'sm');
        return $this;
    }
    /**
     * 设置操作按钮
     * @param array $data 
     * @return mixed|null
     */
    public function toolButton($data=[])
    {
        $this->data['toolButton'] = FieldService::instance()->setButton($data,'xs');
        return $this;
    }

    /**
     * 设置模板信息
     * @return string
     */
    public function assign($data=[])
    {
        
        foreach ($data as $name => $value) $this->data[$name] = $value;
        $arr = [];
        $str = "{field: '%s', title: '%s',width: '%s', align: '%s',sort:%0d,hide:%0d,edit:%0d}";
        if(is_array($this->fields)) foreach($this->fields as $v){
            $width = $v['width']>0 ||empty($v['width']) ? '' : $v['width'];
            $arr[] = sprintf($str, $v['name'],$v['title'],$width,$v['align'],$v['sort'],$v['list']?0:1,$v['edit']??0);
        }
        $this->data['listCol'] = count($arr) >0 ? implode(',',$arr).',' : '';
        $this->class->assign($this->data);
    }
}