<?php
namespace think\admin\taglib;
use think\template\TagLib;
class Tpl extends TagLib{
    /**
     * 定义标签列表
     */
    protected $tags   =  [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'close'     => ['attr' => 'time,format', 'close' => 0], //闭合标签，默认为不闭合
        'page'      => ['attr' => 'value,type,id', 'close' => 1,'expression' => true],
        'content'      => ['attr' => 'cid,where,row,order,id,empty', 'close' => 1],//栏目ID，where条件，row行数，order排序，id记录集名
        'list'      => ['attr' => 'cid,where,row,order,id,empty', 'close' => 1],//栏目ID，where条件，row行数，order排序，id记录集名
        'category'      => ['attr' => 'cid,where,row,type,id,empty', 'close' => 1],//栏目ID，where条件，row行数，order排序，id记录集名
        'cateurl'      => ['attr' => 'cid,type', 'close' => 0],//栏目ID，where条件，row行数，order排序，id记录集名
        'pagelist'      => ['attr' => 'cid,where,row,order,id,empty', 'close' => 1],//
        'pages'     => ['attr' => '', 'close' => 0], //闭合标签，默认为不闭合
    ];

    /**
     * 这是一个闭合标签的简单演示
     */
    public function tagClose($tag)
    {
        $format = empty($tag['format']) ? 'Y-m-d H:i:s' : $tag['format'];
        $time = empty($tag['time']) ? time() : $tag['time'];
        $parse = '<?php ';
        $parse .= 'echo date("' . $format . '",' . $time . ');';
        $parse .= ' ?>';
        return $parse;
    }

    /**
     * 这是一个非闭合标签的简单演示
     */
    public function tagPage($tag, $content)
    {
        $value  = $tag['value'];
        $id     = isset( $tag['id'] ) ? $tag['id'] : 'page';
        $type   = isset( $tag['type'] ) ? $tag['type'] : 'cate';
        $empty = isset( $tag['empty'] ) ? $tag['empty'] : '暂无信息';
        if (':' == substr($value, 0, 1)) {
            $value = $this->autoBuildVar($value);
            $parseStr .= '$_result=' . $value . ';';
            $value = '$_result';
        }else{
            $value = $this->autoBuildVar($value);
        }
        $parseStr = '<?php $'.$id.' = \think\admin\service\DocumentService::instance()->getViewOne(' . $value . ',"' . $type . '");'."\n";
        $parseStr .= 'if(!empty($'.$id.')):'."\n";
        $parseStr .= '?>'."\n";
        $parseStr .= $content."\n";
        $parseStr .= '<?php else:echo "'.$empty.'";endif;?>'."\n";
        return $parseStr;
    }

    public function tagContent($attr,$content) {
        $order = isset( $attr['order'] ) ? $attr['order'] : 'sort ASC,create_time desc,id desc';   //排序
        $row = isset( $attr['row'] ) ?  $this->autoBuildVar($attr['row']) : 10;//显示条数
        $where = isset( $attr['where'] ) ? $attr['where'] : ' 1=1 '; //查询条件
        $cid = isset( $attr['cid'] ) ? $this->autoBuildVar($attr['cid']) : 0;
        $id = isset( $attr['id'] ) ? $attr['id'] : 'field';
        $flag = isset( $attr['flag'] ) ? $attr['flag'] : '';
        $empty = isset( $attr['empty'] ) ? $attr['empty'] : '暂无信息';
        if($flag) $where .= " and FIND_IN_SET('{$flag}',flag) ";
        $str = '<?php'."\n";
        $str .='$cid = '.(is_numeric($cid) || stripos("$",$cid) != "-1" ? $cid : "'.$cid.'").';'."\n";
        $str .='$where = "'.$where.'";$cidArr=[];'."\n";
        $str .='if($cid):'."\n";
        $str .='  $cid = explode(",",$cid);'."\n";
        $str .='  foreach ($cid as $key => $c) {
          $cidArr = array_merge($cidArr,$categorys[$c]["arrchildid"]??[]);
        };'."\n";
        $str .='endif;'."\n";
        $str .='$result=\think\admin\service\DocumentService::instance()->getViewList($cidArr,$where,'.$row.',"'.$order.'");'."\n";
        $str .= 'if($result && is_array($result)): foreach ($result as $index=>$field):'."\n";
        $str .= '$field["index"] = $index;'."\n";
        $str .= '$'.$id. ' = $field;'."\n";
        $str .= '?>'."\n";
        $str .= $content."\n";
        $str .= '<?php endforeach;else:echo "'.$empty.'";endif;?>'."\n";
        return $str;
    }

    /*分页列表标签*/
    public function tagPagelist($attr,$content) {
        $order = isset( $attr['order'] ) ? $attr['order'] : 'sort ASC,create_time desc,id desc';   //排序
        $row = isset( $attr['row'] ) ?  $this->autoBuildVar($attr['row']) : 10;//显示条数
        $where = isset( $attr['where'] ) ? $attr['where'] : ' 1=1 '; //查询条件
        $cid = isset( $attr['cid'] ) ? $this->autoBuildVar($attr['cid']) : 0;
        $id = isset( $attr['id'] ) ? $attr['id'] : 'field';
        $flag = isset( $attr['flag'] ) ? $attr['flag'] : '';
        $empty = isset( $attr['empty'] ) ? $attr['empty'] : '暂无信息';
        if($flag) $where .= " and FIND_IN_SET('{$flag}',flag) ";
        $str = '<?php'."\n";
        $str .='$cid = '.(is_numeric($cid) || stripos("$",$cid) != "-1" ? $cid : "'.$cid.'").';'."\n";
        $str .='$where = "'.$where.'";$cidArr=[];$path="";$page=request()->param("page",1);'."\n";
        $str .='$cate = \think\admin\model\SystemCategory::mk()->with(["module"])->findorEmpty($cid)->toArray();'."\n";
        $str .='if($cate):'."\n";
        $str .='  $cidArr = !is_array($cate["arrchildid"])?explode(",",$cate["arrchildid"]):$cate["arrchildid"];'."\n";
        $str .='  $path = sysuri("index/cms/list",["catdir"=>$cate["catdir"],"module"=>$cate["module"]["name"],"page"=>"[PAGE]"]);'."\n";
        $str .='endif;'."\n";
        $str .='list($result,$pages)=\think\admin\service\DocumentService::instance()->getViewPage($cidArr,$where,'.$row.',"'.$order.'",$page,$path);'."\n";
        $str .= 'if($result && is_array($result)): foreach ($result as $index=>$field):'."\n";
        $str .= '$field["index"] = $index;'."\n";
        $str .= '$'.$id. ' = $field;'."\n";
        $str .= '?>'."\n";
        $str .= $content."\n";
        $str .= '<?php endforeach;else:echo "'.$empty.'";endif;?>'."\n";
        return $str;
    }

    // 分类信息标签
    public function tagCategory($attr,$content) {
        $row = isset( $attr['row'] ) ? $this->autoBuildVar($attr['row']) : 0;//显示条数
        $type = isset( $attr['type'] ) ? $attr['type'] : 'top';//读取类型son，self，current
        $cid = isset( $attr['cid'] ) ? $this->autoBuildVar($attr['cid']) : 0;
        $id = isset( $attr['id'] ) ? $attr['id'] : 'field';
        $where = isset( $attr['where'] ) ? $this->autoBuildVar($attr['where']) : '1=1';
        $cid = is_numeric($cid) || substr($cid, 0, 1) == "$" ? $cid : '"'.$cid.'"';
        $order = isset( $attr['order'] ) ? $attr['order'] : 'sort asc';
        list($sfield,$stype) = explode(' ',$order);
        $stype = $stype == 'desc' ? 'SORT_DESC' : 'SORT_ASC';
        //判断并解析
        $str = "\n".'<?php'."\n";
        $str.='$where="'.$where.'";$row_limit="'.$row.'";'."\n";
        $str.='$type="'.$type.'";'."\n";
        $str.='$cid = '.$cid.';'."\n";
        $str.='switch ($type):'."\n";
        $str.='  case "top":$list = searchArray($categorys,"pid",0);break;'."\n";
        $str.='  case "current":$list = searchArray($categorys,"id",$cid);break;'."\n";
        $str.='  case "son":$list = searchArray($categorys,"pid",$cid);break;'."\n";
        $str.='  case "self":$pid=array_column(searchArray($categorys,"id",$cid),"pid");$list = searchArray($categorys,"pid",$pid);break;'."\n";
        $str.='endswitch;'."\n";
        $str.='$list = arraySort($list,"'.$sfield.'",'.$stype.');'."\n";
        $str .= 'foreach ($list as $index=>$field):'."\n";
        $str .= '$field["index"] = $index;'."\n";
        $str .= '$field["title"] = $field["catname"];'."\n";
        $str .= '$'.$id. ' = $field;'."\n";
        $str .= 'if ($row_limit && $row_limit == $index)break;'."\n";
        $str .= '?>'."\n";
        $str .= $content;
        $str .= '<?php endforeach;?>';
        return $str;
    }
    /**
     * 分类连接
     */
    public function tagCateurl($attr){
      $cid = isset( $attr['cid'] ) ? $this->autoBuildVar($attr['cid']) : 0;
      $type = isset( $attr['type'] ) ? $attr['type'] : 'url';
      $str = '<?php echo getCate('.$cid.',"'.$type.'");?>';
      return $str;
    }

    public function tagList($attr,$content) {
        $order = isset( $attr['order'] ) ? $attr['order'] : 'sort ASC,id desc';   //排序
        $row = isset( $attr['row'] ) ?  $this->autoBuildVar($attr['row']) : 10;//显示条数
        $where = isset( $attr['where'] ) ? $attr['where'] : ''; //查询条件
        $cid = isset( $attr['cid'] ) ? $this->autoBuildVar($attr['cid']) : 0;
        $id = isset( $attr['id'] ) ? $attr['id'] : 'field';
        $ids = isset( $attr['ids'] ) ? $this->autoBuildVar($attr['ids']) : 0;
        $empty = isset( $attr['empty'] ) ? $attr['empty'] : '暂无信息';
        $db = isset( $attr['table'] ) ? $attr['table'] : 'cms_document'; //数据表

        if($where){
            $where     = explode("=",$where);
            foreach ($where as $key => $value) {
                $where[$key] = stripos($value,"$") !== false  ?  str_replace(["$","]"],['".$',']."'],$this->autoBuildVar($value)) : $value;
            }
       
            $where = implode("=",$where);
        }
        $str = '<?php'."\n";
        $str .='$cid = '.(is_numeric($cid) || stripos($cid,"$") !== false ? $cid : "'.$cid.'").';'."\n";
        $str .='$where = "'.$where.'";'."\n";
        $str .='$ids = '.(is_numeric($ids) || stripos($ids,"$") !== false ? $ids : "'.$ids.'").';'."\n";
        $str .='$categorys = \think\admin\service\DocumentService::instance()->getCache();'."\n";
        $str .='$db = '.(stripos($db,"$") === false ? "'$db'" : $this->autoBuildVar($db)).';'."\n";
        $str .='if ($cid):'."\n";
        $str .='  $cidArr=[];$cid = explode(",",$cid);'."\n";
        $str .='  foreach ($cid as $key => $c) {
          $cidArr = $categorys[$c]["arrchildid"]??[];
        };'."\n";
        $str .='    $query = \think\facade\Db::name($db)->where($where)->whereIn("cid",$cidArr)->where(function ($query) use($cidArr) {
          foreach ($cidArr as $key => $id) {
            $query->whereFindInSet("relation_cid",$id,"or");
          }
        });'."\n";
        $str .='    $query->limit('.$row.')->order("'.$order.'");'."\n";
        $str .='elseif ($ids):'."\n";
        $str .='    $query = \think\facade\Db::name($db)->where($where)->whereIn("id",$ids)->limit('.$row.')->order("'.$order.'");'."\n";
        $str .='else:'."\n";
        $str .='    $query = \think\facade\Db::name($db)->where($where)->limit('.$row.')->order("'.$order.'");'."\n";
        $str .='endif;'."\n";
        $str .='$result=$query->where(["status"=>1])->select()->toArray();'."\n";
        $str .= 'if($result && is_array($result)): foreach ($result as $index=>$field):'."\n";
        $str .= '$field["index"] = $index;$cid=$field["cid"];'."\n";
        $str .= '$field["url"] = isset($field["url"]) && !empty($field["url"]) ? $field["url"] : url("index/cms/info",array("cid"=>$cid,"id"=>$field["id"]));'."\n";
        $str .= '$field["cate_url"] = url("index/cms/list",array("cid"=>$cid));'."\n";
        $str .= '$field["cate_name"] = @$categorys[$cid]["name"];'."\n";
        $str .= '$'.$id. ' = $field;'."\n";
        $str .= '?>'."\n";
        $str .= $content."\n";
        $str .= '<?php endforeach;else:echo "'.$empty.'";endif;?>'."\n";
        return $str;
    }
}
