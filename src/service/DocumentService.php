<?php

namespace think\admin\service;

use think\admin\Service;
use think\admin\extend\DataExtend;
use think\admin\service\AdminService;
use think\admin\service\FieldService;
use think\admin\service\ModuleService;
use think\admin\service\CategoryService;
use think\admin\model\SystemDocument;
/**
 * 文章数据服务
 * Class DocumentService
 * @package app\cms\service
 */
class DocumentService extends Service
{
    //基础数据字段
    protected $fields = [
        Array('name'=>'from','title'=>'来源','type'=>'text','tips'=>'文档来源','default'=>'本站','option'=>'extend'),
        Array('name'=>'view','title'=>'阅读量','type'=>'decimal','tips'=>'文档阅读量','default'=>'100','option'=>'extend'),
        Array('name'=>'create_time','title'=>'发布时间','type'=>'datetime','tips'=>'文档的发布时间','default'=>'','option'=>'extend'),
    ];

    /**
     * 文档管理列表表格字段规则
     * @param  array $module  模型锡尼希
     */
    public function layTabeField($module)
    {
        $searchList = [];
        $listFieldA = [
            ['checkbox' => true,'fixed' => true],
            ['field' => 'sort','sort' => true, 'title' => '排序', 'width' => 80, 'align' => 'center','templet' => '#SortInputTpl'],
        ];
        
        if($module) foreach ($module['fields'] as $k => $v) {
            $width = $v['width'] > 0 ? $v['width'] :'';
            if($v['list']){
                if($v['name'] == 'title'){
                    $listFieldA[] = ['field' => $v['name'],'title' => $v['title'], 'minWidth'=>160, 'templet' => '#TitleTpl'];
                }else{
                    $listFieldA[] = ['field' => $v['name'],'title' => $v['title'], 'width'=>$width, 'align'=>'center'];
                }
            }
            if($v['search']){
                if($v['options'] && !is_array($v['options'] )){
                    $v['options'] = explode('\n',$v['options']);
                }
                $searchList[] = $v;
            }
        }
        $listFieldB = [
            ['field' => 'status','title' => '发布状态', 'width' => 110, 'templet' => '#StatusSwitchTpl','fixed' => 'right'],
            ['toolbar' => '#toolbar','title' => '操作面板', 'width' => 140, 'align' => 'center', 'fixed' => 'right'],
        ];
        return [json_encode(array_merge($listFieldA,$listFieldB)),$searchList];
    }

    //检查文档是否需要审核
    public function setStatus($model,$id,$cid)
    {
        if(sysconf('cms.audit')){
            $nodes = FlowService::instance()->getFlow(sysconf('cms.flow'));
            $data = ['aid' => $id,'cid' => $cid,'node' => FlowService::instance()->getStartNode($nodes),'model'=>$model];
            data_save('cms_audit',$data,'aid');
        }
    }

    /**
     * 获取文档表单
     * @param string $data 默认数据
     * @param string $fields 附加的字段信息
     * @return Array
     */
    public function getFormFields($fields,$data = [])
    {
        return FieldService::instance()->formatFields($fields,$data);
    }
    
    /**
     * 文档表单提交数据获取
     * @param array $fields 字段信息
     * @param array $data 默认数据
     */
    public function getSaveData($fields,$data=[])
    {
        $siteinfo = SiteService::instance()->getSiteById();
        $cmsSetting = unserialize($siteinfo['setting']??'a:0:{}');
        list( $data ,$rules)= FieldService::instance()->getFormData($fields,$data);
        //设定发布用户和发布时间
        $data['uid'] = $data['uid'] ?? AdminService::instance()->getUserId();
        $data['create_time'] = !empty($data['create_time']) ? $data['create_time'] : date('Y-m-d H:i:s');
        //自动获取关键词
        $data['keyword'] = !empty($data['keyword']) ? $data['keyword'] : $data['title'];
        //自动获取文档描述
        if(empty($data['desc'])){
            if ($cmsSetting['autokey'] > 0) {
                $data['desc'] = mb_substr(strip_tags($data['content']), 0, 180, 'utf-8');
            }else{
                $data['desc'] = $data['title'];
            }
        }
        //文档审核状态
        $data['status'] = $cmsSetting['audit'] ? 0 : 1;
        // 远程图片本地化处理
        if ($cmsSetting['downimg'] > 0) {
            $data['content'] = $this->downContentImage($data['content']);
            $data['content'] = $this->downContentLink($data['content']);
        }
        //缩略图处理
        $data['thumb'] = !empty($data['thumb']) ? $data['thumb'] : $this->getThumbForContent($data['content']);
        // 处理自定义属性
        $data['flag'] = $data['flag']??[];
        if ($data['thumb'] && !in_array('p',$data['flag'])){
            $data['flag'][] = 'p';
        }
        return [$data,$rules];
    }
    /**
     * 获取文档列表数据
     * @param string|array $cids 文档的分类ID
     * @param string|array $where 查询条件
     * @param string $limit 读取的条数
     * @param string $order 排序方式
     */
    public function getViewList($cids='',$where='',$limit=10,$order='sort ASC,create_time desc,id desc')
    {
        $list = $this->buildData(SystemDocument::mk()->getList($cids,$where,$limit,$order));
        return $list;
    }
    /**
     * 获取文档分页数据
     * @param string|array $cids 文档的分类ID
     * @param string|array $where 查询条件
     * @param string $limit 每页读取的条数
     * @param string $order 排序方式
     * @return array
     */
    public function getViewPage($cids='',$where='',$limit=10,$order='sort ASC,create_time desc,id desc',$page=1,$path='')
    {
        list($list,$pages) = SystemDocument::mk()->getPageList($cids,$where,$limit,$order,$page,$path);
        $list = $this->buildData($list['data']);
        return [$list,$pages];
    }
    /**
     * 获取单条文档数据
     * @param string $id 文档的ID
     * @return array
     */
    public function getViewOne($id,$type='doc'): array
    {
        return SystemDocument::mk()->getOne($id,$type);
    }
    /**
     * 获取关联文档列表数据
     * @param string|array $aids 文档的ID
     */
    public function getRelationList($aids='')
    {
        $list = SystemDocument::mk()->with(['cate','site','module'])->whereIn('id',$aids)->select()->toArray();
        return $this->buildData($list);
    }
    /**
     * CMS文档内容绑定
     * @param array $data 数据列表
     * @param array $cate 分类信息
     * @return array
     * @throws \ReflectionException
     */
    protected function buildData($data=[])
    {
        //格式化图片为网络图片
        foreach ($data as $key => &$val) {
            
            if(!empty($val['jump_url'])){
                $val['url'] =  $val['jump_url'];
            }else{
                $host = 'http://'.$val['site']['domain'];
                $val['url'] = url('index/cms/info',['id'=>$val['id'],'module'=>$val['module']['name']]);
            }
            $val['catname'] = $val['cate']['name']??'';
            $val['caturl'] = sysuri('index/cms/list',['catdir'=>$val['cate']['catdir'],'module'=>$val['module']['name']]);
            
        }
        return $data;
    }
    /**
     * 提取内容中的第N张图片
     * @param string $content 内容
     * @param int $key 第几张图片的数据
     */
    public function getThumbForContent($content='',$all = false)
    {
        if(empty($content)) return null;
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png|\.jpeg]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern,$content,$match);
        
        if( isset($match[1]) && count($match[1]) > 0 ){
            return $all ? $match[1] : $match[1][0];
        }
        return '';
    }
    /**
     * 查找内容中图片并下载到本地
     */
    public function downContentImage($content='')
    {
        if(empty($content)) return null;
        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png|\.jpeg]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern,$content,$match);
        if( isset($match[1]) && count($match[1]) >= 0 ){
            foreach ($match[1] as $key => $url) {
            //判断是否远程图片
                if(preg_match("/^http(s)?:\\/\\/.+/",$url)){
                    if($this->downloadFile($url)){
                        $filename = pathinfo($url, PATHINFO_BASENAME);
                        $content = str_replace($url,'/upload/remote/'.$filename,$content);
                    }
                }
            }
        }
        return $content;
    }
    /**
     * 下载到本地
     */
    public function downloadFile($url, $path='upload/remote/')
    {
        if(!is_dir($path)) mkdir($path,0755);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        $file = curl_exec($ch);
        curl_close($ch);
        if($file){
            $this->saveAsFile($url, $file, $path);
            return true;
        }else{
            return false;
        }
    }
   /**
    * 保存图片到本地
    */
    private function saveAsFile($url, $file, $path)
    {
        $filename = pathinfo($url, PATHINFO_BASENAME);
        $resource = fopen($path . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
    }
    /**
     * 查找内容中附件并下载到本地
     */
    public function downContentLink($content='')
    {
        if(empty($content)) return null;
        $pattern="/<[a|A].*?href=[\'|\"](.*?(?:[\.doc|\.docx|\.xls|\.xlsx|\.zip|\.rar|\.pdf|\.ppt]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern,$content,$match);

        if( isset($match[1]) && count($match[1]) >= 0 ){
            foreach ($match[1] as $key => $val) {
                //判断是否远程图片
                if(preg_match("/^http(s)?:\\/\\/.+/",$val)){
                    if($this->downloadFile($val)){
                        $filename = pathinfo($val, PATHINFO_BASENAME);
                        $content = str_replace($val,'/upload/remote/'.$filename,$content);
                    }
                }
            }
        }
        return $content;
    }
}