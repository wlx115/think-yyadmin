<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace {$namespace};
use think\Addons;
/**
* {$title}插件
* @author {$author}
*/
class Plugin extends Addons
{
    //插件信息
    public $info = array(
        'name'          =>  '{$name}',
        'title'         =>  '{$title}',
        'description'   =>  '{$description}',
        'status'        =>  {$status},
        'author'        =>  '{$author}',
        'version'       =>  '{$version}',
        'is_config'     =>  {$is_config},
        'is_admin'      =>  {$is_admin},
        'is_index'      =>  {$is_index},
        'is_weixin'     =>  {$is_weixin}
    );

    //插件安装方法，这里写入插件相关表的Sql语句，$prefix为表前缀
    public function install(){
        $prefix = $this->app->config->get('database.connections.mysql.prefix');
        $this->app->db->query("DROP TABLE IF EXISTS `{$prefix}addon_{$name}`;");
        $this->app->db->query("
        CREATE TABLE `{$prefix}addon_{$name}` (
            `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `sort` bigint(20) unsigned NULL DEFAULT '0' COMMENT '排序',
            `status` tinyint(3) unsigned DEFAULT '1' COMMENT '状态',
            `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='{$title}插件';");
        return true;
    }

    //插件卸载方法，这里写入插件相关表的卸载Sql语句
    public function uninstall(){
        $prefix = $this->app->config->get('database.connections.mysql.prefix');
        $this->app->db->query("DROP TABLE IF EXISTS `{$prefix}addon_{$name}`;");
        return true;
    }

    // 实现的钩子方法
    {$hookfun}
}