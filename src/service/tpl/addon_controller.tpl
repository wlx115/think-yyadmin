<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace [namespace];

use think\admin\Controller;
use think\admin\service\WechatService;
/*
* [title]
* Class [controller]
* @package [namespace]
*/

class [controller] extends Controller
{
    /**
     * 当前应用绑定的公众号Appid
     */
    protected $appid;

    /**
     * 模型
     */
    protected $model;
    /*
    * 初始化服务
    */
    protected function initialize() 
    {
        $this->appid = WechatService::instance()->getAppid();
        $this->model = M("addon_[name]");
    }
    
    /**
     * [title]管理
     * @auth true
     * @menu true
     */
    public function index()
    {
        $this->model->mQuery()->layTable(function () {
            $this->title = '插件[title]管理';
        }, function ($query) {
            $query->equal('status')->dateBetween('create_at')->order('id desc');
        });
    }

    /**
     * 列表数据处理
     * @param array $data
     */
    protected function _index_page_filter(&$data)
    {
        foreach ($data as $key => $val) {
          // code...
        }
    }
    /**
     * 添加
     * @auth true
     */
    public function add()
    {
        $this->model->mForm('form');
    }

    /**
     * 编辑
     * @auth true
     */
    public function edit()
    {
        $this->model->mForm('form');
    }

    /**
     * 表单数据处理
     * @param array $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _form_filter(array &$data)
    {
        if ($this->request->isPost()) {
            // code...
        } else {
            // code...
        }
    }

    /**
     * 修改状态
     * @auth true
     */
    public function state()
    {
        $this->model->mSave($this->_vali([
            'status.in:0,1'  => '状态值范围异常！',
            'status.require' => '状态值不能为空！',
        ]));
    }

    /**
     * 删除
     * @auth true
     */
    public function remove()
    {
        $this->model->mDelete();
    }
}
