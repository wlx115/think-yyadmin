<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\service;

use think\helper\Str;
use think\admin\Exception;
use think\admin\Service;
use think\admin\model\SystemAddon;
use think\admin\model\SystemHook;
use think\admin\model\SystemMenu;
use think\admin\extend\DirExtend;
/**
 * 插件管理服务
 * Class AddonService
 * @package think\admin\service
 */
class AddonService extends Service
{
    //插件目录
  	protected $addons_path;
  	//已安装插件
  	protected $inaddons = array();
    //已实例化的插件
    static $_addons=[];
    /**
     * 初始化服务
     * @return static
     */
    protected function initialize()
    {
        $this->addons_path = $this->app->getRootPath().'addons'.DIRECTORY_SEPARATOR;
        if(!is_dir($this->addons_path)){
            DirExtend::dirCreate($this->addons_path);
        }
    }
    /** 
	 * 获取所有的钩子列表
	 */
	public function getHooks($map=[])
	{
		return SystemHook::mk()->where($map)->column('*','name');;
	}
    /** 
	 * 获取所有的已安装插件
	 */
	public function getAddons()
	{
        $this->inaddons = SystemAddon::mk()->column('*','name');
		return $this->inaddons;
	}
    /** 
	 * 获取插件目录
	 */
	public function getAddonsPath($name='')
	{
        if($name){
            return $this->addons_path .$name . DIRECTORY_SEPARATOR;
        }else{
            return $this->addons_path;
        }
	}
    /**
	 * 本地插件列表
	 * @return mixed
	 */
	public function local()
	{
		$addons = [];
        $installed = $this->getAddons();
		foreach (scandir($this->addons_path) as $file) {
			if ($file != '.' && $file != '..' && is_dir($this->addons_path . $file)) {
                // 已安装的插件
                $install = isset($installed[strtolower($file)]) ? true : false;
                $indexurl = addons_url($file.'://index/index');
                $adminurl = addons_url($file.'://admin/index');
				$addons[$file] = array_merge(get_addons_info($file),['install'=>$install,'adminurl'=>$adminurl,'indexurl'=>$indexurl]);
			}
		}
		return $addons;
	}
    /**
     * 安装插件
     * @param string $name 插件名称
     * @return bool
     */
    public function install($name='')
    {
        $addons = $this->local();
        $addon = $addons[$name] ?? [];
        
        if (!$addon && $addon['install'] ) throw new Exception('当前插件不存在或已安装！');

        $info = get_addons_info($name);
        // 当前插件控制器
        $object = get_addons_instance($name);
        $info['setting'] = $object->getConfig();
        // 插件依赖的基类控制器方法
        $base = get_class_methods("\\think\\Addons");
        // 读取出所有公共方法
        $methods = (array)get_class_methods($object);
        // 跟插件基类方法做比对，得到差异结果
        $addon_hooks = array_diff($methods, array_merge($base,['install','uninstall']));
        // 开启数据库事务操作
        $this->app->db->startTrans();
        try {
            //安装数据表
            $object->install();
            //添加插件到插件库
            SystemAddon::mk()->save($info);
            //更新钩子信息
            if (!empty($addon_hooks)) foreach ($addon_hooks as $hook) {
                $obj = SystemHook::mk()->where('name', $hook)->find();
                if($obj){
                    $obj->addons = array_merge($obj->addons,[$name]);
                    $obj->save();
                }else{
                    $hooks = [
                        'name'      => $hook,
                        'title'     =>  $name.'插件自定义行为',
                        'addons'    =>  $name
                    ];
                    SystemHook::mk()->save($hooks);
                }
            }
            if($info['is_admin']){
                $menu = [
                    'pid' => 163,
                    'title' => $info['title'].'管理',
                    'node'  => 'addons/'.$info['name'].'/admin/index',
                    'url'  => 'addons/'.$info['name'].'/admin/index',
                ];
                SystemMenu::mk()->insert($menu);
            }
            //提交事务
            $this->app->db->commit();
        } catch (\Exception $e) {
            $this->app->db->rollback();
            throw new Exception('安装异常:'.$e->getMessage() ?? '安装失败!');
        }
    }
    /**
     * 卸载插件
     * @param string $name
     * @return void
     */
	public function uninstall($name ='')
	{
		$addons = $this->local();
        $addon = $addons[$name] ?? [];
        if (!$addon || !$addon['install'] ){
            throw new Exception('当前插件不存在或已卸载！');
        }else{
            // 当前插件控制器
            $object = get_addons_instance($name);
            // 开启事务
            $this->app->db->startTrans();
            try {
                // 删除插件
                SystemAddon::mk()->where(['name' => $name])->delete();
                SystemMenu::mk()->where(['node'=>'addons/'.$name.'/admin/index'])->delete();
                // 更新钩子绑定的插件
                $hookObj = SystemHook::mk()->whereFindinSet('addons', $name)->select();
                foreach ($hookObj as $hook) {
                    $hook->addons = array_diff($hook->addons, [$name]);
                    $hook->save();
                }
                //删除数据表
                $object->uninstall();
                //提交事务
                $this->app->db->commit();
            } catch (Exception $e) {
                $this->app->db->rollback();
                throw new Exception('卸载异常:'.$e->getMessage() ?? '卸载失败');
            }
        }
	}

    /**
	 * 预览插件
	 * @param array $data
	 * @return bool|string
	 */
	public function preview($data = [])
	{
		$data['status'] = 1;
		$data['hooks'] = isset($data['hooks']) ? $data['hooks'] : [];
		$data['is_config'] = isset($data['is_config']) ? $data['is_config'] : 0;
		$data['is_index'] = isset($data['is_index']) ? $data['is_index'] : 0;
		$data['is_admin'] = isset($data['is_admin']) ? $data['is_admin'] : 0;
		$data['is_weixin'] = isset($data['is_weixin']) ? $data['is_weixin'] : 0;

		return $this->plugin_create_preview($data);
	}
    /**	
	 * 生成插件
	 */
    public function build($data = [])
    {
        $addon = $data['name'];
        //插件目录中的插件目录
        $addons_path = $this->addons_path.$addon.DIRECTORY_SEPARATOR;
		if(!is_dir($addons_path)){
            //创建插件基础目录
			$files = [$addons_path,$addons_path.'controller',$addons_path.'view',$addons_path.'model',$addons_path.'service'];
			foreach ($files as $dir) {
                DirExtend::dirCreate($dir);
			}
			// 生成插件入口文件
			$PluginContent = $this->preview($data);
            DirExtend::createFile($addons_path ."/Plugin.php", $PluginContent);
			// 如果有配置文件
			if (isset($data['is_config'] ) && $data['is_config'] == 1) {
                $tplStr = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'tpl'.DIRECTORY_SEPARATOR.'addon_config.tpl');
				DirExtend::createFile($addons_path ."config.php", $tplStr);
			}
			// 如果存在后台
			if (isset($data['is_admin']) && $data['is_admin'] == 1) {
                list($namespace,$controller,$title) = ["addons\\{$addon}\\controller",'Admin',$data['title'].'管理'];
                $tplStr = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'tpl'.DIRECTORY_SEPARATOR.'addon_controller.tpl');
                $tplStr = str_replace(['[namespace]','[controller]','[title]','[name]'],[$namespace,$controller,$title,$addon],$tplStr);
				DirExtend::createFile($addons_path ."controller".DIRECTORY_SEPARATOR."Admin.php", $tplStr);
			}
			// 如果存在前台
			if (isset($data['is_index']) && $data['is_index'] == 1) {
				list($namespace,$controller,$title) = ["addons\\{$addon}\\controller",'Index',$data['title']];
                $tplStr = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'tpl'.DIRECTORY_SEPARATOR.'addon_controller.tpl');
                $tplStr = str_replace(['[namespace]','[controller]','[title]','[name]'],[$namespace,$controller,$title,$addon],$tplStr);
				DirExtend::createFile($addons_path ."controller".DIRECTORY_SEPARATOR."Index.php", $tplStr);
			}
			return [true,'创建插件成功'];
		}else{
            return [false,'插件' . $data['name'] . '目录已存在'];
		} 
    }
	/**
	 * 插件配置文件生成
	 * @param string $data 插件数据
	 * @param string $controller 控制器
	 * @param string $action 动作
	 * @author 蔡伟明 <314013107@qq.com>
	 * @return bool
	 */

	protected function plugin_create_preview($data = [])
	{
		$data['status'] = 1;
		$data['hooks'] = isset($data['hooks']) ? $data['hooks'] : [];
		$hookMethod = '';
		foreach ($data['hooks'] as $value) {
			$hookMethod .= "// 实现的 {$value} 钩子方法\n\tpublic function {$value}(){\n\t\t\n\t}";
		}
		$namespace = 'addons\\' . $data['name'];
        $tplStr = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'tpl'.DIRECTORY_SEPARATOR.'addon_plugin.tpl');
        $tplStr = str_replace(['{$namespace}','{$hookfun}'],[$namespace,$hookMethod],$tplStr);
        preg_match_all('/\{\$([a-zA-Z_\d+]+)\}/i', $tplStr, $matches);
        // pr($matches);
        if($matches)foreach($matches[1] as $k=>$v){
            if(isset($data[$v])){
                $tplStr =  str_replace($matches[0][$k],$data[$v],$tplStr);
            }
        }
        return $tplStr;
    }   

    /**
	 * 在线插件
	 */
	public function online()
	{
		$current_version = ['copyrihgt'=>''];//getversion();
        $localAddons = $this->local();
		if(isset($current_version['copyright'])){
			$content = unlock_url(file_get_contents('http://domain.qhxckj.com/addons?copyright='.$current_version['copyright'].'&domain='.$_SERVER['HTTP_HOST']));
			$addons = unserialize($content);
			foreach ($addons as &$val) {
                $val['local'] = isset($localAddons[$val['name']]) ? 1 : 0;
			    $val['install'] = isset($localAddons[$val['name']['install']]) ? 1 : 0;
			}
		}else{
			$addons = [];
		}
        return $addons;
	}
}