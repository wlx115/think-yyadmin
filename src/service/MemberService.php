<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\service;

use think\admin\Service;
use think\admin\extend\DataExtend;
use think\admin\model\SystemMember;

/**
 * 系统会员管理服务
 * Class MemberService
 * @package think\admin\service
 */
class MemberService extends Service
{
    /**
     * 认证有效时间
     * @var integer
     */
    private $expire = 7200;

    /**
     * 是否已经登录
     * @return boolean
     */
    public function isLogin(): bool
    {
        return $this->getMemberId() > 0;
    }

    /**
     * 获取登录会员ID
     * @return integer
     */
    public function getMemberId(): int
    {
        return intval($this->app->session->get('member.id', 0));
    }

    /**
     * 获取登录会员名称
     * @return string
     */
    public function getMemberrName(): string
    {
        return $this->app->session->get('member.username', '');
    }

    /**
     * Token获取
     * @return $string
     * 
     */
    public function checkAccessToken()
    {
        $token = $this->app->request->header('token','');
        $action = $this->app->request->action();
        if($action != 'access_token'){ 
            $access_token = $this->app->cache->get($token);
            $time = time();
            if(!$access_token || $token != $access_token['access_token'] || $time > $access_token['expires_time']){
                return false;
            }else{
                return true;
            }
        }
        return true;
    }

    /**
     * Token获取
     * @return $string
     * 
     */
    public function setAccessToken()
    {
        $appKey = $this->app->request->post('appKey');
        if(!$appKey){
            return [0, 'appKey错误'];
        }else{
            $token = md5($appKey.'10000');
            $access_token = ['access_token'=>$token,'expires_time'=>time()+7200,'appKey'=>$appKey];
            $this->app->cache->set($token,$access_token,7200);
            return [1, ['access_token'=>$token,'expires_time'=>7200]];
        }
    }

    /**
     * 检查 TOKEN 是否有效
     * @param string $token 认证令牌
     * @param array $data 认证数据
     * @return array [ 检查状态，状态描述，会员UID, 有效时间 ]
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function checkToken(string $token, array $data = []): array
    {
        if (empty($data)) {
            $map = ['token' => $token];
            $data = SystemMember::mk()->where($map)->findOrEmpty()->toArray();
        }
        if (empty($data) || empty($data['uid'])) {
            return [-1, 'Token认证无效,请重新登录', 0, 0];
        } elseif ($token !== 'token' && $data['tokentime'] < time()) {
            return [-1, 'Token认证失效,请重新登录', 0, 0];
        } elseif ($token !== 'token' && $data['tokenv'] !== $this->_buildTokenVerify()) {
            return [-1, '客户端已更换,请重新登录', 0, 0];
        } else {
            $this->expireUpdate($token);
            return [1, 'Token验证成功', $data['id'], $data['tokentime']];
        }
    }

    /**
     * 获取令牌的认证值
     * @return string
     */
    private function _buildTokenVerify(): string
    {
        return md5($this->app->request->server('HTTP_USER_AGENT', '-'));
    }

    /**
     * 延期 TOKEN 有效时间
     * @param string $token 授权令牌
     * @throws DbException
     */
    protected function expireUpdate(string $token)
    {
        $map = ['token' => $token];
        SystemMember::mk()->where($map)->update([
            'tokentime' => time() + $this->expire,
        ]);
    }

    /**
     * 生成新的会员令牌
     * @param int $uuid 授权会员
     * @return array [创建状态, 状态描述, 令牌数据]
     * @throws DbException
     */
    public function token(int $uuid): array
    {
        // 清理无效认证数据
        $time = time();
        $map1 = [['token', '<>', 'token'], ['time', '<', $time]];
        $map2 = [['token', '<>', 'token'], ['uid', '=', $uuid]];
        SystemMember::mk()->whereOr([$map1, $map2])->update(['token'=>'','tokenv'=>'','tokentime'=>0]);
        // 创建新的认证数据
        do $map = ['token' => md5(uniqid() . rand(100, 999))];
        while (SystemMember::mk()->where($map)->count() > 0);
        // 写入会员认证数据
        $data = array_merge($map, ['id' => $uuid, 'tokentime' => $time + $this->expire, 'tokenv' => $this->_buildTokenVerify()]);
        if (SystemMember::mk()->save($data) !== false) {
            return [1, '刷新认证成功', $data];
        } else {
            return [0, '刷新认证失败', []];
        }
    }
}