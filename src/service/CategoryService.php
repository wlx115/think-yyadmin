<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace think\admin\service;

use think\admin\extend\DataExtend;
use think\admin\Service;
use think\admin\model\SystemCategory;
use think\admin\model\SystemDepart;

/**
 * 系统分类管理服务
 * Class CategoryService
 * @package app\admin\service
 */
class CategoryService extends Service
{
    /**
     * 获取模型所属栏目
     * @param $mid  模型ID
     * @param $fields 查询的字段
     * @return array
     */
    public function getModuleCategory($mid='',$fields='*')
    {
        return SystemCategory::mk()->where(['status' => '1','mid'=>$mid])->order('sort desc,id asc')->column($fields,'id');
    }

    /**
     * 通过ID获取栏目
     * @param int $id
     * @return array
     */
    public function getCategory($id='')
    {
        $category =  $this->getCache();
        return $id ? $category[$id] : $category;
    }
    /**
     * 通过别名获取栏目
     * @param int $alias
     * @return array
     */
    public function getCategoryByAlias($alias='')
    {
        $category =   $this->getCache(true);;
        return $alias ? $category[$alias] : $category;
    }
    /**
     * 获取子栏目
     * @param string|array $id 子栏目ID
     * @return array
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getSubCategory($id='')
    {
        $categorys =   $this->getCategory($id);
        $subs = [];
        foreach($categorys as $val){
            if($val['pid'] == $id) $subs[] = $val;
        }
        return $subs;
    }

    /**
     * 获取分类Select
     * @param $id 默认数据
     * @param $mid 模型ID
     * @param $group 是否分组
     * @return array
     */
    public function getSelect($id='',$mid='',$group=true)
    {
        $siteId = SiteService::instance()->getSiteId();
        $maps = $mid ? ['mid' => $mid,'status' => '1','siteid'=>$siteId] : ['status' => '1','siteid'=>$siteId];
        $query = SystemCategory::mk()->where($maps)->order('sort desc,id asc')->field('id,pid,icon,url,catname,arrchildid,mid');
        if(!AdminService::instance()->isSuper()){
            $useAuth = SystemDepart::mk()->where('id',$this->app->session->get('user.departid'))->value('auth'); 
            $query->whereIn('id',$useAuth);
        }
        $menus = $query->select()->toArray();
        $menus = DataExtend::arr2table($menus);

        //过滤外部分类
        foreach ($menus as $key => &$menu) {
            if ($menu['spt'] >= 3 || $menu['url'] !== '#') unset($menus[$key]);
        }

        $str = '';
        foreach ($menus as $key => $val) {
            if(count($val['arrchildid']) > 1 && $group && $val['mid'] != '22'){
                $str .="<optgroup label=\"".$val['spl'] . $val['catname']."\"></optgroup>";
            }else{
                if($val['id'] == $id){
                    $str .= "<option selected value='".$val['id']."'>".$val['spl'] . $val['catname']."</option>";
                }else{
                    $str .= "<option value='".$val['id']."'>".$val['spl'] . $val['catname']."</option>";
                }
            }

        }
        return $str;
    }

    /**
     * 栏目缓存
     * @param bool $alias  返回数据主键类型，true为别名，false为主键ID
     * @return $this
     */
    public function getCache($alias=false)
    {
        $siteId = SiteService::instance()->getSiteId();
        $map = ['status'=>1,'siteid'=>$siteId];
        $categorys = $this->app->cache->get('site_categorys_'.$siteId);
        if(!$categorys){
            $idCates = SystemCategory::mk()->getAllCate( $map );
            $aliasCates =  array_combine(array_column($idCates,'catdir'),$idCates);
            $categorys = ['id'=>$idCates,'alias'=>$aliasCates];
            $this->app->cache->set('site_categorys_'.$siteId,$categorys);   
        }
        return $alias ? $categorys['alias'] : $categorys['id'];
    }
    /**
     * 清理栏目缓存
     * @return $this
     */
    public function clearCache()
    {
        $siteId = SiteService::instance()->getSiteId();
        $this->app->cache->delete('site_categorys_'.$siteId);
    }
    
    /**
     * 栏目修正
     * 修改栏目的子集，父级和父级目录
     */
    public function repair() {
        @set_time_limit(500);
        $categorys = SystemCategory::mk()->column('id,pid,catdir','id');

        if(is_array($categorys)) {
            foreach($categorys as $id => $cat) {
                $allparent = DataExtend::getArrParents($categorys,$cat['pid']);
                $arrparentid = array_merge([0],array_column($allparent,'id'));
                $parentdir = array_column($allparent,'catdir');
                $arrchildid = DataExtend::getArrSubIds($categorys,$id);
                SystemCategory::mk()->where('id',$id)->save([
                    'parentdir'=>$parentdir ? implode('/',$parentdir).'/':'',
                    'arrparentid'=>implode(',',$arrparentid),
                    'arrchildid'=>implode(',',$arrchildid)
                ]);
            }
        }
    }
    /**
     * 找某一个父级的直接子集
     * @param array $data 当前所有分类
     * @param array $pid 系父级ID
     * @return array
     */
    public function getSon($data,$pid=0){
    	return DataExtend::getArrSub($data,$pid);
    }
    /**
     * 获取某个分类的所有子集
     * @param array $data 当前所有分类
     * @param array $pid 系父级ID
     * @return array
     */
    public function getSons($data,$pid=0){
        return DataExtend::getArrSubs($data,$pid);
    }
    /**
     * 获取某一个子类的所有父级  递归
     * @param array $data 当前所有分类
     * @param array $pid ID
     * @return array
     */
    public function getParents($data,$pid=0){
        return DataExtend::getArrParents($data,$pid);
    }
}
