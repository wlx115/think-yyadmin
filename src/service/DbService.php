<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\service;

use think\admin\Exception;
use think\exception\HttpResponseException;
use think\admin\Service;

/**
 * 数据库管理服务
 * Class DbService
 * @package app\admin\service
 */
class DbService extends Service
{

    /**
     * 数据库配置
     */
    public $config=[];

    //字段类型
    public $fieldType = [
        'text'      =>  '单行文本',
        'textarea'  =>  '多行文本',
        'decimal'   =>  '数字',
        'checkbox'  =>  '复选框',
        'radio'     =>  '单选按钮',
        'date'      =>  '日期',
        'datetime'  =>  '日期+时间',
        'select'    =>  '下拉框',
        'image'     =>  '单张图片',
        'images'    =>  '多张图片',
        'file'      =>  '单个文件',
        'files'     =>  '多个文件',
        'editor'    =>  '编辑器',
        'vote'      =>  '投票器',
    ];
    //字段属性
    protected $fieldDefine = [
        'text'      =>  "varchar(128) NULL",
        'textarea'  =>  "text NULL",
        'decimal'   =>  'decimal(20,2) NULL',
        'checkbox'  =>  "varchar(100) NULL",
        'radio'     =>  "varchar(32) NULL",
        'date'      =>  "int(11) UNSIGNED NULL",
        'datetime'  =>  "int(11) UNSIGNED NULL",
        'select'    =>  "varchar(32) NULL",
        'image'     =>  "varchar(200) NULL",
        'images'    =>  "text NULL",
        'file'      =>  "int(11) UNSIGNED NULL",
        'files'     =>  "text NULL",
        'editor'    =>  "longtext NULL",
        'vote'      =>  "text NULL",
    ];
    /**
     * 验证规则
     */
    protected $fieldRule = [
        'number'   =>  '纯数字',
        'email'     =>  'email地址',
        'date'      =>  '有效的日期',
        'url'       =>  'URL地址',
        'mobile'    =>  '手机号',
        'idCard'    =>  '身份证格式',
    ];
    /**
     * 数据库服务初始化
     */
    protected function initialize()
    {
        $this->config = $this->getConfig();
    }
    /** 
     * 返回自动类型和属性
     */
    public function getFieldDefine()
    {
        return [$this->fieldType,$this->fieldDefine,$this->fieldRule];
    }

    /**
     * 创建数据表
     * @param string $name 数据表名
     * @param array $fields 表字段数据[['name'=>'fieldName','define'=>'archar(128) NULL','comment'=>'字段说明']]
     * @param string $title 表备注名
     * @throws \think\admin\Exception
     */
    public function createTable(string $name,string $title,array $fields=[])
    {
        $table = $this->getTable($name);
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `{$table}` (
                `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
                `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
                `update_at` timestamp NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
                PRIMARY KEY (`id`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET={$this->config['charset']} COMMENT='{$title}';";
            // pr($sql);
            $this->app->db->query($sql);
            //加入自定义的表字段
            foreach ($fields as $field) {
                $this->createField($table,$field);
            }
        }catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * 修改数据表
     * @param string $name 数据表名
     * @param array $fields 表字段数据[['name'=>'fieldName','define'=>'archar(128) NULL','comment'=>'字段说明']]
     * @param string $title 表备注名
     * @throws \think\admin\Exception
     */
    public function updateTable(string $from,string $to)
    {
        $from = $this->getTable($from);
        $to = $this->getTable($to);
        $tables = $this->getTables();
        
        if (in_array($to, $tables)) {
            throw new Exception("数据表 {$to} 已存在！");
        } else {
            if (!in_array($from, $tables)) {
                throw new Exception("数据表 {$from} 不存在！");
            }else{
                try {
                    $this->app->db->execute("rename table {$from} to {$to};");
                } catch (Exception $e) {
                    throw $e;
                }
            }
        }
    }

    /**
     * 删除数据表
     * @param string $name 数据表名
     * @return bool
     */
    public function deleteTable(string $name)
    {
        $table = $this->getTable($name);
        try {
            $this->app->db->execute("DROP TABLE IF EXISTS `{$table}`");
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * 创建字段
     * @param string $table 数据表名
     * @param array $field 字段数据
     * @return bool
     */
    public function createField(string $table,array $field, array $tables = [])
    {
        $table = $this->getTable($table);
        $tables = $this->getTables();
        if (in_array($table, $tables)) {
            list($name,$define,$comment) = [$field['name'],$field['define'],$field['comment']??$field['title']];
            try {
                $this->app->db->execute("ALTER TABLE `{$table}` ADD COLUMN `{$name}` {$define} COMMENT '{$comment}'");
            } catch (Exception $e) {
                throw $e;
            } 
        } else {
            throw new Exception("数据表 {$table} 不存在！");
        }
    }

    /**
     * 更新字段
     * @param string $table 数据表名
     * @param string $from 要修改的字段名
     * @param array $to 修改后的字段名[name,define,title]
     * @return bool
     */
    public function updateField(string $table,string $from,array $to=[])
    {
        $table = $this->getTable($table);
        $tables = $this->getTables();

        if (in_array($table, $tables)) {
            //第一步查询数据库表中的所有字段
            $fields = $this->app->db->getTableInfo($table,'fields');
            list($field,$define,$comment) = [$to['name'],$to['define'],$to['comment']??$to['title']];
            //判断是否存在要改的字段，不存在则新建字段
            if(!in_array($from,$fields)){
                $sql = "ALTER TABLE `{$table}` ADD COLUMN `{$field}` {$define} COMMENT '{$comment}'";
            }elseif(!in_array($field,$fields)){
                $sql = "ALTER TABLE `{$table}` CHANGE COLUMN `{$from}` `{$field}` {$define} COMMENT '{$comment}'";
            }else{
                throw new Exception("字段名已存在 {$field}");
            }
            try {
                $this->app->db->execute($sql);
            } catch (Exception $exception) {
                throw $exception;
            }
        } else {
            throw new Exception("数据表 {$table} 不存在！");
        }
    }

    /**
     * 删除字段
     * @param null $field 字段数据
     * @return bool
     */
    public function deleteField(string $table,string $field)
    {
        $table = $this->getTable($table);
        $fields = $this->app->db->getTableInfo($table,'fields');
        //判断是否存在要删除的字段
        if(in_array($field,$fields)){
            try {
                $this->app->db->execute("ALTER TABLE `{$table}` DROP COLUMN `{$field}`");
            } catch (Exception $exception) {
                throw $exception;
            }
        }
    }

    /**
     * 获取带表前缀的表名
     * @param string $name
     * @return array
     */
    protected function getTable(string $name)
    {
        $perfix = $this->config['prefix'];
        $lenth = strlen($perfix);
        return (substr($name,0,$lenth) == $perfix) ? $name : $perfix . $name;
    }

    /**
     * 获取数据库所有数据表
     * @return array [table, total, count]
     */
    protected function getTables(): array
    {
        $tables = [];
        foreach ($this->app->db->query("show tables") as $item) {
            $tables = array_merge($tables, array_values($item));
        }
        return $tables;
    }

    /**
     * 获取连接配置
     * @param string $name
     * @return array
     */
    protected function getConfig()
    {
        $name = $this->app->config->get('database.default','mysql');
        $connections = $this->app->config->get('database.connections',[]);
        if (!isset($connections[$name])) {
            throw new Exception("无法连接到{$name}");
        }
        return $connections[$name];
    }
}