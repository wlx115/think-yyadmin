<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace think\admin\service;

use think\admin\Service;

/*
* 字段自动化服务
* Class FieldService
* @package think\admin\service
*/

class FieldService extends Service
{
    public $prefix;
    /**
     * 设定的字段类型
     */
    public $fieldType = [
        'text'  =>  '单行文本',
        'textarea'  =>  '多行文本',
        'decimal'  =>  '数字',
        'checkbox'  =>  '复选框',
        'radio'  =>  '单选按钮',
        'date'  =>  '日期',
        'datetime'  =>  '日期+时间',
        'time'  =>  '时间',
        'hidden'  =>  '隐藏',
        'select'  =>  '下拉框',
        'image'  =>  '单张图片',
        'images'  =>  '多张图片',
        'file'  =>  '单个文件',
        'editor'  =>  '编辑器',
        'vote'  =>  '投票',
    ];
    /**
     * 字段数据库类型
     */
    protected $field_define = [
        'text'=>"varchar(128) NULL",
        'textarea'=>"text NULL",
        'decimal'=>'decimal(20,2) NULL',
        'checkbox'=>"varchar(100) NULL",
        'radio'=> "varchar(32) NULL",
        'date'=>"date NULL",
        'time'=> "time NULL",
        'datetime'=> "datetime NULL",
        'hidden'=> "varchar(32)  NULL",
        'select'=> "varchar(32) NULL",
        'image'=>"varchar(200) NULL",
        'images'=>"text NULL",
        'file'=>"varchar(200) NULL",
        'editor'=> "longtext NULL",
        'vote'=> "text NULL",
    ];
    /**
     * 初始化
     */
    protected function initialize()
    {
        $this->prefix = $this->app->config->get('database.connections.mysql.prefix');
    }
    /** 
     * 返回所有扩展
     */
    public function getFieldDefine()
    {
        return [$this->fieldType,$this->field_define];
    }

    /**
     * 获取表单数据
     * @param array $fields 模型ID
     * @param array $data 提交的数据
     * @return array
     */
    public function getFormData(array $fields,array $data=[]): array
    {
        $rules = [];
        // 处理特殊字段类型
        foreach ($fields as $val) {
            list($type,$name,$title,$default,$required) = [ $val['type'], $val['name'], $val['title'],$val['default'] ,$val['required']];
            $value = $data[$name] ?? $default;

            //数组转换
            if (is_array($value)) {
                $data[$name] = serialize($value);
            }
            //开关盒子
            if($type == 'switch'){
                $data[$name] = 1;
            }elseif($type == 'date'){
                $data[$name] = $value ? $value : date('Y-m-d',time());
            }elseif($type == 'time'){
                $data[$name] = $value ? $value : date('H:i:s',time());
            }elseif($type== 'datetime'){
                $data[$name] = $value ? $value : date('Y-m-d :H:i:s',time());
            }elseif($type== 'checkbox'){
                $data[$name] = $value ? $value : [];
            }else{
                $data[$name] = $value;
            }
            if($required){
                $rules[$name.'.require'] = $title.'不能为空';
            }
        }

        return [$data,$rules];
    }

    /**
     * 字段配置格式化
     * @param array $field 字段信息
     * @param array $data 默认数据
     * @return array
     */
    public function formatFields($fields=[],$data=[])
    {
        foreach ($fields as &$vo) {
            if(in_array($vo['type'],['radio','select','checkbox']) && !is_array($vo['options'])){
                $options = explode(',',str_replace(array("\r\n"),',',$vo['options']));
                $newArr = [];
                foreach($options as &$val){
                    list($v,$k) = explode('|',$val.'|'.$val);
                    $newArr[$k] = $v;
                }
                $vo['options'] = $newArr;
            }
            //字段的位置
            $vo['postion'] = isset($vo['group']) && $vo['group'] > 0 ? 'extend' : 'base';
            $vo['hidden'] = $vo['hidden'] ?? 0;
            $vo['html'] = $this->createForm($vo,$data);
        }
        return $fields;
    }
    /**
     * 数据操作字段信息生成
     * @param string $name 字段名
     * @param string $title 字段中文名称    
     * @param string $type 字段类型
     * @param string $options 字段配置
     * @param integer $required 是否必填
     * @param string $default 默认值
     * @param integer $list 数据列表是否显示
     * @param integer $search  允许搜索
     * @param string $align  数据列表对齐方式
     * @param integer $width  数据列表显示的宽度
     * @param integer $sort 数据列表是否允许排序
     * @return void
     */
    public function setField($name,$title,$type='text',$options='',$required=0,$default='',$list=0,$search=0,$align="left",$width=0,$sort=0)
    {
        return [
            'name' => $name,
            'title' => $title,
            'type' => $type,
            'options' => $options,
            'required' => $required,
            'default' => $default,
            'list' => $list,
            'search' => $search,
            'align' =>  $align,
            'width' => $width > 0?$width :'',
            'sort'  => $sort,
            'hidden' => $type == 'hidden' ? 1 : 0,
            'define'    =>  $this->field_define[$type] ?? 'varchar(200) NULL'
        ];
    }
    /**
     * 按钮动态生成
     *
     * @param array $data  按钮配置
     * @return void
     */
    public function setButton($data,$size='sm')
    {
        $str = '';
        foreach($data as $key => $vo){
            list($type,$title,$url,$query,$style,$w,$h) = array_replace(['modal','按钮','','','primary','',''],$vo);
            if(auth($key)){
                $str .= "<button data-{$type}='{$url}?{$query}' data-title='{$title}' class='layui-btn layui-btn-{$size} layui-btn-{$style}'";
                switch ($type) {
                    case 'action':
                        $str .= " data-confirm='确定要删除这些信息吗？' ";
                        $str .= $size == 'sm' ? " data-rule='id#{key}'" : " data-value='id#{{d.id}}'";
                        break;
                    case 'iframe':
                        $w = !empty($w) ?: '1000px'; $h =!empty($h) ?: '80%';
                        $str .= " data-width='{$w}' data-height='{$h}' ";
                        break;
                    case 'modal':
                        $str .= " data-width='{$w}' data-height='{$h}' ";
                        break;
                }
                $str .=">{$title}</button>";
            }
        }
        return $str;
    }
    /**
     * 字段数据格式化
     * @param $field 字段信息
     * @param $data 数据
     * @return array
     */
    protected function createForm($field=[],$data=[])
    {
        //默认值
        if($field['default'] && strpos($field['default'],':') > -1){
            $fun = str_replace(':','',$field['default']);
            $field['default'] = $fun();
        }
        
        $field['value'] = isset($data[$field['name']]) ? $data[$field['name']] : $field['default'];

        // 解析options
        if (isset($field['options']) && !is_array($field['options'])) {
            $field['options'] = parse_attr($field['options']);
        }
        //字段说明
        $field['tips'] = !empty($field['tips']) ? $field['tips'] : '请输入'.$field['title'];
        //必填自动属性
        $field['required'] = isset($field['required']) && $field['required'] ? ' required ' : '';
        //字段类型初始化
        switch ($field['type']) {
            case 'linkage':// 解析联动下拉框异步请求地址
                if (!empty($field['ajax_url']) && substr($value['ajax_url'], 0, 4) != 'http') {
                    $value['ajax_url'] = url($field['ajax_url']);
                }
                break;
            case 'date':
                $field['value'] = !empty($field['value']) ? (is_numeric($field['value']) ? date('Y-m-d',$field['value']) : $field['value']) : date('Y-m-d',time());
                break;
            case 'time':
                $field['value'] = !empty($field['value']) ? (is_numeric($field['value']) ? date('H:i:s',$field['value']) : $field['value']) : date('H:i:s',time());
                break;
            case 'datetime':
                $field['value'] = !empty($field['value']) ? (is_numeric($field['value']) ? date('Y-m-d H:i:s',$field['value']) : $field['value']) : date('Y-m-d H:i:s',time());
                break;
            case 'bmap':
                $field['level'] = $field['level'] == 0 ? 12 : $field['level'];
                break;
            case 'colorpicker':
                $field['mode']  = 'rgba';
                break;
            case 'image':
            case 'images':
                $field['allow']  = 'png,jpg,jpeg,gif,bmp,ico';
                break;
            case 'file':
            case 'files':
                $field['allow']  = 'png,jpg,jpeg,gif,bmp,ico,doc,docx,xls,xlxs,pdf,rar,zip,mp3,mp4,swf,per,pem,p12';
                break;
            case 'vote':
                $field['tips'] = "投票选项一行一个，格式为：选项名称|投票数量，如下:\n很好|1\n非常好|2\n一般|3\n不好|4";
                break;
            case 'ip':
                $field['value']  = !empty($field['value']) ?: $this->app->request->ip();
                break;
        }
        $funs = '_'.$field['type'];
        if(isset($field['hidden']) && $field['hidden']){
            return $this->_hidden($field);
        }elseif(method_exists($this,$funs)){
            return $this->$funs($field);
        }else{
            return $this->_text($field);
        } 
    }

    /**
     * 隐藏文本域
     */
    protected function _hidden($field)
    {
        $value = is_array($field['value']) ? implode(',',$field['value']) : $field['value'];
        return '<input type="hidden" '.$field['required'].' name="'.$field['name'].'" value="'.$value.'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 普通文本框
     */
    protected function _text($field)
    {
       return '<input type="text" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 多行文本框
     */
    protected function _textarea($field)
    {
       return '<textarea '.$field['required'].' name="'.$field['name'].'" placeholder="'.$field['tips'].'" class="layui-textarea">'.$field['value'].'</textarea>';
    }
    /**
     * 数字文本框
     */
    protected function _decimal($field)
    {
       return '<input type="text" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 日期文本框
     */
    protected function _date($field)
    {
       return '<input type="text" data-date-input="date" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 时间文本框
     */
    protected function _time($field)
    {
       return '<input type="text" data-date-input="time" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 日期+时间文本框
     */
    protected function _datetime($field)
    {
       return '<input type="text" data-date-input="datetime" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="'.$field['tips'].'" class="layui-input">';
    }
    /**
     * 多选框
     */
    protected function _checkbox($field)
    {
        $html = '';
        foreach($field['options'] as $k => $r){
            $selected = $k == $field['value'] ? ' checked ' : '';
            $html.='<input type="checkbox" '.$selected.' name="'.$field['name'].'[]" value="'.$k.'" title="'.$r.'" lay-skin="primary" lay-filter="'.$field['name'].'">';
        }
       return $html;
    }
    /**
     * 单选框
     */
    protected function _radio($field)
    {
        $html = '';
        foreach($field['options'] as $k => $r){
            $selected = $k == $field['value'] ? ' checked ' : '';
            $html.='<input type="radio" '.$selected.' name="'.$field['name'].'" value="'.$k.'" title="'.$r.'" lay-skin="primary" lay-filter="'.$field['name'].'">';
        }
       return $html;
    }
    /**
     * 下拉列表
     */
    protected function _select($field)
    {
        $html = '<select class="layui-select" name="'.$field['name'].'" lay-filter="'.$field['name'].'">';
        $html .= '<option value="">--请选择'.$field['title'].'--</option>';
        foreach($field['options'] as $k => $r){
            $selected = $k == $field['value'] ? ' selected ' : '';
            $html.='<option '.$selected.' value="'.$k.'">'.$r.'</option>';
        }
        $html.='</select>';
       return $html;
    }
    /**
     * 单张图片上传
     */
    protected function _image($field)
    {
        $html = '<input type="text" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="请上传或输入图片地址" class="layui-input">';
        $html .= '<a class="input-right-icon layui-icon layui-icon-upload margin-right-30" data-field="'.$field['name'].'" data-file data-type="'.$field['allow'].'" title="上传"></a>';
        $html .= '<a class="input-right-icon layui-icon layui-icon-picture" data-field="'.$field['name'].'" data-tips-image title="预览"></a>';
       return $html;
    }
    /**
     * 多张图片上传
     */
    protected function _images($field)
    {
        return '<textarea '.$field['required'].' name="'.$field['name'].'" placeholder="'.$field['tips'].'" class="layui-hide" data-type="'.$field['allow'].'">'.$field['value'].'</textarea><script>$("[name=\''.$field['name'].'\']").uploadMultipleImage();</script>';
    }
    /**
     * 单文件上传
     */
    protected function _file($field)
    {
        $html = '<input type="text" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="请上传或输入文件地址" class="layui-input">';
        $html .= '<a class="input-right-icon layui-icon layui-icon-upload" data-field="'.$field['name'].'" data-file data-type="'.$field['allow'].'" title="上传"></a>';
        return $html;
    }
    /**
     * 多文件上传
     */
    protected function _files($field)
    {
        return '<textarea '.$field['required'].' name="'.$field['name'].'" placeholder="'.$field['tips'].'" class="layui-hide" data-type="'.$field['allow'].'">'.$field['value'].'</textarea><script>$("[name=\''.$field['name'].'\']").uploadMultipleImage();</script>';
    }
    /**
     * 编辑器
     */
    protected function _editor($field)
    {
        $html = '<textarea '.$field['required'].' data-editor="'.$field['name'].'" name="'.$field['name'].'" placeholder="'.$field['tips'].'" class="layui-textarea">'.$field['value'].'</textarea>';
        return $html;
    }
    /**
     * 投票选项
     */
    protected function _vote($field)
    {
        $html = '<textarea '.$field['required'].' name="'.$field['name'].'" placeholder="'.$field['tips'].'" class="layui-textarea">'.$field['value'].'</textarea>';
        $html .= '<p class="help-block">投票选项一行一个，格式为：选项名称|投票数量</p>';
        return $html;
    }
    /**
     * IP
     */
    protected function _ip($field)
    {
        $html = '<input type="text" '.$field['required'].' name="'.$field['name'].'" value="'.$field['value'].'" placeholder="请上传IP地址" class="layui-input">';
        return $html;
    }
}