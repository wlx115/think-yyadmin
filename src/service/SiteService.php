<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\service;

use think\admin\Service;
use think\admin\model\SystemSite;

/**
 * 系统站点管理服务
 * Class SiteService
 * @package app\admin\service
 */
class SiteService extends Service
{
    /**
     * 站点数据缓存
     * @var array
     */
    protected $dataIds = [];
    protected $dataDomains = [];

    /**
     * 获取所有站点
     * @param string $type;获取类型
     * @return array
     */
    public function allSite(string $type = 'id')
    {
        if ($type == 'id') {
            return SystemSite::mk()->cache('SystemSiteID')->column('*','id');
        } else {
            return SystemSite::mk()->cache('SystemSiteDomain')->column('*','domain');
        }
    }
    /**
     * 通过当前域名获取站点信息
     * @param string $host;域名信息
     * @param string $field;返回字段字名
     * @return array
     */
    public function getSiteByDomain(string $host='',string $field ='')
    {
        $sites = $this->allSite('host');
        $domain = !empty($host) ? $host : $this->app->request->host();
        $site = $sites[$domain] ?? [];
        return $site[$field] ?? [];
    }
    /**
     * 通过站点ID获取站点信息
     * @param int $siteid;站点ID
     * @param string $field;返回字段字名
     * @return array
     */
    public function getSiteById(int $siteid = 0,string $field ='')
    {
        $siteid = $siteid > 0 ? $siteid : $this->getSiteId();
        $sites = $this->allSite();
        $site = $sites[$siteid] ?? [];
        return $site[$field] ?? $site;
    }
    /**
     * 获取当前站点ID
     */
    public function getSiteId()
    {
        $siteid = !empty($this->getSiteByDomain('','id')) ?:1;
        return $this->app->session->get('manage_siteid',$siteid);
    }
    /**
     * 清理站点缓存
     */
    public function clearSite()
    {
        $this->app->cache->delete('SystemSite');
        $this->allSite();
        return true;
    }
}