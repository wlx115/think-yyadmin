<?php

// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/zoujingli/ThinkLibrary
// | github 仓库地址 ：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

use think\admin\extend\CodeExtend;
use think\admin\extend\HttpExtend;
use think\admin\extend\DataExtend;
use think\admin\extend\PinyinExtend;
use think\admin\extend\SplitWord;
use think\admin\extend\DirExtend;
use think\admin\Helper;
use think\admin\service\AdminService;
use think\admin\service\QueueService;
use think\admin\service\SystemService;
use think\admin\service\TokenService;
use think\admin\Storage;
use think\db\Query;
use think\Model;
use think\facade\Event;
use think\helper\{
    Str, Arr
};

/**
 * 分类的链接信息
 * @param int $cid 分类ID
 * @param string $type 返回类型，link|field
 */
function getCate($cid='',$type='url'){
	if(!$cid) return '';
	$categorys = \think\admin\service\CategoryService::instance()->getCache();
	$cate = $categorys[$cid]??[];
	if($type == 'link') {
		return '<a href="'.$cate['url'].'" target="'.$cate['target'].'">'.$cate['name'].'</a>';
	}else{
		return isset($cate[$type]) ? $cate[$type] : '';
	}
}
/**
 * 返回钩子数据
 */
function ShowLabel($type='',$param=[]){
	$params = ['type'=>$type,'param'=>$param];
	return hook('ShowLabel',$params);
}
/**
 * CMS模板自定义标签
 * 自定义标签解析
 */
function Label($type='') {
	$data = \think\facade\Db::name('CmsLabel')->where('name',$type)->value('content');
	if(!is_null(json_decode($data))){
		$data = json_decode($data,true);

		if($data['tag'] == 'addons'){
			$params = ['title'=>$data['title'],'classname'=>$data['classname'],'hook_name'=>$data['name'],'hook_query'=>$data['param']];
			return ShowLabel('addons_tabs',$params);
		}else{
			return ShowLabel($data['tag'],$data['param']);
		}

	}
	return $data;
}

if (!function_exists('clear_html')) {
    /**
     * 过滤HTML代码，并截取一定长度的数据
     */
    function clear_html($string, $sublen=0){ 
        $string = strip_tags($string); 
        $string = trim($string); 
        $string = str_replace("\t","",$string); 
        $string = str_replace("\r\n","",$string); 
        $string = str_replace("\r","",$string); 
        $string = str_replace("\n","",$string); 
        $string = str_replace(" ","",$string); 
        return $sublen ? str_cut($string,$sublen) : trim($string); 
    }
}
if (!function_exists('getNumber')) {
    /**
     * 在字符串中提取数字
     */
    function getNumber($str){
        if(preg_match('/\d+/',$str,$arr)){
        return $arr[0];
        }
    }
}
if (!function_exists('str_cut')) {
    /**
     *  参数说明
     *  $string  欲截取的字符串
     *  $sublen  截取的长度
     *  $start   从第几个字节截取，默认为0，一个汉字按2个字符计算
     *  $code    字符编码，默认UTF-8
     */


    function str_cut($string, $sublen,$suffix='...', $start = 0, $code = 'UTF-8') {
        if ($code == 'UTF-8') {
            $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
            preg_match_all($pa, $string, $t_string);
            if (count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen)) . $suffix;
            return join('', array_slice($t_string[0], $start, $sublen));
        } else {
            $start = $start * 2;
            $sublen = $sublen * 2;
            $strlen = strlen($string);
            $tmpstr = '';
            for ($i = 0; $i < $strlen; $i++) {
                if ($i >= $start && $i < ($start + $sublen)) {
                    if (ord(substr($string, $i, 1)) > 129) {
                        $tmpstr.= substr($string, $i, 2);
                    } else {
                        $tmpstr.= substr($string, $i, 1);
                    }
                }
                if (ord(substr($string, $i, 1)) > 129) $i++;
            }
            if (strlen($tmpstr) < $strlen) $tmpstr.= "";
            return $tmpstr;
        }
    }
}
if (!function_exists('siteinfo')) {
    /**
     * 站点信息
     */
    function siteinfo($name){
      $sites = request()->siteinfo;
      return isset($sites[$name]) ? $sites[$name] : '';
    }
  }
if (!function_exists('SplitWord')) {
    /**
     * SCWS中文分词
     * @param string $text 分词字符串
     * @param number $number 权重高的词数量(默认5个)
     * @param string $type 返回类型,默认字符串
     * @param string $delimiter 分隔符
     * @return string|array 字符串|数组
     */
    function SplitWord($text = '', $number = 5, $type = true, $delimiter = ',')
    {
        return SplitWord::SplitWord($text, $number, $type, $delimiter);
    }
}

if (!function_exists('pr')) {
    /**
     * 打印输出数据到浏览器
     * @param mixed $data 输出的数据
     * @param boolean $var 数据打印类型，ture:var_dump,flase:print_r
     */
    function pr($data='', bool $var = false)
    {
       $var ? var_dump($data):print_r($data);
       die;
    }
}


if (!function_exists('p')) {
    /**
     * 打印输出数据到文件
     * @param mixed $data 输出的数据
     * @param boolean $new 强制替换文件
     * @param null|string $file 保存文件名称
     * @return false|int
     */
    function p($data, bool $new = false, ?string $file = null)
    {
        return SystemService::instance()->putDebug($data, $new, $file);
    }
}
if (!function_exists('M')) {
    /**
     * 动态创建模型对象
     * @param string $name 模型名称
     * @param array $data 初始数据
     * @param string $conn 指定连接
     * @return Model
     */
    function M(string $name, array $data = [], string $conn = ''): Model
    {
        return Helper::buildModel($name, $data, $conn);
    }
}
if (!function_exists('auth')) {
    /**
     * 访问权限检查
     * @param null|string $node
     * @return boolean
     * @throws ReflectionException
     */
    function auth(?string $node): bool
    {
        return AdminService::instance()->check($node);
    }
}
if (!function_exists('sysuri')) {
    /**
     * 生成最短 URL 地址
     * @param string $url 路由地址
     * @param array $vars PATH 变量
     * @param boolean|string $suffix 后缀
     * @param boolean|string $domain 域名
     * @return string
     */
    function sysuri(string $url = '', array $vars = [], $suffix = true, $domain = false): string
    {
        return SystemService::instance()->sysuri($url, $vars, $suffix, $domain);
    }
}
if (!function_exists('adminuri')) {
    /**
     * 生成后台 URL 地址
     * @param string $url 路由地址
     * @param array $vars PATH 变量
     * @param boolean|string $suffix 后缀
     * @param boolean|string $domain 域名
     * @return string
     */
    function adminuri(string $url = '', array $vars = [], $suffix = true, $domain = false): string
    {
        return sysuri('admin/index/index') . '#' . url($url, $vars, $suffix, $domain)->build();
    }
}
if (!function_exists('sysconf')) {
    /**
     * 获取或配置系统参数
     * @param string $name 参数名称
     * @param mixed $value 参数内容
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function sysconf(string $name = '', $value = null)
    {
        if (is_null($value) && is_string($name)) {
            return SystemService::instance()->get($name);
        } else {
            return SystemService::instance()->set($name, $value);
        }
    }
}
if (!function_exists('sysdata')) {
    /**
     * JSON 数据读取与存储
     * @param string $name 数据名称
     * @param mixed $value 数据内容
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function sysdata(string $name, $value = null)
    {
        if (is_null($value)) {
            return SystemService::instance()->getData($name);
        } else {
            return SystemService::instance()->setData($name, $value);
        }
    }
}
if (!function_exists('sysqueue')) {
    /**
     * 注册异步处理任务
     * @param string $title 任务名称
     * @param string $command 执行内容
     * @param integer $later 延时执行时间
     * @param array $data 任务附加数据
     * @param integer $rscript 任务类型(0单例,1多例)
     * @param integer $loops 循环等待时间
     * @return string
     * @throws \think\admin\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function sysqueue(string $title, string $command, int $later = 0, array $data = [], int $rscript = 1, int $loops = 0): string
    {
        return QueueService::instance()->register($title, $command, $later, $data, $rscript, $loops)->code;
    }
}
if (!function_exists('xss_safe')) {
    /**
     * 文本内容XSS过滤
     * @param string $text
     * @return string
     */
    function xss_safe(string $text): string
    {
        $rules = [
            '#<script.*?<\/script>#i'        => '',
            '#\s+on\w+=[\'\"]+.*?(\'|\")+#i' => '',
            '#\s+on\w+=\s*.*?(\s|>)+#i'      => '$1',
        ];
        foreach ($rules as $rule => $value) {
            $text = preg_replace($rule, $value, $text);
        }
        return $text;
    }
}
if (!function_exists('systoken')) {
    /**
     * 生成 CSRF-TOKEN 参数
     * @param null|string $node
     * @return string
     */
    function systoken(?string $node = null): string
    {
        $result = TokenService::instance()->buildFormToken($node);
        return $result['token'] ?? '';
    }
}
if (!function_exists('sysoplog')) {
    /**
     * 写入系统日志
     * @param string $action 日志行为
     * @param string $content 日志内容
     * @return boolean
     */
    function sysoplog(string $action, string $content): bool
    {
        return SystemService::instance()->setOplog($action, $content);
    }
}
if (!function_exists('str2arr')) {
    /**
     * 字符串转数组
     * @param string $text 待转内容
     * @param string $separ 分隔字符
     * @param null|array $allow 限定规则
     * @return array
     */
    function str2arr(string $text, string $separ = ',', ?array $allow = null): array
    {
        $text = trim($text, $separ);
        $data = strlen($text) ? explode($separ, $text) : [];
        if (is_array($allow)) foreach ($data as $key => $item) {
            if (!in_array($item, $allow)) unset($data[$key]);
        }
        foreach ($data as $key => $item) {
            if ($item === '') unset($data[$key]);
        }
        return $data;
    }
}
if (!function_exists('arr2str')) {
    /**
     * 数组转字符串
     * @param array $data 待转数组
     * @param string $separ 分隔字符
     * @param null|array $allow 限定规则
     * @return string
     */
    function arr2str(array $data, string $separ = ',', ?array $allow = null): string
    {
        if (is_array($allow)) foreach ($data as $key => $item) {
            if (!in_array($item, $allow)) unset($data[$key]);
        }
        foreach ($data as $key => $item) {
            if ($item === '') unset($data[$key]);
        }
        return $separ . join($separ, $data) . $separ;
    }
}
if (!function_exists('encode')) {
    /**
     * 加密 UTF8 字符串
     * @param string $content
     * @return string
     */
    function encode(string $content): string
    {
        [$chars, $length] = ['', strlen($string = iconv('UTF-8', 'GBK//TRANSLIT', $content))];
        for ($i = 0; $i < $length; $i++) $chars .= str_pad(base_convert(ord($string[$i]), 10, 36), 2, 0, 0);
        return $chars;
    }
}
if (!function_exists('decode')) {
    /**
     * 解密 UTF8 字符串
     * @param string $content
     * @return string
     */
    function decode(string $content): string
    {
        $chars = '';
        foreach (str_split($content, 2) as $char) {
            $chars .= chr(intval(base_convert($char, 36, 10)));
        }
        return iconv('GBK//TRANSLIT', 'UTF-8', $chars);
    }
}
if (!function_exists('enbase64url')) {
    /**
     * Base64安全URL编码
     * @param string $string
     * @return string
     */
    function enbase64url(string $string): string
    {
        return CodeExtend::safeBase64Encode($string);
    }
}
if (!function_exists('debase64url')) {
    /**
     * Base64安全URL解码
     * @param string $string
     * @return string
     */
    function debase64url(string $string): string
    {
        return CodeExtend::safeBase64Decode($string);
    }
}
if (!function_exists('http_get')) {
    /**
     * 以get模拟网络请求
     * @param string $url HTTP请求URL地址
     * @param array|string $query GET请求参数
     * @param array $options CURL参数
     * @return boolean|string
     */
    function http_get(string $url, $query = [], array $options = [])
    {
        return HttpExtend::get($url, $query, $options);
    }
}
if (!function_exists('http_post')) {
    /**
     * 以post模拟网络请求
     * @param string $url HTTP请求URL地址
     * @param array|string $data POST请求数据
     * @param array $options CURL参数
     * @return boolean|string
     */
    function http_post(string $url, $data, array $options = [])
    {
        return HttpExtend::post($url, $data, $options);
    }
}
if (!function_exists('data_save')) {
    /**
     * 数据增量保存
     * @param Model|Query|string $dbQuery
     * @param array $data 需要保存或更新的数据
     * @param string $key 条件主键限制
     * @param array $where 其它的where条件
     * @return boolean|integer
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function data_save($dbQuery, array $data, string $key = 'id', array $where = [])
    {
        return SystemService::instance()->save($dbQuery, $data, $key, $where);
    }
}
if (!function_exists('format_bytes')) {
    /**
     * 文件字节单位转换
     * @param string|integer $size
     * @return string
     */
    function format_bytes($size): string
    {
        if (is_numeric($size)) {
            $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
            for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
            return round($size, 2) . ' ' . $units[$i];
        } else {
            return $size;
        }
    }
}
if (!function_exists('filesize_format')) {
    /**
     * 文件大小获取
     * @param string|integer $path
     * @return string
     */
    function filesize_format($path): string
    {
        $size = filesize($path);
        return format_bytes($size);
    }
}
if (!function_exists('format_datetime')) {
    /**
     * 日期格式标准输出
     * @param int|string $datetime 输入日期
     * @param string $format 输出格式
     * @return string
     */
    function format_datetime($datetime, string $format = 'Y年m月d日 H:i:s'): string
    {
        if (empty($datetime)) return '-';
        if (is_numeric($datetime)) {
            return date($format, $datetime);
        } else {
            return date($format, strtotime($datetime));
        }
    }
}
if (!function_exists('toDate')) {
    /**
     * 日期格式标准输出
     * @param int|string $datetime 输入日期
     * @param string $format 输出格式
     * @return string
     */
    function toDate($datetime, string $format = 'Y-m-d H:i:s'): string
    {
        if (empty($datetime)) return '-';
        if (is_numeric($datetime)) {
            return date($format, $datetime);
        } else {
            return date($format, strtotime($datetime));
        }
    }
}
if (!function_exists('down_file')) {
    /**
     * 下载远程文件到本地
     * @param string $source 远程文件地址
     * @param boolean $force 是否强制重新下载
     * @param integer $expire 强制本地存储时间
     * @return string
     */
    function down_file(string $source, bool $force = false, int $expire = 0): string
    {
        return Storage::down($source, $force, $expire)['url'] ?? $source;
    }
}
if (!function_exists('mitobyte')) {

    /**
     * @param number|string $value
     * @return number
     */

    function mitobyte($value)
    {
        return preg_replace_callback('/^\s*(\d+)\s*(?:([kmgt]?)b?)?\s*$/i', function ($m) {
            switch (strtolower($m[2])) {
                case 't':
                    $m[1] *= 1024;
                case 'g':
                    $m[1] *= 1024;
                case 'm':
                    $m[1] *= 1024;
                case 'k':
                    $m[1] *= 1024;
            }
            return $m[1];
        }, $value);
    }
}
if (!function_exists('delDir')) {
    /**
     * 删除目录（递归删除）
     * @param $dir 目录路径
     * @return bool
     */
    function delDir($dir){
        DirExtend::dirDelete($dir);
    }
}
if (!function_exists('copyDir')) {
    /**
     * 复制目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     */
    function copyDir($dir1, $dir2){
        DirExtend::dirCopy($dir1, $dir2);
    }
}
if (!function_exists('moveDir')) {
    /**
     * 移动目录
     * @param $dir1 目录源路径
     * @param $dir2 目录目标路径
     */
    function moveDir($dir1, $dir2){
        DirExtend::dirMove($dir1, $dir2);
    }
}
if (!function_exists('testwrite')) {
    /**
     * 检查读写权限
     * @param $path 目录或文件地址
     * @return string
     */
    function testwrite($path)
    {
        return DirExtend::isWrite($path);
    }
}
if (!function_exists('dir_create')) {
    /**
     * 判断并创建目录
     * @param $path  目录地址 
     */
    function dir_create($path, $mode = 0777){
        return DirExtend::dirCreate($path, $mode);
    }
}
if (!function_exists('dir_path')) {
    /**
     * 格式化目录地址
     */
    function dir_path($path){
        return DirExtend::dirPath($path);
    }
}
/**
 * 404错误页面
 */
function _404($text=''){
    $text = !empty($text) ? $text : '对不起，您请求的页面不存在、或已被删除、或暂时不可用';
    $html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>404-对不起！您访问的页面不存在</title>
            <style type="text/css">
            .head404{ width:580px; height:234px; margin:50px auto 0 auto; }
            .txtbg404{ width:499px; height:169px; margin:10px auto 0 auto; }
            .txtbg404 .txtbox{ width:390px; position:relative; top:30px; left:60px;color:#eee; font-size:13px;}
            .txtbg404 .txtbox p {margin:5px 0; line-height:18px;}
            .txtbg404 .txtbox .paddingbox { padding-top:15px;}
            .txtbg404 .txtbox p a { color:#eee; text-decoration:none;}
            .txtbg404 .txtbox p a:hover { color:#FC9D1D; text-decoration:underline;}
            </style>
            </head>
            <body bgcolor="#494949">
                <div class="head404"></div>
                <div class="txtbg404">
              <div class="txtbox">
                  <p>'.$text.'</p>
                  <p class="paddingbox">请点击以下链接继续浏览网页</p>
                  <p>》<a style="cursor:pointer" onclick="history.back()">返回上一页面</a></p>
                  <p>》<a href="'.request()->domain().'">返回网站首页</a></p>
                </div>
              </div>
            </body>
            </html>';
    exit($html);
}

if (!function_exists('clear_js')) {
    /**
     * 过滤js内容
     * @param string $str 要过滤的字符串
     * @author 蔡伟明 <314013107@qq.com>
     * @return mixed|string
     */
    function clear_js($str = '')
    {
        $search ="/<script[^>]*?>.*?<\/script>/si";
        $str = preg_replace($search, '', $str);
        return $str;
    }
}

if (!function_exists('parse_attr')) {
    /**
     * 解析配置
     * @param string $value 配置值
     * @return array|string
     */
    function parse_attr($value = '') {
        $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
        if (strpos($value, ':')) {
            $value  = array();
            foreach ($array as $val) {
                list($k, $v) = explode(':', $val);
                $value[$k]   = $v;
            }
        } else {
            $value = $array;
        }
        return $value;
    }
}

if(!function_exists('thumb')){
    /**
     * 自动缩略图生成
     * @param srting $src 图片地址
     * @param int $w 缩略图的库宽度
     * @return string
     */
    function thumb($src,$w=600){
        $allowext = ['png','jpg','bmp','jpeg','gif'];
        $extension = pathinfo($src,PATHINFO_EXTENSION);
        if(in_array($extension,$allowext)){
            return $src.'.w'.$w.'.'.$extension;
        }else{
            return $src;
        }
    }
}
/**
 * 多维数组中查找数据
 */
if(!function_exists('searchArray')){
    /**
     * 多维数组中查找数据
     * @param array $array 要查找的数据
     * @param string $key 查找的键值
     * @param string|array $value 查找的键值对应的值
     * @return array
     */
    function searchArray(array $array,string $key,$value=''){
        return DataExtend::getArrSearch($array,$key,$value);
    }
}


if (!function_exists('getWeather')) {
    /**
     * 高德天气查询接口
     * 文档地址https://lbs.amap.com/api/webservice/guide/api/weatherinfo
     * @param string $city 市编码
     * @param string $key 请求服务权限标识
     * @return string
     */
    function getWeather($city='630100', string $key = 'ad723003a89ce12f96cfcdc79e15dc74')
    {
        $data = json_decode(http_get('https://restapi.amap.com/v3/weather/weatherInfo?extensions=all&city='.$city.'&key='.$key),true);
        $h=date('H');
        $weekname=array('星期一','星期二','星期三','星期四','星期五','星期六','星期日');
        $weather = '';
        if($data['status']){
            $forecasts = $data['forecasts'][0];
            $casts = $forecasts['casts'][0];
            if($h>=8 && $h<=20){
                $weather = [
                    'city' => $forecasts['city'],
                    'weather' =>  $casts['dayweather'],
                    'temp' =>  $casts['nighttemp'].'~'.$casts['daytemp'].'℃',
                    'wind'  =>  $casts['daywind'],
                    'power'  =>  $casts['daypower'],
                    'week'  =>  $weekname[$casts['week']-1],
                    'day'   =>  date('Y年m月d日',time())
                ];
            }else{
                $weather = [
                    'city' => $forecasts['city'],
                    'weather' =>  $casts['nightweather'],
                    'temp' =>  $casts['nighttemp'].'~'.$casts['daytemp'].'℃',
                    'wind'  =>  $casts['nightwind'],
                    'power'  =>  $casts['nightpower'],
                    'week'  =>  $weekname[$casts['week']-1],
                    'day'   =>  date('Y年M月D日',time())
                ];
            }
        }
        
        return $weather;
    }
}
if (!function_exists('Pinyin')) {
    /**
     * 获取完整拼音
     * @param string $string  需要转换的字符串
     * @param string $encoding 编码类型utf8
     */
    function Pinyin($string, $encoding = 'utf-8'){
        return PinyinExtend::Pinyin($string, $encoding);
    }
}
if (!function_exists('ShortPinyin')) {
    /**
     * 获取拼音缩写
     * @param string $string  需要转换的字符串
     * @param string $encoding 编码类型utf8
     */
    function ShortPinyin($string, $encoding = 'utf-8'){
        return PinyinExtend::ShortPinyin($string, $encoding);
    }
}

if (!function_exists('clearHtml')) {
    /**
     * 清除html换行等
     * @param $str
     * @return mixed|string
     */
    function clearHtml($str){
        return DataExtend::clearHtml($str);
    }
}
if (!function_exists('dataDesensitization')) {
    /**
     * 数据脱敏
     * @param $string 需要脱敏值
     * @param int $start 开始
     * @param int $length 结束
     * @param string $re 脱敏替代符号
     * @return bool|string
     * 例子:
     * dataDesensitization('18811113683', 3, 4); //188****3683
     * dataDesensitization('独角戏', 0, -1); //**戏
     */
    function dataDesensitization($string, $start = 0, $length = 0, $re = '*')
    {
        if (empty($string)){
            return false;
        }
        $strarr = array();
        $mb_strlen = mb_strlen($string);
        while ($mb_strlen) {//循环把字符串变为数组
            $strarr[] = mb_substr($string, 0, 1, 'utf8');
            $string = mb_substr($string, 1, $mb_strlen, 'utf8');
            $mb_strlen = mb_strlen($string);
        }
        $strlen = count($strarr);
        $begin = $start >= 0 ? $start : ($strlen - abs($start));
        $end = $last = $strlen - 1;
        if ($length > 0) {
            $end = $begin + $length - 1;
        } elseif ($length < 0) {
            $end -= abs($length);
        }
        for ($i = $begin; $i <= $end; $i++) {
            $strarr[$i] = $re;
        }
        if ($begin >= $end || $begin >= $last || $end > $last) return false;
        return implode('', $strarr);
    }
}
if (!function_exists('systabfiled')) {
    /**
     * layuiTable字段生成
     * @param array $field
     * @return string
     */
    function systabfiled(array $field = null): string
    {
        $col = [
            'field' => $field['name'],
            'sort' => $field['sort']?true : false,
            'hide' => $field['list']?true : false,
            'width' => $field['width'],
            'title' => $field['title'],
            'align' => $field['align'],
        ];
        return json_encode($col).',';
    }
}
if (!function_exists('arraySort')) {
    /**
     * 二维数组根据某个字段排序
     * @param array $array 要排序的数组
     * @param string $keys   要排序的键字段
     * @param string $sort  排序类型  SORT_ASC     SORT_DESC 
     * @return array 排序后的数组
     */
    function arraySort($array, $keys, $sort = SORT_DESC) {
        $keysValue = [];
        foreach ($array as $k => $v) {
            $keysValue[$k] = $v[$keys];
        }
        array_multisort($keysValue, $sort, $array);
        return $array;
    }
}