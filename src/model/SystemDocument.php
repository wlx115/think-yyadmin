<?php
namespace think\admin\model;

use think\admin\Model;
/**
 * 文档管理模型
 * Class SystemDocument
 * @package think\admin\model
 */
class SystemDocument extends Model
{
    //数据表名
    protected $name="CmsDocument";

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '内容文档';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '文档内容管理';
    /**
     * 文档分类模型关联
     */
    public function cate($where=[])
    {
        return $this->hasOne(SystemCategory::class,'id','cid')->where($where);
    }
    /**
     * 模块关联
     */
    public function module()
    {
        return $this->hasOne(SystemModule::class,'id','mid');
    }
    /**
     * 字段关联
     */
    public function fields()
    {
        return $this->hasMany(SystemField::class,'mid','mid');
    }
    /**
     * 站点关联
     */
    public function site()
    {
        return $this->hasOne(SystemSite::class,'id','siteid');
    }
    
    /**
     * 审核日志
     */
    public function audit()
    {
        return $this->hasOne(CmsAuditlog::class,'aid','id')->bind(['level']);
    }
    /**
     * 属性修改器
     */
    public function setFlagAttr($value)
    {
        return implode(',',$value);
    }
    /**
     * 获取属性
     */
    public function getFlagAttr($value)
    {
        return is_array($value) ? $value : explode(',',$value.'');
    }
    /**
     * 获取状态
     */
    public function getStatusTextAttr($value,$data)
    {
        $status = [-1=>'删除',0=>'禁用',1=>'正常'];
        return $status[$data['status']];
    }
    /**
     * 模型扩展模块数据关联
     */
    public function getExtendData($name='',$id)
    {
        return $this->setSuffix('_'.$name)->findOrEmpty($id)->toArray();
    }
    /**
     * 获取单篇文档信息
     * @param string $ids
     */
    public function getOne(string $id,$type='doc')
    {
        if($type == 'cate'){
            $obj = $this->with(['cate','module','fields'])->where('cid',$id)->order('id desc')->findOrEmpty();
        }else{
            $obj = $this->with(['cate','module','fields'])->findOrEmpty($id);
        }
        if (!$obj->isEmpty()) {
            //更新访问量
            if($type == 'doc')$obj->inc('view')->update();
            //组合扩展数据
            $info = $obj->toArray();
            $extData = $this->setSuffix('_'.$info['module']['name'])->findOrEmpty($info['id'])->toArray();
            $info = array_merge($info,$extData);
            foreach ($info['fields'] as $key => $val) {
                $field = $val['name'];
                $value = $info[$field];
                if($val['type'] == 'checkbox'){
                    $info[$field] = is_array($value) ? $value : explode(',',$value.'');
                }elseif($val['type'] == 'files' || $val['type'] == 'images'){
                    $info[$field] = explode('|',$value);
                }
            }
            return $info;
        }
        return[];
    }
    /**
     * 获取多条文档信息
     * @param string|array $cids
     */
    public function getList($cids='',$where='',$limit=10,$order='sort ASC,create_time desc,id desc')
    {
        $cidArr = is_array($cids) ? $cids :explode(',',$cids);
        $map = ["status"=>1,"deleted"=>0];
        $query = $this->with(['cate','site','module'])->where($where)->where($map);
        if(count($cidArr) > 0){
            $query->whereIn("cid",$cidArr)->whereOr(function ($query) use($cidArr) {
                foreach ($cidArr as $key => $id) {
                    $query->whereFindInSet("relation_cid",$id,"or");
                }
            });
        }
        $list = $query->limit($limit)->order($order)->select()->toArray();
        
        return $list;
    }
    /**
     * 获取分页文档信息
     * @param string|array $cids
     */
    public function getPageList($cids='',$where='',$limit=10,$order='sort ASC,create_time desc,id desc',$page=1,$pagepath='')
    {
        $cidArr = is_array($cids) ? $cids :explode(',',$cids);
        $map = ["status"=>1,"deleted"=>0];
        $query = $this->with(['cate','site','module'])->where($where)->where($map);
        if(count($cidArr) > 0){
            $query->whereIn("cid",$cidArr)->whereOr(function ($query) use($cidArr) {
                foreach ($cidArr as $key => $id) {
                    $query->whereFindInSet("relation_cid",$id,"or");
                }
            });
        }
        $list = $query->order($order)->paginate(['list_rows'=>$limit,'path'=>$pagepath,'page'=>$page]);
        return [$list->toArray(),$list->render(4)];
    }
    /**
     * 添加字段事件
     * @param string $ids
     */
    public function onAdminInsert(string $id)
    {
        //扩展表数据新增
        $this->saveExtendData($id);
        //发起审核流程操作
        sysoplog($this->oplogType, "新增{$this->oplogName}[{$id}]成功");
    }

    
    /**
     * 修改字段事件
     * @param string $ids
     */
    public function onAdminUpdate(string $id)
    {
        //扩展表数据新增
        $this->saveExtendData($id);
        //审批操作
        $this->auditAction($id);
        //发起审核流程操作
        sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]成功");
    }

    /**
     * 删除字段事件
     * @param string $ids
     */
    public function onAdminDelete(string $ids)
    {
        $recycle = input('type','');
        
        if (count($aids = str2arr($ids ?? '')) > 0) {
            foreach($this->whereIn('id', $aids)->cursor() as $val){
                if($recycle == 'recycle'){
                    $extTable = $this->getExtendTable($val['mid']);
                    $this->setSuffix($extTable)->where('id',$val['id'])->delete();
                    $this->destroy($val['id']);
                }
            }
            $text = $recycle == 'recycle' ? '删除' : '回收';
            sysoplog($this->oplogType, "{$text}{$this->oplogName}[{$ids}]成功");
        }
        
    }

    /**
     * 扩展表数据更新
     * @param string $mid
     */
    protected function saveExtendData($id)
    {
        $data = $this->getData();
        $extTable = $this->getExtendTable($data['mid']);
        $extData = $this->setSuffix($extTable)->where('id',$id)->find();
        if( $extData ){
            $this->setSuffix($extTable)->strict(false)->save($data);
        }else{
            $this->setSuffix($extTable)->strict(false)->insert($data);
        }
    }

    /**
     * 获取扩展表名
     * @param string $mid
     */
    protected function getExtendTable(string $mid='')
    {
        $module = SystemModule::mk()->cache(10)->find($mid)->toArray();
        return $module ? '_'.$module['name'] : '';
    }

    /**
     * 内容审批操作
     * @param string $mid
     */
    protected function auditAction($id)
    {
        $data = $this->getData();
        if(isset($data['audit_status'])){
            CmsAuditlog::mk()->setAuditLog($id,$data['audit_status'],$data['audit_desc'],$data['audit_level']);
        }
    }
}