<?php 
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;
use think\admin\service\DbService;
/**
 * 字段管理模型
 * Class SystemField
 * @package app\data\model
 */
class SystemField extends Model
{

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '扩展字段';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '扩展字段管理';

	/**
     * 关联自动所属模块
     * @return HasOne
     */
    public function module()
    {
        return $this->hasOne(SystemModule::class,'id','mid');
    }

    /**
     * 建立公共字段库
     * @param string $mid 模块ID
     * @return void
     */
    public function comFields($mid)
    {
        $comFields = $this->where('mid=0')->select()->toArray();
        foreach($comFields as &$val){
            $val['mid'] = $mid;
            unset($val['id']);
            $this->insert($val);
        }
    }
    /**
     * 格式化字段属性
     * @param string $value
     * @return array
     */
    public function getOptionArrAttr(string $value): array
    {
    	$value = explode("\n\r",$value);
    	$arr = [];
    	foreach ($value as $val) {
    		list($v,$k) = explode('|',$val.'|'.$val);
    		$arr[$k] = $v;
    	}
        return $arr;
    }
    /**
     * 添加字段事件
     * @param string $ids
     */
    public function onAdminInsert(string $id)
    {
        list($tableName,$fieldInfo) = $this->_getModuleTableInfo($id);
        try {
            DbService::instance()->createField($tableName,$fieldInfo);
            sysoplog($this->oplogType, "新增{$this->oplogName}[{$id}]及数据表更新");
        } catch (\Exception $e) {
            $this->destroy($id);
        } 
    }

    
    /**
     * 修改字段事件
     * @param string $ids
     */
    public function onAdminUpdate(string $id)
    {
        $system = $this->getData()['system'];
        // pr($this->getData());
        if(!$system){
            $oldField = $this->getData()['oldname'];
            list($tableName,$fieldInfo) = $this->_getModuleTableInfo($id);
            try {
                DbService::instance()->updateField($tableName,$oldField,$fieldInfo);
                sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]及数据表更新");
            } catch (\Exception $e) {
                $this->where('id',$id)->save(['name'=> $oldField]);
            }    
        }
             
    }

    /**
     * 删除字段事件
     * @param string $ids
     */
    public function onAdminDelete(string $id)
    {
        list($tableName,$fieldInfo) = $this->_getModuleTableInfo($id);
        try {
            DbService::instance()->deleteField($tableName,$fieldInfo['name']);
            $this->destroy($id);
            sysoplog($this->oplogType, "删除{$this->oplogName}[{$id}]及数据表");
        } catch (\Exception $e) {
            $this->where('id',$id)->save(['deleted'=> 0]);
        }       
        
    }
    /**
     * 获取字段关联的模块数据表名
     * @param string $id
     */
    private function _getModuleTableInfo($id)
    {
        $data = $this->with('module')->find($id)->toArray();
        $tableName   =   $data['module']['relation'].$data['module']['name'];
        $fieldInfo = [
            'comment'   =>  $data['title']. ' '. str_replace("\n\r",',',$data['options']),
            'name'      =>  $data['name'],
            'define'    =>  $data['define'],
        ];
        return [$tableName,$fieldInfo];
    }
}