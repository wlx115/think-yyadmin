<?php
namespace think\admin\model;

use think\admin\Model;
/**
 * 系统会员分组管理模型
 * Class SystemMemberGroup
 * @package think\admin\model
 */
class SystemMemberGroup extends Model
{
    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '会员分组管理';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统会员分组管理';


    /**
     * 关联用户
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(SystemMember::class, 'group_id', 'id')->where(['status' => 1, 'deleted' => 0]);
    }

    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}