<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;
/*
* 系统会员模型
* Class SystemMember
* @package think\admin\model
*/

class SystemMember extends Model
{
    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '系统会员';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统会员管理';

    /**
     * 获取会员数据
     * @param array $map 数据查询规则
     * @param array $data 会员数据集合
     * @param string $field 原外连字段
     * @param string $target 关联目标字段
     * @param string $fields 关联数据字段
     * @return array
     */
    public function items(array $map, array &$data = [], string $field = 'uuid', string $target = 'user_info', string $fields = 'username,nickname,headimg,status,is_deleted'): array
    {
        $query = $this->where($map)->order('sort desc,id desc');
        if (count($data) > 0) {
            $users = $query->whereIn('id', array_unique(array_column($data, $field)))->column($fields, 'id');
            foreach ($data as &$vo) $vo[$target] = $users[$vo[$field]] ?? [];
            return $users;
        } else {
            return $query->column($fields, 'id');
        }
    }

    /**
     * 关联会员角色
     * @return HasOne
     */
    public function usergroup()
    {
        return $this->hasOne(SystemMemberGroup::class, 'id', 'groupid');
    }

    /**
     * 格式化登录时间
     * @param string $value
     * @return string
     */
    public function getLoginAtAttr(string $value): string
    {
        return format_datetime($value);
    }

    /**
     * 格式化锁定时间
     * @param string $value
     * @return string
     */
    public function getLockAtAttr(string $value): string
    {
        return format_datetime($value);
    }

    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}