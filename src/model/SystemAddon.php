<?php
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace think\admin\model;

use  think\admin\Model;

/*
* 系统插件模型
* Class SystemAddon
* @package think\admin\model
*/

class SystemAddon extends Model
{
    /**
    * 日志名称
    * @var string
    */
    protected $oplogName = '系统插件';

    /**
    * 日志类型
    * @var string
    */
    protected $oplogType = '系统插件管理';

    /**
     * 数据类型转换
     */
    protected $type = [
        'setting'   =>  'json'
    ];
}