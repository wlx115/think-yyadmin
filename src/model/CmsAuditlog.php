<?php 
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 审核日志模型
 * Class CmsAuditlog
 * @package think\admin\model
 */
class CmsAuditlog extends Model
{

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '审核日志';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '文章审核管理';

    /**
     * 记录审核日志
     */
    public function setAuditLog($aid,$status=1,$desc='',$level=0)
    {
        $history = $this->where(['aid'=>$aid])->find();
        $nextLevel = $level<0 ? 99 : ($status ? $level + 1 : 99);
        $Content = $history ? unserialize($history['content']??'a:0:{}') : [];
        $Content[] = [
            'create_by'=>session('user.id'),
            'nickname'=>session('user.nickname'),
            'desc'=>$desc,
            'status' => $status ? '审批通过' : '退回修改',
            'create_at'=>date('Y-m-d H:i:s',time()),
        ];
        if ($history) {
            $history['level'] =  $nextLevel;
            $history['content'] = serialize($Content);
        }else{
            $history = [
                'content'   =>  serialize($Content),
                'aid'    =>  $aid,
                'level' =>  $nextLevel
            ];
        }
        $this->save($history);
    }

}