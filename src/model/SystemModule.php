<?php 
// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace think\admin\model;

use think\admin\Model;
use think\admin\service\DbService;
use think\admin\Exception;
use think\exception\HttpResponseException;
/**
 * 系统模块管理模型
 * Class SystemModule
 * @package think\admin\modell
 */
class SystemModule extends Model
{

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '自定义模块';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '自定义模块管理';

    
    /**
     * 关联字段模型
     */
    public function fields()
    {
        return $this->hasMany(SystemField::class,'mid')->order('sort asc,id asc');
    }

    /**
     * 模块初始化事件
     * @param string $id
     */
    public function mBuild(string $id)
    {
        $data = $this->with('fields')->findOrEmpty($id)->toArray();
        if($data){
            $tableName = $data['relation'].$data['name'];
            try {
                DbService::instance()->deleteTable($tableName);
                DbService::instance()->createTable($tableName,$data['title'],$data['fields']);
                sysoplog($this->oplogType, "初始化{$this->oplogName}[{$id}]及数据表成功"); 
                throw new HttpResponseException(json(['code' => 1, 'info' => '初始化模块成功']));
            } catch (Exception $e) {
                throw new HttpResponseException(json(['code' => 0, 'info' => $e->getMessage() ?? '初始化模块失败']));
            }
        }else{
            throw new HttpResponseException(json(['code' => 0, 'info' => '模块信息不存在']));
        }
    }

    /**
     * 新增模块事件
     * @param string $ids
     */
    public function onAdminInsert(string $id)
    {
        $data = $this->getData();
        $tableName = $data['relation'].$data['name'];
        try {
            DbService::instance()->createTable($tableName,$data['title'],[]);
            if($data['relation'] == 'cms_document_'){
                SystemField::mk()->comFields($id);
            }
            sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]及数据表更新"); 
        } catch (Exception $e) {
            $this->destroy($id);
            throw new HttpResponseException(json(['code' => 0, 'info' => $e->getMessage()]));
        }
    }

    /**
     * 修改模块事件
     * @param string $ids
     */
    public function onAdminUpdate(string $id)
    {
        $data = $this->getData();
        $oldname = $data['relation'].$data['oldname'];
        $newname = $data['relation'].$data['name'];
        if($oldname != $newname){
            try {
                DbService::instance()->updateTable($oldname,$newname);
            } catch (Exception $e) {
                $this->where('id',$id)->save(['name'=> $data['oldname']]);
                throw new HttpResponseException(json(['code' => 0, 'info' => $e->getMessage()]));
            }
        }
        sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]及数据表更新"); 
    }

    /**
     * 删除模块事件
     * @param string $ids
     */
    public function onAdminDelete(string $id)
    {
        $query = $this->with('fields')->find($id);
        $data = $query->toArray();
        $tableName = $data['relation'].$data['name'];
        try {
            DbService::instance()->deleteTable($tableName);
            $query->together(['fields'])->delete();
            sysoplog($this->oplogType, "删除{$this->oplogName}[{$id}]及数据表");
        } catch (Exception $e) {
            $query->save(['deleted'=>0]);
            throw new HttpResponseException(json(['code' => 0, 'info' => $e->getMessage()]));
        }
    }
}