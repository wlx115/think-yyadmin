<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkLibrary
// | github 代码仓库：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统钩子模型
 * Class SystemHook
 * @package think\admin\model
 */
class SystemHook extends Model
{
    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '系统钩子';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统钩子管理';

    /**
     * 插件字段字符串转数组
     * @param string $value
     * @return array
     */
    public function getAddonsAttr(string $value): array
    {
        return explode(',',$value.'');
    }

    /**
     * 插件字段数组转字符串，过滤重复
     * @param string|array $value
     * @return string
     */
    public function setAddonsAttr($value): string
    {
        return is_array($value) ? implode(',', array_filter(array_unique($value))) : $value;
    }
}