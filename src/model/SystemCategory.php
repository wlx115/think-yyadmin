<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkLibrary
// | github 代码仓库：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\admin\extend\DataExtend;

/**
 * 系统分类模型
 * Class SystemCategory
 * @package think\admin\model
 */
class SystemCategory extends Model
{
 /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '系统分类';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统分类管理';
    
    /**
     * 模块关联
     */
    public function module()
    {
        return $this->hasOne(SystemModule::class,'id','mid');
    }
    /**
     * 关联文档
     */
    public function document()
    {
        return $this->hasOne(SystemDocument::class,'cid');
    }

    /**
     * 格式化字段
     * @param string $value
     * @return array
     */
    public function getArrchildidAttr(string $value): array
    {
        return explode(",",$value);
    }
    /**
     * 格式化字段
     * @param string $value
     * @return array
     */
    public function getArrparentidAttr(string $value): array
    {
        return explode(",",$value);
    }

    /**
     * 获取所有栏目信息
     */
    public function getAllCate($where='',$type = '')
    {
        $sysCategory = $this->where($where)->with('module')->order('sort asc,id asc')->select()->toArray();
        $sysCategory = array_combine(array_column($sysCategory,'id'),$sysCategory);
        foreach ($sysCategory as &$vo) {
            $module = isset($vo['module']) ? $vo['module']['name'] : 'article';
            
            if($vo['type'] == 3){
                $fristSubId = $vo['arrchildid'][1];
                $cate = $sysCategory[$fristSubId];
                $vo['url']  =   $cate['url'] != '#' ? $cate['url'] : sysuri('index/cms/list',['catdir'=>$cate['catdir'],'module'=>$cate['module']['name']]);
            }else{
                $vo['url']  =   $vo['url'] != '#' ? $vo['url'] : sysuri('index/cms/list',['catdir'=>$vo['catdir'],'module'=>$module]);
            }
            
        }

        if($type == 'tree'){
            return DataExtend::arr2tree($sysCategory);
        }elseif($type == 'table'){
            return DataExtend::arr2table($sysCategory);
        }else{
            return $sysCategory;
        }
    }
}