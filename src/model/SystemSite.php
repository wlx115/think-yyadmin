<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统数据模型
 * Class SystemSite
 * @package think\admin\model
 */
class SystemSite extends Model
{

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '系统站点';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统站点管理';


    /**
     * 新增模块事件
     * @param string $ids
     */
    public function onAdminInsert(string $id)
    {
        sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]及数据表更新"); 
        $this->clearCache();
    }

    /**
     * 修改模块事件
     * @param string $ids
     */
    public function onAdminUpdate(string $id)
    {
        sysoplog($this->oplogType, "修改{$this->oplogName}[{$id}]及数据表更新"); 
        $this->clearCache();
    }

    /**
     * 删除模块事件
     * @param string $ids
     */
    public function onAdminDelete(string $id)
    {
        sysoplog($this->oplogType, "删除{$this->oplogName}[{$id}]");
        $this->clearCache();
    }
    /**
     * 清除缓存
     *
     * @return void
     */
    public function clearCache()
    {
        app()->cache->delete('SystemSiteID');
        app()->cache->delete('SystemSiteDomain');
    }
}