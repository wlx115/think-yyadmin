<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2021 青海西诚电子科技有限公司 [ http://www.qhxckj.com ]
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/qhweb/ThinkAdmin
// +----------------------------------------------------------------------

namespace think\admin\model;

use think\admin\Model;
use think\admin\Storage;
/**
 * 上传管理模型
 * Class SystemUpload
 * @package think\admin\model
 */
class SystemUpload extends Model
{

    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '上传管理';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统上传附件管理';

    /**
     * 获取列表状态
     * @param array $list 数据列表
     * @param integer $uuid 用户UID
     * @return array
     */
    public function buildData(array &$list): array
    {
        if (count($list) > 0) {
            $region = new \Ip2Region();
            foreach ($list as &$vo) {
                $isp = $region->btreeSearch($vo['geoip']);
                $vo['geoisp'] = str_replace(['内网IP', '0', '|'], '', $isp['region'] ?? '') ?: '-';
                $vo['size'] = format_bytes($vo['size']);
            }
        }
        return $list;
    }

    /**
     * IP地址
     * @param string $value
     * @return string
     */
    public function setGeoipAttr(string $value,array $data): string
    {
        return $this->app->request->ip() ?: '127.0.0.1';
    }
    /**
     * 删除权限事件
     * @param string $ids
     */
    public function onAdminDelete(string $ids)
    {
        if (count($aids = str2arr($ids ?? '')) > 0) {
            $Storage = Storage::instance(strtolower(sysconf('storage.type')));
            foreach($this->whereIn('id', $aids)->cursor() as $val){
                $filename = str_replace('upload/','',$val['key']);
                if($Storage->has($filename)){
                    $Storage->del($filename);
                };
            }
        }
        sysoplog($this->oplogType, "删除{$this->oplogName}[{$ids}]及文件");
    }
}